#use wml::debian::template title="Debian 開発者のコーナー" MAINPAGE="true"
#use wml::debian::translation-check translation="9a3deb4236c92e083ea1e494362dc1ff242f6a8c"

<link href="$(HOME)/font-awesome.css" rel="stylesheet" type="text/css">

<aside>
<p><span class="fas fa-caret-right fa-3x"></span>このページの情報は一般に公開されてはいますが、主に Debian 開発者を対象としています。</p>
</aside>

<ul class="toc">
<li><a href="#basic">基本</a></li>
<li><a href="#packaging">パッケージ開発</a></li>
<li><a href="#workinprogress">進行中の仕事</a></li>
<li><a href="#projects">プロジェクト</a></li>
<li><a href="#miscellaneous">その他</a></li>
</ul>

<div class="row">

  <div class="column column-left" id="basic">
    <div style="text-align: center">
     <span class="fa fa-users fa-3x" style="float:left;margin-right:5px;margin-bottom:5px;"></span>
      <h2><a id="basic">一般的な情報</a></h2>
      <p>現在の開発者およびメンテナーや、プロジェクトへの参加方法、開発者データベース、憲章、選挙の流れ、リリース、アーキテクチャに関するリストがあります。</p>
    </div>

    <div style="text-align: left">
      <dl>
        <dt><a href="$(HOME)/intro/organization">Debian の組織構成</a></dt>

        <dd>
        Debianプロジェクトには1000人を超えるボランティアが参加しています。
        このページにはDebianの組織やチームやメンバー、連絡先のアドレスを含めて記載されています。
        </dd>
      
        <dt><a href="$(HOME)/intro/people">Debianプロジェクトを支えている人々</a></dt>

        <dd>
        <a href="https://wiki.debian.org/DebianDeveloper">Debian 開発者
        (DD)</a> (Debian プロジェクトの完全なメンバー)、そして <a
        href="https://wiki.debian.org/DebianMaintainer">Debian メンテナ
        (DM)</a> がプロジェクトに貢献しています。
	関わっている人を見つけるには、<a href="https://nm.debian.org/public/people/dd_all/">Debian開発者のリスト</a>と
	<a href="https://nm.debian.org/public/people/dm_all/">Debianメンテナーのリスト</a>を参照してください。
 	<a href="developers.loc">Debian開発者の世界地図</a>というのもあります。

        <dt><a href="join/">Debian に参加する</a></dt>

        <dd>
        Debianプロジェクトへの参加を希望しますか？
	私たちrは、新規の開発者や、技術的なスキルに限らずフリーソフトウェアに興味がある人を求めています。
        上記リンクのページを見てください。
        </dd>

        <dt><a href="https://db.debian.org/">開発者データベース</a></dt>

        <dd>
        このデータベースには、誰もがアクセスできる基本的な情報と、
        開発者だけが見ることができるよりプライベートな情報とが入っています。

        <p>データベースを使うと、
        <a href="https://db.debian.org/machines.cgi">プロジェクトのマシン</a>
        を見たり、
        <a href="extract_key">開発者の GPG キーを取得</a>したり、
        <a href="https://db.debian.org/password.html">パスワードを変更</a>
        したり、あなたの Debian メールアカウントを
        <a href="https://db.debian.org/forward.html">フォワードする方法を
        調べたり</a>することができます。</p>

        <p>Debian マシンを使おうと思っているのでしたら、まず
        <a href="dmup">Debian マシン使用方針</a>を読んでください。</p>
        </dd>

        <dt><a href="constitution">憲章</a></dt>

        <dd>
        プロジェクトにおける正式な意思決定のための組織構造について書かれています。
        </dd>

        <dt><a href="$(HOME)/vote/">投票情報</a></dt>

        <dd>
        私たちはどのようにしてリーダーやロゴを選ぶか、
        そして一般的に、投票のやりかたが分かります。
        </dd>
      </dl>

      <dl>
        <dt><a href="$(HOME)/releases/">リリース</a></dt>

        <dd>
        現在のリリース (<a href="$(HOME)/releases/stable/">安定版 (stable)</a>と
        <a href="$(HOME)/releases/testing/">テスト版 (testing)</a>
        <a href="$(HOME)/releases/unstable/">開発版 (unstable)</a>)
	と過去のリリースやコードネームの一覧があります。
        </dd>

        <dt><a href="$(HOME)/ports/">さまざまなアーキテクチャ</a></dt>

        <dd>
        Debian は、さまざまな種類のアーキテクチャで動作します。
        上記ページでは、さまざまなDebianの移植版
        - Linuxカーネルをベースにしていたり、FreeBSDやNetBSD、Hurdのカーネルをベースにしているものの情報をあつめてあります。
        </dd>
      </dl>

    </div>
  </div>

  <!-- right column -->
   <div class="column column-right" id="software">
    <div style="text-align: center">
      <span class="fa fa-code fa-3x" style="float:left;margin-right:5px;margin-bottom:5px;"></span>
      <h2><a id="packaging">パッケージ開発</a></h2>
      <p>ポリシーマニュアルとDebian開発者向けのDebianポリシーや手続き等に関連したドキュメントへのリンクです。新メンテナーガイドへのリンクもあります。</p>
    </div>

    <div style="text-align: left">
      <dl>
        <dt><a href="$(DOC)/debian-policy/">Debianポリシーマニュアル</a></dt>
        <dd>
        このマニュアルには、Debian ディストリビューションが
        守るべき方針が書かれています。Debian アーカイブの構造と内容、
        オペレーティングシステムの設計に関するいくつかの事項、
        個々のパッケージがディストリビューションに含まれるために
        満たさなければならない技術的な必要事項などが含まれます。

        <p>つまり、あなたはこれを読む<strong>必要</strong>があります。</p>
        </dd>
      </dl>

      <p>あなたが興味を持ちそうな、ポリシーに関連したドキュメントがいくつかあります。たとえば、</p>

      <ul>
        <li><a href="https://wiki.linuxfoundation.org/lsb/fhs/">Filesystem Hierarchy Standard</a> (FHS)
        <br />FHS は、ものごとを格納するためのディレクトリ (やファイル)
            の一覧で、Debian ポリシー 3.x でこれに準拠することが求められています。
	    (Debianポリシーマニュアルの<a href="https://www.debian.org/doc/debian-policy/ch-opersys.html#file-system-hierarchy">第9章</a>を参照してください。)</li>
        <li><a href="$(DOC)/packaging-manuals/build-essential">build-essential パッケージ</a>の一覧
        <br />build-essential パッケージは、あなたが何かパッケージを構築
            しようとしたときにすでにインストールしてあると期待されて
            いるパッケージのことです。これらのパッケージは、<a href="https://www.debian.org/doc/debian-policy/ch-relationships.html">依存関係を宣言する際</a>
	    あなたのパッケージの <code>Build-Depends</code> 行に含める必要がありません</li>
        <li><a href="$(DOC)/packaging-manuals/menu-policy/">メニューシステム</a>
        <br />Debianのメニューエントリの構造についてです。<a href="$(DOC)/packaging-manuals/menu.html/">メニューシステム文書</a>も参照してください。</li>
        <li><a href="$(DOC)/packaging-manuals/debian-emacs-policy">Emacs ポリシー</a></li>
        <li><a href="$(DOC)/packaging-manuals/java-policy/">Java ポリシー</a></li>
        <li><a href="$(DOC)/packaging-manuals/perl-policy/">Perl ポリシー</a></li>
        <li><a href="$(DOC)/packaging-manuals/python-policy/">Python ポリシー</a></li>
        <li><a href="$(DOC)/packaging-manuals/debconf_specification.html">Debconf 仕様書</a></li>
        <li><a href="https://www.debian.org/doc/manuals/dbapp-policy/">データベースアプリケーションポリシー</a>(草案)</li>
        <li><a href="https://tcltk-team.pages.debian.net/policy-html/tcltk-policy.html/">Tcl/Tk ポリシー</a>(草案)</li>
        <li><a href="https://people.debian.org/~lbrenta/debian-ada-policy.html">Debian ポリシー</a></li>
      </ul>

      <p><a href="https://bugs.debian.org/debian-policy">ポリシー改訂の
      提案</a>も見てください。</p>

      <dl>
        <dt><a href="$(DOC)/manuals/developers-reference/">
        デベロッパーズリファレンス</a></dt>

        <dd>
        このドキュメントの目的は、Debian 開発者に、推奨される手順と
        利用できるリソースの概要を提供することです。もう一つの
        必読文書です。
        </dd>

        <dt><a href="$(DOC)/manuals/debmake-doc/">Debian メンテナー用ガイド</a></dt>

        <dd>
        このドキュメントは、Debian パッケージの構築について実例を交えて
        一般的な言葉で説明しています。もし Debian 開発者 (パッケージ
        開発者) になりたいのなら、この文書を必ず読みたくなるはずです。
        </dd>
      </dl>

    </div>
  </div>
</div>

<h2><a id="workinprogress">進行中: 活発に活動しているDebian開発者やメンテナー向けのリンク</a></h2>

<aside class="light">
  <span class="fas fa-wrench fa-5x"></span>
</aside>


      <dl>
        <dt><a href="testing">テスト版
        (testing) ディストリビューション</a></dt>
        <dd>
        unstableディストリビューションから自動生成されたディストリビューションです。
	Debian の次期リリースにあなたのパッケージが含まれるためには、まず「テスト版」ディストリビューションに含まれなければなりません。
        </dd>

        <dt><a href="https://bugs.debian.org/release-critical/">リリース上の致命的バグ</a></dt>

        <dd>
        ここには、該当パッケージがテスト版 (testing) ディストリビューションから
        削除されたり、ときにはディストリビューションのリリースが
        遅れる原因となるようなバグの一覧があります。重要度
        (severity) が「serious」以上のバグがこれに相当します。
        あなたのパッケージにこのようなバグがあれば、できるだけ早く
        修正してください。

        </dd>

        <dt><a href="$(HOME)/Bugs/">バグ追跡システム</a></dt>
        <dd>
        Debian のバグ追跡システム (Bug Tracking System、略称 BTS) は、
        バグを報告したり、論議したり、修正したりするためのものです。Debian の
        ほとんどどんな部分の問題の報告も、ここでは歓迎されます。BTS は、
        ユーザと開発者双方に役立ちます。
        </dd>

        <dt>開発者の視点から見たパッケージ概観
        <dd>
        <a href="https://qa.debian.org/developer.php">パッケージ情報</a>
        と<a href="https://packages.qa.debian.org/">パッケージトラッキング</a>
        の各ウェブページは、メンテナにとって有用な情報が集められています。

        開発者が他のパッケージを最新に保ちたい場合、パッケージ追跡
        システムのサービスを (メールを用いて) 講読すると、BTS
        メールの写しや、アップロードやインストールの通知を受け取る
        ことができます。
        </dd>

        <dt><a href="wnpp/">援助が必要なパッケージ</a></dt>
        <dd>
        援助が必要なパッケージ (Work-Needing and Prospective Packages, 
        略して WNPP) は、新規メンテナが必要とされていて、Debian に
        含まれることがまだ必要とされているパッケージの一覧です。
        </dd>

        <dt><a href="$(DOC)/manuals/developers-reference/resources.html#incoming-system">\
            Incoming システム</a></dt>
        <dd>
        新しいパッケージは、内部のアーカイブサーバの「Incoming システム」
        にアップロードされます。受理されたパッケージはほとんどすぐに
        ブラウザ経由でアクセスでき、1日に 4 回<a href="$(HOME)/mirror/">ミラー</a>へと伝えられます。
        <br />
        <strong>注意</strong>: incoming
        ディレクトリの性質上、それをミラーリングすることはおすすめ
        できません。
        </dd>

        <dt><a href="https://lintian.debian.org/">Lintian レポート</a></dt>

        <dd>
        <a href="https://packages.debian.org/unstable/devel/lintian">
        Lintian</a> は、パッケージが Debian ポリシーを満たしているか
        どうかをチェックするプログラムです。パッケージをアップロード
        する前に必ずこれを使うべきです。
        </dd>

        <dt><a href="$(DOC)/manuals/developers-reference/resources#experimental">\
            Experimental ディストリビューション</a></dt>
        <dd>
        「<em>experimental</em>」ディストリビューションは、
        極めて実験的なソフトウェアのための一時的な準備エリアです。
        <a href="https://packages.debian.org/experimental/">\
        <em>experimental</em> パッケージ</a>の使用は、<em>unstable</em>
        (不安定版) の使い方をすでに知っている場合だけに限ってください。
        </dd>

	<dt><a href="https://wiki.debian.org/HelpDebian">Debian Wiki</a></dt>
       <dd>
      開発者やコントリビューターに役立つDebian Wiki
      </dd>

      </dl>

<h2><a id="projects">プロジェクト: 内部のグループやプロジェクト</a></h2>

<aside class="light">
  <span class="fas fa-folder-open fa-5x"></span>
</aside>


<ul>
<li><a href="website/">Debian ウェブページ</a></li>
<li><a href="https://ftp-master.debian.org/">Debian
    アーカイブ</a></li>
<li><a href="$(DOC)/ddp">Debian ドキュメンテーションプロジェクト
    (DDP)</a></li>
<li><a href="https://qa.debian.org/">品質保証
    (Quality Assurance)</a> グループ</li>
<li><a href="$(HOME)/CD/">Debian CD イメージ</a></li>
	  <li><a href="https://wiki.debian.org/Keysigning">キー署名</a></li>
<li><a href="https://wiki.debian.org/Keysigning">キー署名
    コーディネーションページ</a></li>
<li><a href="https://wiki.debian.org/DebianIPv6">Debian
    IPv6 プロジェクト</a></li>
<li><a href="buildd/">自動構築ネットワーク</a>と<a href="https://buildd.debian.org/">ビルドログ</a></li>
<li><a href="$(HOME)/international/l10n/ddtp">Debian
    パッケージ説明文翻訳プロジェクト (DDTP)</a></li>
<li><a href="debian-installer/">Debian インストーラ</a></li>
<li><a href="debian-live/">Debian Live</a></li>
<li><a href="$(HOME)/women/">Debian Women</a></li>
<li><a href="$(HOME)/blends/">Debian Pure Blends</a></li>
</ul>

<h2><a id="miscellaneous">その他のリンク</a></h2>

<aside class="light">
  <span class="fas fa-bookmark fa-5x"></span>
</aside>

<ul>
<li>PeerTubeにあるカンファレンストークの<a href="https://peertube.debian.social/home">録画</a> 
    <a href="https://debconf-video-team.pages.debian.net/videoplayer/">異なるインタフェース</a>を使えます</li>
<li><a href="passwordlessssh">パスワードを尋ねられないようにssh を設定する方法</a></li>
<li><a href="$(HOME)/MailingLists/HOWTO_start_list">新しい
            メーリングリストを要求する方法</a></li>
<li><a href="$(HOME)/mirror/">Debian をミラーする</a>ための情報</li>
<li><a href="https://qa.debian.org/data/bts/graphs/all.png">すべてのバグをグラフにしたもの</a></li>
<li>Debianに含まれるのを待っている (NEW Queue) <a href="https://ftp-master.debian.org/new.html">新規パッケージ</a> </li>
<li>直近 7日間の <a href="https://packages.debian.org/unstable/main/newpkg">新規 Debianパッケージ</a></li>
<li><a href="https://ftp-master.debian.org/removals.txt">Debianから削除されたパッケージ</a></li>
</ul>
