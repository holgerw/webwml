#use wml::debian::template title="Debian &ldquo;buster&rdquo; release-informatie"
#include "$(ENGLISHDIR)/releases/info"
#include "$(ENGLISHDIR)/releases/buster/release.data"
#include "$(ENGLISHDIR)/releases/arches.data"
#use wml::debian::translation-check translation="6bafc9758f46d9e4fe5b48e5d85484d9f3dc8e3e"


<p>Debian <current_release_buster> werd uitgebracht op
 <a href="$(HOME)/News/<current_release_newsurl_buster/>"><current_release_date_buster></a>.
<ifneq "10.0" "<current_release>"
  "Debian 10.0 werd oorspronkelijk uitgebracht op
   <:=spokendate('2019-07-06'):>."
/>
De release bevatte verschillende ingrijpende wijzigingen, beschreven in ons <a
href="$(HOME)/News/2019/20190706">persbericht</a> en de <a
href="releasenotes">notities bij de release</a>.</p>

<p><strong>Debian 10 werd vervangen door
<a href="../bullseye/">Debian 11 (<q>bullseye</q>)</a>.
Er worden geen beveiligingsupdates meer uitgebracht sinds <:=spokendate('2022-06-30'):>.
</strong></p>

<p><strong>Buster geniet evenwel van langetermijnondersteuning (Long Term Support - LTS) tot
eind juni 2024. De LTS is beperkt tot i386, amd64, armel, armhf en arm64.
Alle andere architecturen worden niet langer ondersteund in buster.
Raadpleeg voor meer informatie de <a
href="https://wiki.debian.org/LTS">sectie over LTS op de Wiki van Debian</a>.
</strong></p>

<p>Raadpleeg de <a href="debian-installer/">installatie-informatie</a>-pagina en de
<a href="installmanual">Installatiehandleiding</a> over het verkrijgen en installeren
van Debian. Zie de instructies in
de <a href="releasenotes">notities bij de release</a> om van een oudere Debian release
op te waarderen.</p>

### Wat volgt activeren wanneer de LTS-periode begint.
#<p>Ondersteunde architecturen tijdens de langetermijnondersteuning:</p>
#
#<ul>
#<:
#foreach $arch (@archeslts) {
#	print "<li><a href=\"$(HOME)/ports/$arch/\">$arches{$arch}</a>\n";
#}
#:>
#</ul>

<p>De volgende computerarchitecturen worden ondersteund in deze release:</p>
# <p>Ondersteunde computerarchitecturen bij de initiële release van buster:</p> ### Deze regel gebruiken als LTS start, in plaats van die hierboven.


<ul>
<:
foreach $arch (@arches) {
	print "<li><a href=\"$(HOME)/ports/$arch/\">$arches{$arch}</a>\n";
}
:>
</ul>

<p>In tegenstelling tot wat we zouden wensen, kunnen er enkele problemen bestaan
in de release, ondanks dat deze <em>stabiel</em> wordt genoemd. We hebben
<a href="errata">een overzicht van de belangrijkste bekende problemen</a> gemaakt
en u kunt ons altijd <a href="../reportingbugs">andere problemen rapporteren</a>.</p>

<p>Tot slot, maar niet onbelangrijk, een overzicht van de
<a href="credits">mensen</a> die deze release mogelijk maakten.</p>



