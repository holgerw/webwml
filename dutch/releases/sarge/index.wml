#use wml::debian::template title="Debian &ldquo;sarge&rdquo; release-informatie"
#include "$(ENGLISHDIR)/releases/info"
#use wml::debian::translation-check translation="c828123435e049f9c2b89a8e80a66d0817b40439"

<p>Debian GNU/Linux 3.1 (ook bekend als <em>sarge</em>) werd
uitgebracht op 6 juni 2005.
De nieuwe release bevat verschillende belangrijke wijzigingen, beschreven in ons
<a href="$(HOME)/News/2005/20050606">persbericht</a> en in de
<a href="releasenotes">Notities bij de release</a>.</p>

<p><strong>Debian GNU/Linux 3.1 werd vervangen door
<a href="../etch/">Debian GNU/Linux 4.0 (<q>etch</q>)</a>.
Er worden geen beveiligingsupdates meer uitgebracht sinds eind maart 2008.
</strong></p>

<p>Raadpleeg de installatie-informatie-pagina en de
Installatiehandleiding over het verkrijgen en installeren
van Debian GNU/Linux 3.1. Zie de instructies in de
<a href="releasenotes">Notities bij de release</a> om van een oudere Debian release
op te waarderen.</p>

<p>De volgende computerarchitecturen werden in deze release ondersteund:</p>

<ul>
<li><a href="../../ports/alpha/">Alpha</a>
<li><a href="../../ports/arm/">ARM</a>
<li><a href="../../ports/hppa/">HP PA-RISC</a>
<li><a href="../../ports/i386/">32-bits pc (i386)</a>
<li><a href="../../ports/ia64/">Intel Itanium IA-64</a>
<li><a href="../../ports/m68k/">Motorola 680x0</a>
<li><a href="../../ports/mips/">MIPS (big endian)</a>
<li><a href="../../ports/mipsel/">MIPS (little endian)</a>
<li><a href="../../ports/powerpc/">PowerPC</a>
<li><a href="../../ports/s390/">IBM S/390</a>
<li><a href="../../ports/sparc/">SPARC</a>
</ul>

<p>In tegenstelling tot wat we zouden wensen, kunnen er enkele problemen bestaan
in de release, ondanks dat deze <em>stabiel</em> wordt genoemd. We hebben
<a href="errata">een overzicht van de belangrijkste bekende problemen</a> gemaakt.
</p>
