#use wml::debian::template title="Alpha Port -- Systeemtypes" NOHEADER="yes"
#include "$(ENGLISHDIR)/ports/alpha/menu.inc"
#use wml::debian::translation-check translation="6f3adf6374f35194686f89dec2ba66b1ecf3bb5f"

<p>
Deze lijst is mogelijk niet compleet, dus als u iets ziet dat hier niet staat
of iets dat niet klopt, post het dan op
<a href="mailto:debian-alpha@lists.debian.org">debian-alpha</a> ter
verificatie en voor opname. Houd er rekening mee dat
<a href="https://www.gnu.org/software/hurd/gnumach.html">GNUMach</a>
momenteel alleen op
<a href="https://www.gnu.org/software/hurd/faq.en.html#q2-3">IA 32</a>
gebaseerde machines ondersteunt. Zodra het overzetten naar alpha is gestart,
wordt deze lijst bijgewerkt om zichtbaar te maken welke machines daadwerkelijk
worden ondersteund.</p>

#FIXME: Add links where appropriate, to e.g. BSDs info pages?
#FIXME: Also Niels has some machine specific links

<br>
<br>
<table class="ridgetable">
<tr>
<th>Model:</th>

<th>Alias:</th>
<th>Ondersteund door Linux</th>
<th>Ondersteund door GNUMach</th>
</tr>

<tr>
<td>21164 PICMG SBC (<abbr lang="nl" title="original equipment manufacturer - fabrikant van originele apparatuur">OEM</abbr>-segment &mdash; éénbordcomputer)</td>

<td>Takara/DMCC</td>
<td>Ja</td>
<td>Neen</td>
</tr>

<tr>
<td>DMCC 21264 PICMG (<abbr lang="nl" title="original equipment manufacturer - fabrikant van originele apparatuur">OEM</abbr>-segment)</td>

<td>Eiger <i>(Tsunami-familie, maar met 1 p-chip)</i></td>
<td>Ja</td>
<td>Neen</td>
</tr>

<tr>
<td>Alphabook1</td>

<td>Alphabook1/Burns <i>(Alpha Notebook-familie)</i></td>
<td>Ja</td>
<td>Neen</td>
</tr>

<tr>
<td>Alpha Demonstration Unit (prototypetoestel)</td>

<td>ADU</td>
<td>Neen</td>
<td>Neen</td>
</tr>

<tr>
<td>AlphaPC164</td>

<td>PC164 <i>(EB164-familie)</i></td>
<td>Ja</td>
<td>Neen</td>
</tr>

<tr>
<td>AlphaPC164-BX</td>

<td>Ruffian <i>(geproduceerd door Samsung)</i></td>
<td>Ja</td>
<td>Neen</td>
</tr>

<tr>
<td>AlphaPC164-LX</td>

<td>LX164 <i>(EB164-familie)</i></td>
<td>Ja</td>
<td>Neen</td>
</tr>

<tr>
<td>AlphaPC164-SX</td>

<td>SX164 <i>(EB164-familie)</i></td>
<td>Ja</td>
<td>Neen</td>
</tr>

<tr>
<td>AlphaPC164-UX</td>

<td>Ruffian <i>(geproduceerd door Samsung)</i></td>
<td>Ja</td>
<td>Neen</td>
</tr>

<tr>
<td>AlphaPC64</td>

<td>Cabriolet <i>(EB64+ familie)</i></td>
<td>Ja</td>
<td>Neen</td>
</tr>

<tr>
<td>AlphaPCI64</td>

<td>Cabriolet <i>(EB64+ familie)</i></td>
<td>Ja</td>
<td>Neen</td>
</tr>

<tr>
<td>AlphaStation 200 4/100...166</td>

<td>Mustang <i>(Avanti-familie)</i></td>
<td>Ja</td>
<td>Neen</td>
</tr>

<tr>
<td>AlphaStation 200 4/233</td>

<td>Mustang+ <i>(Avanti-familie)</i></td>
<td>Ja</td>
<td>Neen</td>
</tr>

<tr>
<td>AlphaStation 205 4/133...333</td>

<td>LX3 <i>(Avanti-familie)</i></td>
<td>Ja</td>
<td>Neen</td>
</tr>

<tr>
<td>AlphaStation 250 4/300</td>

<td>M3+ <i>(Avanti-familie)</i></td>
<td>Ja</td>
<td>Neen</td>
</tr>

<tr>
<td>AlphaStation 255 4/233...300</td>

<td>LX3+ <i>(Avanti-familie)</i></td>
<td>Ja</td>
<td>Neen</td>
</tr>

<tr>
<td>AlphaStation 300 4/266</td>

<td>Melmac <i>(Avanti-familie)</i></td>
<td>Ja</td>
<td>Neen</td>
</tr>

<tr>
<td>AlphaStation 400 4/166</td>

<td>Chinet <i>(Avanti-familie)</i></td>
<td>Ja</td>
<td>Neen</td>
</tr>

<tr>
<td>AlphaStation 400 4/233...300</td>

<td>Avanti <i>(Avanti-familie)</i></td>
<td>Ja</td>
<td>Neen</td>
</tr>

<tr>
<td>AlphaStation 500 5/266...300</td>

<td>Maverick <i>(Alcor-familie)</i></td>
<td>Ja</td>
<td>Neen</td>
</tr>

<tr>
<td>AlphaStation 500 5/333...500</td>

<td>Bret <i>(Alcor-familie)</i></td>
<td>Ja</td>
<td>Neen</td>
</tr>

<tr>
<td>AlphaStation 600/266...300</td>

<td>Alcor <i>(Alcor-familie)</i></td>
<td>Ja</td>
<td>Neen</td>
</tr>

<tr>
<td>AlphaStation 600/300...433</td>

<td>XLT <i>(Alcor-familie)</i></td>
<td>Ja</td>
<td>Neen</td>
</tr>

<tr>
<td>AlphaStation 600A</td>

<td>Alcor-Primo <i>(Noritake-familie)</i></td>
<td>Ja</td>
<td>Neen</td>
</tr>

<tr>
<td><a name="as800">AlphaServer 800</a> 5/333...500</td>

<td>Corelle <i>(Noritake-familie)</i></td>
<td>Ja</td>
<td>Neen</td>
</tr>

<tr>
<td>AlphaServer 1000 4/200</td>

<td>Mikasa <i>(Mikasa-familie)</i></td>
<td>Ja</td>
<td>Neen</td>
</tr>

<tr>
<td>AlphaServer 1000 4/233...266</td>

<td>Mikasa+ <i>(Mikasa-familie)</i></td>
<td>Ja</td>
<td>Neen</td>
</tr>

<tr>
<td>AlphaServer 1000 5/300</td>

<td>Pinnacle <i>(Mikasa-familie)</i></td>
<td>Ja</td>
<td>Neen</td>
</tr>

<tr>
<td>AlphaServer 1000 5/333...500</td>

<td>Primo <i>(Mikasa-familie)</i></td>
<td>Ja</td>
<td>Neen</td>
</tr>

<tr>
<td>AlphaServer 1000A 4/233...266</td>

<td>Noritake <i>(Noritake-familie)</i></td>
<td>Ja</td>
<td>Neen</td>
</tr>

<tr>
<td>AlphaServer 1000A 5/300</td>

<td>Pinnacle <i>(Noritake-familie)</i></td>
<td>Ja</td>
<td>Neen</td>
</tr>

<tr>
<td>AlphaServer 1000A 5/333...500</td>

<td>Primo <i>(Noritake-familie)</i></td>
<td>Ja</td>
<td>Neen</td>
</tr>

<tr>
<td><a name="as1200">AlphaServer 1200</a> 5/xxx</td>

<td>Tincup/DaVinci <i>(Rawhide-familie)</i></td>
<td>Ja</td>
<td>Neen</td>
</tr>

<tr>
<td>AlphaServer 2000 4/xxx</td>

<td>Demi-Sable <i>(Sable-familie)</i></td>
<td>Ja</td>
<td>Neen</td>
</tr>

<tr>
<td>AlphaServer 2000 5/xxx</td>

<td>Demi-Gamma-Sable <i>(Sable-familie)</i></td>
<td>Ja</td>
<td>Neen</td>
</tr>

<tr>
<td>AlphaServer 2100 4/xxx</td>

<td>Sable <i>(Sable-familie)</i></td>
<td>Ja</td>
<td>Neen</td>
</tr>

<tr>
<td>AlphaServer 2100 5/xxx</td>

<td>Gamma-Sable <i>(Sable-familie)</i></td>
<td>Ja</td>
<td>Neen</td>
</tr>

<tr>
<td>AlphaServer 2000a 4/xxx</td>

<td>Demi-Lynx</td>
<td>Ja</td>
<td>Neen</td>
</tr>

<tr>
<td>AlphaServer 2000a 5/xxx</td>

<td>Demi-Gamma-Lynx</td>
<td>Ja</td>
<td>Neen</td>
</tr>

<tr>
<td>AlphaServer 2100a 4/xxx</td>

<td>Lynx</td>
<td>Ja</td>
<td>Neen</td>
</tr>

<tr>
<td>AlphaServer 2100a 5/xxx</td>

<td>Gamma-Lynx</td>
<td>Ja</td>
<td>Neen</td>
</tr>

<tr>
<td>AlphaServer 4000 5/xxx</td>

<td>Wrangler/Durango <i>(Rawhide-familie)</i></td>
<td>Ja</td>
<td>Neen</td>
</tr>

<tr>
<td><a name="as4100">AlphaServer 4100</a> 5/xxx</td>

<td>Dodge <i>(Rawhide-familie)</i></td>
<td>Ja</td>
<td>Neen</td>
</tr>

<tr>
<td>AlphaServer 8200</td>

<td>TurboLaser <i>(Turbolaser-familie)</i></td>
<td>Neen</td>
<td>Neen</td>
</tr>

<tr>
<td>AlphaServer 8400</td>

<td>TurboLaser <i>(Turbolaser-familie)</i></td>
<td>Neen</td>
<td>Neen</td>
</tr>

<tr>
<td>AlphaServer DS10</td>

<td>Slate <i>(Tsunami-familie)</i></td>
<td>Ja</td>
<td>Neen</td>
</tr>

<tr>
<td>AlphaServer DS15</td>

<td>Hyperbrick II <i>(Titan-familie)</i></td>
<td>Ja</td>
<td>Neen</td>
</tr>

<tr>
<td>AlphaServer DS20</td>

<td>Catamaran/Goldrush <i>(Tsunami-familie)</i></td>
<td>Ja</td>
<td>Neen</td>
</tr>

<tr>
<td>AlphaServer DS20E</td>

<td>Goldrack <i>(Tsunami-familie)</i></td>
<td>Ja</td>
<td>Neen</td>
</tr>

<tr>
<td>AlphaServer DS20L</td>

<td>Shark <i>(Tsunami-familie)</i> (orig. CS20 van API)</td>
<td>Ja</td>
<td>Neen</td>
</tr>

<tr>
<td>AlphaServer DS25</td>

<td>Granite <i>(Titan-familie)</i></td>
<td>Ja</td>
<td>Neen</td>
</tr>

<tr>
<td>AlphaServer ES40</td>

<td>Clipper <i>(Tsunami-familie)</i></td>
<td>Ja</td>
<td>Neen</td>
</tr>

<tr>
<td>AlphaServer ES45</td>

<td>Privateer <i>(Titan-familie)</i></td>
<td>Ja</td>
<td>Neen</td>
</tr>

<tr>
<td>AlphaServer ES47</td>

<td>Marvel 2P <i>(Marvel-familie)</i></td>
<td>Ja</td>
<td>Neen</td>
</tr>

<tr>
<td>AlphaServer ES80</td>

<td>Marvel 4x2P <i>(Marvel-familie)</i></td>
<td>Ja</td>
<td>Neen</td>
</tr>

<tr>
<td>AlphaServer GS60</td>

<td>TurboLaser <i>(Turbolaser-familie)</i></td>
<td>Neen</td>
<td>Neen</td>
</tr>

<tr>
<td>AlphaServer GS60E</td>

<td>TurboLaser-Lite <i>(Turbolaser-familie)</i></td>
<td>Neen</td>
<td>Neen</td>
</tr>

<tr>
<td>AlphaServer GS80</td>

<td>Wildfire <i>(Wildfire-familie)</i></td>
<td>Onbekend</td>
<td>Neen</td>
</tr>

<tr>
<td>AlphaServer GS1280</td>

<td>Marvel 8P <i>(Marvel-familie)</i></td>
<td>Neen</td>
<td>Neen</td>
</tr>

<tr>
<td>AlphaServer GS140</td>

# TL67 ?
<td>Turbo-Laser <i>(Turbolaser-familie)</i></td>
<td>Neen</td>
<td>Neen</td>
</tr>

<tr>
<td>AlphaServer GS160</td>

<td>Wildfire <i>(Wildfire-familie)</i></td>
<td>Onbekend</td>
<td>Neen</td>
</tr>

<tr>
<td>AlphaServer GS320</td>

<td>Wildfire <i>(Wildfire-familie)</i></td>
<td>Onbekend</td>
<td>Neen</td>
</tr>

<tr>
<td>Alpha XL-233...266</td>

<td>XL <i>(Alpha XL-familie)</i></td>
<td>Ja</td>
<td>Neen</td>
</tr>

<tr>
<td>AXPpci33</td>

<td>Noname <i>(Noname-familie)</i></td>
<td>Ja</td>
<td>Neen</td>
</tr>

<tr>
<td>DEC 2000/300</td>

<td>Jensen <i>(Jensen-familie)</i></td>
<td>Ja</td>
<td>Neen</td>
</tr>

<tr>
<td>DEC 2000/500</td>

<td>Culzen <i>(Jensen-familie)</i></td>
<td>Ja</td>
<td>Neen</td>
</tr>

<tr>
<td>DEC 3000/300</td>

<td>Pelican</td>
<td>Neen</td>
<td>Neen</td>
</tr>

<tr>
<td>DEC 3000/300L</td>

<td>Pelica</td>
<td>Neen</td>
<td>Neen</td>
</tr>

<tr>
<td>DEC 3000/300LX</td>

<td>Pelica+</td>
<td>Neen</td>
<td>Neen</td>
</tr>

<tr>
<td>DEC 3000/300X</td>

<td>Pelican+</td>
<td>Neen</td>
<td>Neen</td>
</tr>

<tr>
<td>DEC 3000/400</td>

<td>Sandpiper</td>
<td>Neen</td>
<td>Neen</td>
</tr>

<tr>
<td>DEC 3000/500</td>

<td>Flamingo</td>
<td>Neen</td>
<td>Neen</td>
</tr>

<tr>
<td>DEC 3000/500X</td>

<td>Hot Pink</td>
<td>Neen</td>
<td>Neen</td>
</tr>

<tr>
<td>DEC 3000/600</td>

<td>Sandpiper+</td>
<td>Neen</td>
<td>Neen</td>
</tr>

<tr>
<td>DEC 3000/700</td>

<td>Sandpiper45</td>
<td>Neen</td>
<td>Neen</td>
</tr>

<tr>
<td>DEC 3000/800</td>

<td>Flamingo II</td>
<td>Neen</td>
<td>Neen</td>
</tr>

<tr>
<td>DEC 3000/900</td>

<td>Flamingo45</td>
<td>Neen</td>
<td>Neen</td>
</tr>

<tr>
<td>DEC 4000 model 610</td>

<td>Fang</td>
<td>Neen</td>
<td>Neen</td>
</tr>

<tr>
<td>DEC 4000 model 710</td>

<td>Cobra</td>
<td>Neen</td>
<td>Neen</td>
</tr>

<tr>
<td>DEC 7000 model 610/180</td>

<td>Laser/Ruby</td>
<td>Neen</td>
<td>Neen</td>
</tr>

<tr>
<td>DEC 7000 model 610/200</td>

<td>Laser/Ruby+</td>
<td>Neen</td>
<td>Neen</td>
</tr>

<tr>
<td>DEC 7000 model 710</td>

<td>Laser/Ruby45</td>
<td>Neen</td>
<td>Neen</td>
</tr>

<tr>
<td>DEC 10000</td>

<td>Blazer/Ruby</td>
<td>Neen</td>
<td>Neen</td>
</tr>

<tr>
<td>DECpc 150</td>

<td>Jensen <i>(Jensen-familie)</i></td>
<td>Ja</td>
<td>Neen</td>
</tr>

<tr>
<td>Digital Server 3300</td>

#Note to translators: Whitebox means that these machines where limited
#to run NT instead of Digital Unix (though they still could be made to
#run Linux :-)) )
<td>Ontmerkt <a href="#as800">Alpha Server 800</a></td>
<td>Ja</td>
<td>Neen</td>
</tr>

<tr>
<td>Digital Server 3300R</td>

<td>Rekmontage Ontmerkt <a href="#as800">Alpha Server 800</a></td>
<td>Ja</td>
<td>Neen</td>
</tr>

<tr>
<td>Digital Server 3305</td>

<td>Ontmerkt <a href="#as800">Alpha Server 800</a></td>
<td>Ja</td>
<td>Neen</td>
</tr>

<tr>
<td>Digital Server 3305R</td>

<td>Rekmontage Ontmerkt <a href="#as800">Alpha Server 800</a></td>
<td>Neen</td>
<td>Neen</td>
</tr>

<tr>
<td>Digital Server 5300</td>

<td>Ontmerkt <a href="#as1200">Alpha Server 1200</a></td>
<td>Ja</td>
<td>Neen</td>
</tr>

<tr>
<td>Digital Server 5305</td>

<td>Ontmerkt <a href="#as1200">Alpha Server 1200</a></td>
<td>Ja</td>
<td>Neen</td>
</tr>

<tr>
<td>Digital Server 7300</td>

<td>Ontmerkt <a href="#as4100">Alpha Server 4100</a></td>
<td>Ja</td>
<td>Neen</td>
</tr>

<tr>
<td>Digital Server 7305</td>

<td>Ontmerkt <a href="#as4100">Alpha Server 4100</a></td>
<td>Onbekend</td>
<td>Neen</td>
</tr>

<tr>
<td>Digital Server 7310</td>

<td>Ontmerkt <a href="#as4100">Alpha Server 4100</a></td>
<td>Onbekend</td>
<td>Neen</td>
</tr>

<tr>
<td>DP264</td>

<td>DP264 <i>(Tsunami-familie)</i></td>
<td>Ja</td>
<td>Neen</td>
</tr>

<tr>
<td>EB64+ (PCI Eval Board)</td>

<td>EB64+ <i>(EB64+ familie)</i></td>
<td>Ja</td>
<td>Neen</td>
</tr>

<tr>
<td>EB66</td>

<td>EB66 <i>(EB66-familie)</i></td>
<td>Ja</td>
<td>Neen</td>
</tr>

<tr>
<td>EB66+</td>

<td>EB66+ <i>(EB66-familie)</i></td>
<td>Ja</td>
<td>Neen</td>
</tr>

<tr>
<td>EB164</td>

<td>EB164 <i>(EB164-familie)</i></td>
<td>Ja</td>
<td>Neen</td>
</tr>

<tr>
<td>Personal WorkStation 433a</td>

<td>Miata <i>(Miata-familie)</i></td>
<td>Ja</td>
<td>Neen</td>
</tr>

<tr>
<td>Personal WorkStation 433au</td>

<td>Miata <i>(Miata-familie)</i></td>
<td>Ja</td>
<td>Neen</td>
</tr>

<tr>
<td>Personal WorkStation 500a</td>

<td>Miata <i>(Miata-familie)</i></td>
<td>Ja</td>
<td>Neen</td>
</tr>

<tr>
<td>Personal WorkStation 500au</td>

<td>Miata <i>(Miata-familie)</i></td>
<td>Ja</td>
<td>Neen</td>
</tr>

<tr>
<td>Personal WorkStation 600a</td>

<td>Miata <i>(Miata-familie)</i></td>
<td>Ja</td>
<td>Neen</td>
</tr>

<tr>
<td>Personal WorkStation 600au</td>

<td>Miata <i>(Miata-familie)</i></td>
<td>Ja</td>
<td>Neen</td>
</tr>

<tr>
<td>RPL164-2</td>

<td>Ruffian <i>(geproduceerd door DeskStation)</i></td>
<td>Ja</td>
<td>Neen</td>
</tr>

<tr>
<td>RPL164-4</td>

<td>Ruffian <i>(geproduceerd door DeskStation)</i></td>
<td>Ja</td>
<td>Neen</td>
</tr>

<tr>
<td>RPX164-2</td>

<td>Ruffian <i>(geproduceerd door DeskStation)</i></td>
<td>Ja</td>
<td>Neen</td>
</tr>

<tr>
<td>RPX164-4</td>

<td>Ruffian <i>(geproduceerd door DeskStation)</i></td>
<td>Ja</td>
<td>Neen</td>
</tr>

<tr>
<td>SMARTengine 21264 PCI/ISA SBC</td>

<td>Eiger <i>(Tsunami-familie, maar met 1 p-chip)</i></td>
<td>Ja</td>
<td>Neen</td>
</tr>

<tr>
<td>UDB/Multia</td>

<td>UDB/Multia <i>(Noname-familie)</i></td>
<td>Ja</td>
<td>Neen</td>
</tr>

<tr>
<td>UP1000</td>

<td>Nautilus <i>(Nautilus-familie)</i></td>
<td>Ja</td>
<td>Neen</td>
</tr>

<tr>
<td>UP1100</td>

<td>Galaxy-Train/Nautilus Jr. <i>(Nautilus-familie)</i></td>
<td>Ja</td>
<td>Neen</td>
</tr>

<tr>
<td>UP2000</td>

<td>Swordfish <i>(Tsunami-familie)</i></td>
<td>Ja</td>
<td>Neen</td>
</tr>

<tr>
<td>XP900</td>

<td>Webbrick <i>(Tsunami-familie)</i></td>
<td>Ja</td>
<td>Neen</td>
</tr>

<tr>
<td>XP1000</td>

<td>Monet/Brisbane <i>(Tsunami-familie)</i></td>
<td>Ja</td>
<td>Neen</td>
</tr>

<tr>
<td>AlphaVME 4/xxx (<abbr lang="nl" title="original equipment manufacturer - fabrikant van originele apparatuur">OEM</abbr>-segment)</td>

<td>Cortex</td>
<td>Neen</td>
<td>Neen</td>
</tr>

#Most likely never produced:
<tr>
<td>Onbekend</td>

<td>Cusco</td>
<td>Neen</td>
<td>Neen</td>
</tr>

<tr>
<td>AXPvme xxx (<abbr lang="nl" title="original equipment manufacturer - fabrikant van originele apparatuur">OEM</abbr>-segment)</td>

<td>Medulla</td>
<td>Onbekend</td>
<td>Neen</td>
</tr>

#Was cancelled:
<tr>
<td>Onbekend</td>

<td>Tradewind</td>
<td>Neen</td>
<td>Neen</td>
</tr>

#Never produced:
<tr>
<td>Onbekend</td>

<td>Warhol <i>(Tsunami-familie)</i></td>
<td>Ja</td>
<td>Neen</td>
</tr>

#Never produced:
<tr>
<td>Onbekend</td>

<td>Windjammer <i>(Tsunami-familie)</i></td>
<td>Onbekend</td>
<td>Neen</td>
</tr>

#Never produced:
<tr>
<td>Onbekend</td>

<td>PC264 <i>(Tsunami-familie)</i></td>
<td>Onbekend</td>
<td>Neen</td>
</tr>

#Ever produced?
<tr>
<td>Onbekend</td>

<td>XXM</td>
<td>Neen</td>
<td>Neen</td>
</tr>

<tr>
<td>AlphaVME 5/xxx (<abbr lang="nl" title="original equipment manufacturer - fabrikant van originele apparatuur">OEM</abbr>-segment)</td>

<td>Yukon <i>(Titan-familie)</i></td>
<td>Ja</td>
<td>Neen</td>
</tr>

</table>

