#use wml::debian::template title="Informações de lançamento do Debian &ldquo;buster&rdquo;"
#include "$(ENGLISHDIR)/releases/info"
#include "$(ENGLISHDIR)/releases/buster/release.data"
#include "$(ENGLISHDIR)/releases/arches.data"
#use wml::debian::translation-check translation="6bafc9758f46d9e4fe5b48e5d85484d9f3dc8e3e"

<p>O Debian <current_release_buster> foi lançado em
<a href="$(HOME)/News/<current_release_newsurl_buster/>"><current_release_date_buster></a>.
<ifneq "10.0" "<current_release>"
  "O Debian 10.0 foi inicialmente lançado em <:=spokendate('2019-07-06'):>."
/>
O lançamento incluiu muitas mudanças importantes, descritas no
nosso <a href="$(HOME)/News/2019/20190706">comunicado à imprensa</a> e
nas <a href="releasenotes">notas de lançamento</a>.</p>

<p><strong>O Debian 10 foi substituído pelo
<a href="../bullseye/">Debian 11 (<q>bullseye</q>)</a>.
Atualizações de segurança foram descontinuadas em <:=spokendate('2022-06-30'):>.
</strong></p>

<p><strong>No entanto, o buster se beneficia do suporte de longo prazo (Long
Term Support - LTS) até o final de junho de 2024. O LTS é limitado a i386,
amd64, armhf e arm64.
Todas as outras arquiteturas não são mais suportadas no buster.
Para obter mais informações, por favor consulte a
<a href="https://wiki.debian.org/LTS">seção LTS da Wiki do Debian</a>.
</strong></p>

<p>Para obter e instalar o Debian, veja a página de
<a href="debian-installer/">informações de instalação</a> e o
<a href="installmanual">guia de instalação</a>. Para atualizar a partir de uma
versão mais antiga do Debian, veja as instruções nas
<a href="releasenotes">notas de lançamento</a>.</p>

###  Ative o seguinte, quando o período LTS começar.
#<p>Arquiteturas suportadas durante o suporte de longo prazo:</p>
#
#<ul>
#<:
#foreach $arch (@archeslts) {
#	print "<li><a href=\"$(HOME)/ports/$arch/\">$arches{$arch}</a>\n";
#}
#:>
#</ul>

<p>As seguintes arquiteturas de computadores são suportadas nesta versão:</p>
# <p>Computer architectures supported at initial release of buster:</p> ### Use this line when LTS starts, instead of the one above.

<ul>
<:
foreach $arch (@arches) {
	print "<li><a href=\"$(HOME)/ports/$arch/\">$arches{$arch}</a>\n";
}
:>
</ul>

<p>Apesar de nossos desejos, podem existir alguns problemas nesta versão,
embora ela tenha sido declarada <em>estável (stable)</em>. Fizemos
<a href="errata">uma lista dos problemas conhecidos mais importantes</a>,
e você sempre pode <a href="../reportingbugs">relatar outros problemas</a>
para nós.</p>

<p>Por último, mas não menos importante, nós temos uma lista de <a
href="credits">pessoas que merecem crédito</a> por fazer este
lançamento acontecer.</p>

