#use wml::debian::template title="Informações de lançamento do Debian GNU/Linux 2.2 ('potato')" BARETITLE=yes
#use wml::debian::release
#include "$(ENGLISHDIR)/releases/info"
#use wml::debian::translation-check translation="dc28c67a4a9013b56f1b40512f11f8af875d6a6c"

<p>O Debian GNU/Linux 2.2 (a.k.a. Potato) foi lançado em
<:=spokendate ("2000-08-14"):>. A última versão do 2.2 é a
<current_release_potato>, lançada em <a href="$(HOME)/News/<current_release_newsurl_potato/>"><current_release_date_potato></a>.</p>

<p><strongO>O Debian GNU/Linux 2.2 foi substituído pelo
<a href="../woody/">Debian GNU/Linux 3.0 ("woody")</a>.
As atualizações de segurança foram descontinuadas a partir de 30 de junho de 2003.
</strong> Por favor, consulte
<a href="https://lists.debian.org/debian-devel-announce/2003/debian-devel-announce-200302/msg00010.html">\
o resultado da pesquisa da equipe de segurança</a> para mais informações.</p>

<p>Para informações sobre grandes mudanças dessa versão, consulte as
<a href="releasenotes">notas de lançamento</a> e o
<a href="$(HOME)/News/2000/20000815">comunicado à imprensa</a> oficial.</p>

<p>O Debian GNU/Linux 2.2 é dedicado à memória de Joel "Espy" Klecker,
um desenvolvedor Debian, conhecido pela maioria dos participantes do
projeto Debian. Ele estava de cama, lutando contra uma doença conhecida
como Distrofia Muscular Duchene durante a maior parte do tempo em que
se envolveu com o Debian. Somente agora o Projeto Debian percebeu
a extensão de sua dedicação e a amizade que ele conferiu a nós. Então,
como uma mostra de apreciação e em memória de sua vida inspiradora, esse
lançamento do Debian GNU/Linux é dedicado a ele.</p>

<p>O Debian GNU/Linux 2.2 está disponível através da Internet ou através de
vendedores(as) de CD. Por favor, consulte a <a href="$(HOME)/distrib/">página da
distribuição</a> para maiores informações sobre como obter o Debian.</p>

<p>As seguintes arquiteturas são suportadas nesse lançamento:</p>

<ul>
<li><a href="/ports/alpha/">alpha</a>
<li><a href="/ports/arm/">arm</a>
<li><a href="/ports/i386/">i386</a>
<li><a href="/ports/m68k/">m68k</a>
<li><a href="/ports/powerpc/">powerpc</a>
<li><a href="/ports/sparc/">sparc</a>
</ul>

<p>Antes de instalar o Debian, por favor leia o manual de instalação.
O manual de instalação para a arquitetura desejada
contém instruções e links para todos os arquivos que você necessita para a
instalação. Você pode também se interessar pelo guia de instalação do Debian
2.2, que é um tutorial online.</p>

<p>Se você está usando o APT, você pode usar as linhas a seguir no seu
arquivo <code>/etc/apt/sources.list</code> para poder acessar os pacotes
do potato:</p>

<pre>
  deb http://archive.debian.org potato main contrib non-free
  deb http://non-us.debian.org/debian-non-US potato/non-US main non-free
</pre>

<p>Leia as páginas de manual do <code>apt-get</code>(8) e do
<code>sources.list</code>(5) para mais informações.</p>

<p>Apesar dos nossos desejos, existem alguns problemas na versão potato,
embora ela tenha sido declarada <em>estável (stable)</em>. Fizemos
<a href="errata">uma lista dos problemas conhecidos mais importantes</a>,
e você sempre pode relatar outros problemas para nós.</p>
