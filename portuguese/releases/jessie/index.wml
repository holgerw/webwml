#use wml::debian::template title="Informações de lançamento do Debian &ldquo;jessie&rdquo;"
#include "$(ENGLISHDIR)/releases/info"
#use wml::debian::translation-check translation="ff4acdb89311338bebdbb71b28dcc479d29029e3"

<p>O Debian <current_release_jessie> foi
lançado em <a href="$(HOME)/News/<current_release_newsurl_jessie/>"><current_release_date_jessie></a>.
<ifneq "8.0" "<current_release>"
  "O Debian 8.0 foi incialmente lançado em <:=spokendate('2015-04-26'):>."
/>

O lançamento incluiu muitas
mudanças importantes, descritas no
nosso <a href="$(HOME)/News/2015/20150426">comunicado à imprensa</a> e
nas <a href="releasenotes">notas de lançamento</a>.</p>

<p><strong>Debian 8 foi substituído pelo
<a href="../stretch/">Debian 9 (<q>stretch</q>)</a>.
As atualizações de segurança foram descontinuadas em
<:=spokendate('2018-06-17'):>.
</strong></p>

<p><strong>O Jessie também se beneficiou do suporte de longo prazo (Long Term
Support - LTS) até o final de junho de 2020. O LTS era limitado a i386, amd64,
armel e armhf.
Para mais informações, consulte a
<a href="https://wiki.debian.org/LTS">seção LTS da Wiki do Debian</a>.
</strong></p>

<p>Para obter e instalar o Debian, veja a página de informações de instalação e
o guia de instalação. Para atualizar a partir de uma versão mais antiga do
Debian, veja as instruções nas <a href="releasenotes">notas de lançamento</a>.</p>

<p>Arquiteturas suportadas durante o período de suporte do LTS:</p>

<ul>
<li><a href="../../ports/amd64/">64-bit PC (amd64)</a>
<li><a href="../../ports/i386/">32-bit PC (i386)</a>
<li><a href="../../ports/armel/">EABI ARM (armel)</a>
<li><a href="../../ports/armhf/">Hard Float ABI ARM (armhf)</a>
</ul>

<p>No lançamento inicial do Jessie, essas arquiteturas eram suportadas:</p>

<ul>
<li><a href="../../ports/amd64/">64-bit PC (amd64)</a>
<li><a href="../../ports/i386/">32-bit PC (i386)</a>
<li><a href="../../ports/armel/">EABI ARM (armel)</a>
<li><a href="../../ports/powerpc/">PowerPC</a>
<li><a href="../../ports/armhf/">Hard Float ABI ARM (armhf)</a>
<li><a href="../../ports/mipsel/">MIPS (little endian)</a>
<li><a href="../../ports/mips/">MIPS (big endian)</a>
<li><a href="../../ports/s390x/">IBM System z</a>
<li><a href="../../ports/arm64/">64-bit ARM (AArch64)</a>
<li><a href="../../ports/ppc64el/">POWER Processors</a>
</ul>

<p>Apesar dos nossos desejos, podem existir alguns problemas nesta versão,
embora ela tenha sido declarada <em>estável (stable</em>). Fizemos
<a href="errata">uma lista dos problemas conhecidos mais importantes</a>,
e você sempre pode relatar outros problemas para nós.</p>
