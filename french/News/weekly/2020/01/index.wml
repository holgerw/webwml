#use wml::debian::projectnews::header PUBDATE="2020-09-09" SUMMARY="Bienvenue sur les Nouvelles du projet Debian, les canaux de communication de Debian, nouveau chef de projet pour Debian, nouvelles internes et extérieures, DebConf20 et événements, comptes-rendus, demandes d'aide"
#use wml::debian::acronyms
#use wml::debian::translation-check translation="de00676864ba33b45c4e42789e4ea68c0bacd001" maintainer="Jean-Pierre Giraud"
# Status: [sent]


## substitute XXX with the number (expressed in letter) of the issue.
## please note that the var issue is not automagically localized, so
## translators need to put it directly in their language!
## example: <intro issue="fourth" />

## Use &#35; to escape # for IRC channels
## Use &#39 for single quotes

<intro issue="premier" />

<toc-add-entry name="newdpn">Bienvenue sur les Nouvelles du projet Debian !</toc-add-entry>

<p>Nous espérons que vous appréciez cette <b>Édition spéciale d'archive</b> des
nouvelles du projet Debian qui reprend, présente et tente de faire un résumé
de la plupart des nouvelles de l'année 2020 jusqu'à aujourd'hui.</p>

<p>
Dans cette édition spéciale des nouvelles, nous n'avons pas repris certaines
de nos rubriques régulières, nous efforçant d'actualiser notre couverture. Si
vous souhaitez retrouver des informations plus anciennes ou que vous avez
ratées, veuillez consulter le blog officiel de Debian
<a href="https://bits.debian.org">Bits from Debian</a> ou le service
<a href="https://micronews.debian.org">Debian Micronews</a> où tous les sujets
des nouvelles récentes et actuelles ont déjà été partagés durant cette année
calendaire.
</p>

<toc-add-entry name="official">Les canaux de communication officiels de Debian</toc-add-entry>

<p>
De temps à autre, nous sommes interrogés sur les événements en cours, les règles
qui définissent qui peut posséder des sites internet dans l'espace de noms Debian,
l'avancement du développement dans la communauté ou sur les canaux officiels de
et pour Debian.
</p>

<p>
La section <a href="https://www.debian.org/News/">Actualités</a> du site web de
Debian transmet des informations et des annonces officielles dont beaucoup sont
copiées ou partagées à partir des listes de diffusion debian-news et
debian-announce.
</p>

<p>
Le blog <a href="https://bits.debian.org/">Bits from Debian</a> fournit des
nouvelles des nombreuses listes de diffusion de Debian ainsi que des
informations et des annonces semi-officielles avec un cycle de publication de
nouvelles beaucoup plus rapide.
</p>

<p>
Le service <a href="https://micronews.debian.org/">Debian Micronews</a> couvre
et diffuse de manière courte les sujets des dernières actualités et à effet
immédiat. Le service Micronews alimente aussi plusieurs fils de réseaux
sociaux tels que Twitter, identi.ca et framapiaf.org.
</p>

<p>
Bien sûr, il ne faut pas oublier le média que vous être en train de lire : les
<a href="https://www.debian.org/News/weekly/">Nouvelles du projet Debian</a> sont
la lettre d'information du projet, qui est publiée de façon quelque peu aléatoire (aide
demandée – voir plus bas).
</p>

<p>
Veuillez noter que tous ces canaux de communication sont disponibles à travers
le protocole <a href="https://en.wikipedia.org/wiki/HTTPS"><b>https://</b></a>
que vous voyez utilisé sur tout le site web et tous nos canaux officiels.
</p>

<p>
Si vous souhaitez nous joindre pour des questions sur des sujets d'annonce ou
de nouvelle, n'hésitez pas à nous contacter par courriel à l'adresse
<a href="mailto:press@debian.org">press@debian.org</a>. Veuillez bien noter que
les équipes presse et publicité <i>ne fournissent que ces services d'information</i>
et ne peuvent pas fournir une assistance directe aux utilisateurs. Toutes les
demandes en ce sens seront transmises aux
<a href="https://lists.debian.org/debian-user/">listes de diffusion debian-user</a>
ou à <a href="https://wiki.debian.org/Teams">l'équipe Debian</a> appropriée.
</p>

<toc-add-entry name="helpspread">L'équipe publicité demande des volontaires et de l'aide !</toc-add-entry>

<p>L'équipe publicité vous demande de l'aide, à vous nos lecteurs, nos développeurs
et à toutes les personnes intéressées pour contribuer à la réussite des nouvelles
de Debian. Nous vous demandons de nous soumettre des sujets susceptibles d'intéresser
la communauté et sollicitons votre assistance pour la traduction des nouvelles
dans une autre langue (la vôtre !) et aussi pour les relectures nécessaires pour
amender notre travail avant publication. Si vous pouvez donner un peu de votre
temps pour aider notre équipe qui fait tout son possible pour nous tenir tous
informés, nous avons besoin de vous. Veuillez nous joindre sur le canal IRC
sur <a href="irc://irc.debian.org/debian-publicity">&#35;debian-publicity</a> sur
<a href="https://oftc.net/">OFTC.net</a>, sur notre
<a href="mailto:debian-publicity@lists.debian.org">liste de diffusion publique</a>
ou par courriel à l'adresse <a href="mailto:press@debian.org">press@debian.org</a>
pour des requêtes difficiles ou privées.</p>

<toc-add-entry name="longlivetheking">Un nouveau chef du projet Debian élu</toc-add-entry>

<p>Jonathan Carter (highvoltage), le nouveau chef du projet (DPL) élu, partage ses
réflexions, ses remerciements et ses grandes lignes pour l'avenir de Debian dans
son premier
<a href="https://lists.debian.org/debian-devel-announce/2020/04/msg00013.html">article de blog</a>
officiel en tant que DPL. D'autres articles du DPL ont aussi été 
<a href="https://bits.debian.org/dpl/">envoyés</a> sur le blog officiel de Debian.
</p>

<toc-display/>

<toc-add-entry name="security">Annonces de sécurité</toc-add-entry>

<p>L'équipe de sécurité de Debian diffuse au jour le jour les annonces de
sécurité (<a href="$(HOME)/security/2020/">annonces de sécurité 2020</a>).
Nous vous conseillons de les lire avec attention et de vous inscrire à la
<a href="https://lists.debian.org/debian-security-announce/">liste de diffusion correspondante</a>.</p>

<p>Le site web de Debian <a href="https://www.debian.org/lts/security/">archive</a> maintenant également
les annonces de sécurité produites par l'équipe de suivi à long terme de Debian
(Debian LTS) et envoyées à la 
 <a href="https://lists.debian.org/debian-lts-announce/">liste de diffusion debian-lts-announce</a>.
</p>

<toc-add-entry name="internal">Nouvelles internes</toc-add-entry>

<p><b>Nouvelles de Debian 10 <q>Buster</q> : la version 10.5 publiée</b></p>

<p>En juillet 2019, nous avons accueilli
<a href="https://www.debian.org/News/2019/20190706">la publication de Debian 10
(nom de code <q>Buster</q>)</a>. Depuis lors, 
le projet Debian a annoncé les
<a href="https://www.debian.org/News/2019/20190907">première</a>,
<a href="https://www.debian.org/News/2019/20191116">seconde</a>,
<a href="https://www.debian.org/News/2020/20200208">troisième</a>,
<a href="https://www.debian.org/News/2020/20200509">quatrième</a>,
et <a href="https://www.debian.org/News/2020/20200801">cinquième</a>
mises à jour de la distribution stable.
Debian 10.5 a été publiée le 1er août 2020.</p>

<p><b>Mise à jour de Debian 9 :la version 9.13 publiée</b></p>

<p>Le projet Debian a annoncé les
<a href="https://www.debian.org/News/2019/2019090702">dixième</a>,
<a href="https://www.debian.org/News/2019/20190908">onzième</a>,
<a href="https://www.debian.org/News/2020/2020020802">douzième</a>,
et <a href="https://www.debian.org/News/2020/20200718">treizième</a>
(et dernière) mises à jour de l'ancienne distribution stable Debian 9
(nom de code <q>Stretch</q>).
Debian 9.13 a été publiée le 18 juillet 2020.</p>

<p>Le suivi de sécurité pour la version <q>oldstable</q> Debian 9 a pris fin le
6 juillet 2020. Néanmoins, <q>Stretch</q> bénéficiera du suivi à long terme (LTS)
<a href="https://wiki.debian.org/LTS/Stretch">jusqu'à la fin du mois de juin 2022</a>.
Le suivi à long terme est limité aux architectures i386, amd64, armel, armhf
et arm64.
</p>

<p><b>Annonce de la fin de vie du suivi à long terme de Debian 8</b></p>

<p>Le 30 juin 2020, le suivi à long terme de Debian 8 <q>Jessie</q> a atteint
sa fin de vie, cinq ans après sa première publication le 26 avril 2015.</p>

<p><b>Nouvelles de Debian 11 <q>Bullseye</q></b></p>

<p>Paul Gevers de l'équipe de publication nous fait
<a href="https://lists.debian.org/debian-devel-announce/2020/03/msg00002.html">partager</a>
la politique à venir pour notre prochaine publication, les dates provisoires du
gel ainsi que d'autres mises à jour et modifications. Pour plus d'informations
concernant la manière dont cette politique et ces modifications en sont arrivées
là, veuillez consulter cette ancienne annonce <a
href="https://lists.debian.org/debian-devel-announce/2019/07/msg00002.html">Brèves de l'équipe de publications</a>.</p>

<p>Aurelien Jarno a proposé le
<a href="https://lists.debian.org/debian-devel-announce/2019/08/msg00003.html">retrait de l'architecture mips</a>
pour la prochaine publication parce que l'effort de portage est devenu de plus
en plus difficile. Ce retrait n'affecte ni <q>Stretch</q>, ni <q>Buster</q>.
</p>

<p>Dans la perspective du retrait de Python 2 pour la publication de <q>Bullseye</q>,
l'équipe debian-python donne des nouvelles sur <a
href="https://lists.debian.org/debian-devel-announce/2019/11/msg00000.html">les progrès et les prochaines étapes</a>
du processus. La <a href="https://wiki.debian.org/Python/2Removal">page dédiée du wiki</a>
donne davantage de détails.</p>

<p>La version Alpha 1 de l'installateur pour Debian Bullseye a été
<a href="https://www.debian.org/devel/debian-installer/News/2019/20191205.html">publiée</a>
le 5 décembre 2019.</p>

<p>Cyril Brulebois a partagé des informations sur la publication de la <a
href="https://lists.debian.org/debian-devel-announce/2020/03/msg00005.html">version Alpha 2 de l'installateur pour Debian Bullseye</a>,
publiée le 16 mars 2020, qui propose de nombreuses améliorations et modifications
pour clock-setup, grub-installer, preseed et systemd parmi d'autres avancées. Les
tests et rapports pour trouver des bogues et améliorer l’installateur sont
bienvenus. Les images de l’installateur et tout le reste sont
<a href="https://www.debian.org/devel/debian-installer">disponibles au
téléchargement</a>.

<p><b>Un nouveau paquet de régionalisation pour les pages de manuel</b></p>

<p>Historiquement, les pages de manuel d'un paquet donné ont souvent leurs
traductions fournies dans divers autres paquets, utilisant diverses méthodes,
équipes et formats. La plupart ne sont plus entretenues. La plupart des pages
de manuel essentielles étaient auparavant traduites pour garder les pages de
manuel à jour.</p>

<p>Debian se propose d'empaqueter un nouveau paquet
<a href="https://lists.debian.org/debian-l10n-french/2020/02/msg00105.html">manpages-l10n</a>
qui remplacera Perkamon, manpages-de, manpages-fr, manpages-fr-extra,
manpages-pl, manpages-pl-dev, manpages-pt et manpages-pt-dev. En plus, ce
nouveau paquet fournira la traduction des pages de manuel en néerlandais et en
roumain. Le résultat est que désormais les utilisateurs de langue allemande,
française, néerlandaise, polonaise, portugaise et roumaine auront
potentiellement les pages de manuel traduites dans un paquet unique sur lequel
ils pourront contribuer, rapporter des bogues, etc. Nous vous invitons à rejoindre
cette initiative, mais gardez à l'esprit que quand les paquets ne
sont plus entretenus, le plus important est d'abord de les convertir en
fichiers PO et de les mettre à jour.</p>

<p><b>Clés DKIM</b></p>

<p>Adam D. Barratt transmet l'information d'une avancée récente de l'infrastructure
Debian avec l'amélioration pour les développeurs Debian fournie par l'utilisation
de <a href="https://lists.debian.org/debian-devel-announce/2020/04/msg00004.html">clés DKIM pour l'authentification du courrier</a>.</p>

<p><b>Modifications de l'authentification dans Salsa</b></p>

<p>Enrico Zini a annoncé que les <a
href="https://lists.debian.org/debian-devel-announce/2020/04/msg00007.html">connexions à Salsa sont activées dans nm.debian.org</a>.
Cette modification en facilite l'utilisation pour les candidats, les responsables
de paquet et les nouveaux développeurs. À la suite de ce changement, Bastian
Blank de l'équipe des administrateurs de Salsa a annoncé une
<a href="https://lists.debian.org/debian-devel-announce/2020/04/msg00010.html">mise à jour de Salsa : la suppression des comptes -guest</a>
pour utiliser Salsa comme fournisseur d'authentification.</p>

<p>Sean Whitton a publié
<a href="https://www.debian.org/doc/debian-policy/">Debian Policy 4.5.0.2</a>
qui apporte des mises à jour sur les noms d'utilisateur générés,
l'utilisation des scripts d'unités de service et sur les nouveautés d'update-rc.d.</p>


<p><b>Brèves du comité technique</b></p>

<p>Le comité technique lors de la préparation de sa communication pour DebConf20
intitulée
<a href="https://debconf20.debconf.org/talks/16-meet-the-technical-committee/">Meet the Technical Committee</a>
a publié son
<a href="https://lists.debian.org/debian-devel-announce/2020/08/msg00005.html">rapport annuel</a>
et un <a href="https://salsa.debian.org/debian/tech-ctte/-/blob/master/talks/rethinking-the-tc.md">document de propositions</a>
pour l'amélioration des tâches et des procédures du comité technique.
</p>

<p><b>Nouvelle équipe Debian Academy</b></p>

<p>
Nouvelle initiative pour définir et utiliser un apprentissage en ligne officiel
de Debian avec des cours spécifiques pour Debian. Voulez-vous aider à ce que
cela puisse aboutir ? Consultez la 
<a href="https://wiki.debian.org/DebianAcademy">page de wiki</a>
et rejoignez l’équipe.
</p>

<toc-add-entry name="external">Nouvelles externes</toc-add-entry>

<p><b>Debian France a renouvelé ses instances</b></p>

<p>Debian France est une organisation sans but lucratif qui fait partie des
organismes habilités. C'est le principal organisme habilité en Europe.
L'association vient de renouveler son bureau. Il y a eu peu de changement
à l'exception du président qui est maintenant Jean-Philippe MENGUAL.</p>

<p>Il faut se souvenir que cette association sans but lucratif ne compte dans
son bureau que des développeurs Debian en tant que président, secrétaire et
trésorier. Les autres administrateurs ne sont pas tous des développeurs Debian,
mais tous contribuent à Debian notamment sur ses stands et lors d'événements.
<a href="https://france.debian.net/posts/2020/renouvellement_instances/">Voir l'annonce</a>.
</p>


<toc-add-entry name="MiniDebs">MiniDebConfs, MiniDebCamps et DebConf20</toc-add-entry>

<p><b>MiniDebConfs et MiniDebCamps</b></p>
<p>
Au début de l'année 2020, sept <a href="https://wiki.debian.org/MiniDebConf">MiniDebConfs</a>
étaient programmées dans le monde entier, reflétant la vitalité de notre
communauté. Organisées par des membres du projet Debian, les MiniDebConfs sont
ouvertes à tous et fournissent aux développeurs, aux contributeurs et aux autres
personnes intéressées l'occasion de se rencontrer. Mais du fait des contraintes
liées à la pandémie du coronavirus (COVID-19), la plupart de ces conférences,
sinon toutes, ont été annulées ou reportées en 2021 :

<a href="https://wiki.debian.org/DebianEvents/gb/2020/MiniDebConfAberdeen">Aberdeen (Écosse)</a>,
<a href="https://wiki.debian.org/DebianEvents/fr/2020/Bordeaux">Bordeaux (France)</a>,
<a href="https://wiki.debian.org/DebianLatinoamerica/2020/MiniDebConfLatAm">El Salvador</a>,
<a href="https://maceio2020.debian.net">Maceió (Brésil)</a> et
<a href="https://lists.debian.org/debian-devel-announce/2020/02/msg00006.html">Regensburg (Allemagne)</a>.
Certaines ont été reprogrammées en ligne : la première,
<a href="https://wiki.debian.org/DebianEvents/internet/2020/MiniDebConfOnline">MiniDeconfOnline n° 1</a>,
précédée d'un DebCamp, s'est tenue du 28 au 31 mai 2020. La 
<a href="https://wiki.debian.org/DebianEvents/internet/2020/MiniDebConfOnline2">MiniDebConf Online n° 2 « Gaming Edition »</a>,
consacrée au jeu sur Debian (et sur Linux en général) aura lieu du 19 au
22 novembre 2020 – un DebCamp se tiendra les deux premiers jours. Consultez la
<a href="https://wiki.debian.org/DebianEvents/internet/2020/MiniDebConfOnline2">page wiki de l'événement</a>
pour plus de détails.</p>

<p><b>DebConf20 en ligne, DebConf21 à Haïfa, Israël</b></p>

<p>
<a href="https://www.debian.org/News/2020/20200830">DebConf20</a> s'est tenue
pour la première fois en ligne du fait de la pandémie du coronavirus (COVID-19).</p>

<p>La communauté Debian s'est adaptée à ce changement en poursuivant le partage
des idées, des sessions spécialisées (« Birds of a Feather » – BoF), des
discussions et toutes les activités traditionnelles que nous avons développées
au fil du temps. Toutes les sessions ont été diffusées en direct à travers
différents canaux pour participer à la conférence : messagerie IRC, édition de
texte collaboratif en ligne et salons de visioconférence.
</p> 

<p>Avec plus de 850 participants de 80 pays différents et un total de plus de
100 présentations, sessions de discussion, sessions spécialisées et autres
activités <a href="https://debconf20.debconf.org">DebConf20</a> a été un énorme
succès. Le programme de DebConf20 comprenait également deux programmes
particuliers en une autre langue que l'anglais : la MiniConf en espagnol et la
MiniConf en malayalam.</p>

<p>La plupart des communications et des sessions sont disponibles sur le <a
href="https://meetings-archive.debian.net/pub/debian-meetings/2020/DebConf20/">site web des réunions Debian</a>
et le <a href="https://debconf20.debconf.org/">site web de DebConf20</a> restera
actif à fin d'archive et continuera à offrir des liens vers les présentations et
vidéos des communications et des événements.
</p>

<p>
Il est prévu que l'année prochaine, <a href="https://wiki.debian.org/DebConf/21">DebConf21</a>
se tienne à Haïfa, Israël, en août ou en septembre.
</p>

<toc-add-entry name="debday">Journée Debian</toc-add-entry>

<p>27 ans ! Joyeux anniversaire ! Joyeux <a href="https://wiki.debian.org/DebianDay/2020">#DebianDay!</a>
À cause de la pandémie mondiale du Covid-19, moins d'événements locaux ont été
organisés pour la journée Debian et certains d'entre eux ont été organisés en ligne
au <a href="https://wiki.debian.org/Brasil/Eventos/DebianDayBrasil2020">Brésil</a>
(les 15 et 16 août). Néanmoins, des développeurs et des contributeurs ont eu
l'occasion de se rencontrer à La Paz (Bolivie), České Budějovice (Tchéquie)
et à Haïfa (Israël) pendant DebConf20.
</p>

<toc-add-entry name="otherevents">Autres évènements</toc-add-entry>

<p>
La communauté brésilienne de Debian a organisé du 3 au 6 juin 2020, un évènement
en ligne appelé 
<a href="https://debianbrasil.gitlab.io/FiqueEmCasaUseDebian/">#FiqueEmCasaUseDebian (#StayHomeUseDebian)</a>.
Pendant 27 nuits, les développeurs Daniel Lenharo et Paulo Santana (phls) ont
accueilli des invités qui ont partagé leur savoir à propos de Debian. Vous
pouvez lire un
<a href="http://softwarelivre.org/debianbrasil/blog/fiqueemcasausedebian-it-was-35-days-with-talks-translations-and-packaging">compte rendu</a>.
</p>

<toc-add-entry name="reports">Comptes rendus</toc-add-entry>
## It's easier to link to the monthly reports for the LTS section and the RB links rather than
# summarize each report or number of packages. Feel free to input this information if you need to fill
# the issue out
#

<p><b>Comptes rendus mensuels de Freexian sur Debian Long Term Support (LTS)</b></p>

<p>Freexian publie <a href="https://raphaelhertzog.com/tag/Freexian+LTS/">des comptes rendus mensuels</a>
sur le travail réalisé par les contributeurs salariés pour le suivi à long
terme de sécurité de Debian.
</p>

<p><b>État d'avancement des compilations reproductibles</b></p>

<p>Suivez le <a href="https://reproducible-builds.org/blog/">blog des compilations reproductibles</a>
pour obtenir un compte rendu de leur travail sur le cycle de <q>Buster</q>.
</p>


<toc-add-entry name="help">Demandes d'aide !</toc-add-entry>
#Make bold -fix
<p><b>Équipes ayant besoin d’aide</b></p>
## Teams needing help
## $Link to the email from $Team requesting help

<p>Le manuel Référence du développeur Debian est maintenant entretenu au
format ReStructuredText. Les nouvelles traductions et les mises à jour
sont les bienvenues. Veuillez consulter l'annonce dans les
<a href="https://lists.debian.org/debian-devel-announce/2019/08/msg00003.html">Nouvelles diverses
pour les développeurs</a> et le fil
<a href="https://lists.debian.org/debian-devel/2020/02/msg00500.html">traductions de developers-reference</a>
pour plus d'informations.</p>

<p><b>Appel à propositions pour le thème de Bullseye</b></p>

<p>Jonathan Carter a lancé <a
href="https://lists.debian.org/debian-devel-announce/2020/08/msg00002.html">l'appel à propositions
officiel pour le thème de Bullseye</a>.
Pour les détails les plus à jour, veuillez consulter le <a
href="https://wiki.debian.org/DebianDesktop/Artwork/Bullseye">wiki</a>. 
Dans le même temps, nous aimerions remercier Alex Makas pour avoir réalisé le <a
href="https://wiki.debian.org/DebianArt/Themes/futurePrototype">thème futurePrototype
pour Buster</a>. Si vous désirez participer, ou connaissez quelqu'un qui le
désirerait, à la création d'un thème graphique pour le bureau, n’oubliez pas
d’envoyer votre thème. La date limite de dépôt est le 10 octobre 2020.</p>

<p><b>Paquets qui ont besoin de travail</b></p>

## link= link to the mail report from wnpp@debian.org to debian-devel ML
## orphaned= number of packages orphaned according to $link
## rfa= number of packages up for adoption according to $link

<wnpp link="https://lists.debian.org/debian-devel/2020/09/msg00058.html"
        orphaned="1191"
        rfa="213" />

<p><b>Bogues pour débutants</b></p>

## check https://bugs.debian.org/cgi-bin/pkgreport.cgi?tag=newcomer and add outstanding + forwarded + pending upload
<p>
Debian utilise l'étiquette <q>newcomer</q> (débutant) pour signaler les
bogues qui sont adaptés aux nouveaux contributeurs comme point d'entrée pour
travailler sur des paquets particuliers.

Il y a actuellement <a href="https://bugs.debian.org/cgi-bin/pkgreport.cgi?tag=newcomer">195</a> bogues
marqués <q>newcomer</q> disponibles.
</p>

<toc-add-entry name="code">Code, développeurs et contributeurs</toc-add-entry>
<p><b>Nouveaux responsables de paquets depuis le 1 juillet 2019</b></p>
##Run the ./new-package-maintainers script locally
##./new-package-maintainers YYYMMDD
<p>
Bienvenue à Marie Alexandre, Saira Hussain, laokz, Arthur Diniz, Daniel
Pimentel, Ricardo Fantin da Costa, Ivan Noleto, Joao Seckler, Andre Marcelo
Alvarenga, Felipe Amorim, Pedro Vaz de Mello de Medeiros, Marco Villegas,
Thiago Gomes Verissimo, Teemu Toivola, Giuliano Augusto Faulin Belinassi,
Miguel Figueiredo,  Stephan Lachnit, Nicola Di Lieto, Ximin Luo, Paul Grosu,
Thomas Ward, Ganael Laplanche, Aaron M. Ucko, Rodrigo Carvalho, Lukas
Puehringer, Markus Teich, Alexander Ponyatykh, Rob Savoury, Joaquin de Andres,
Georges Basile Stavracas Neto, Jian-Ding Chen (timchen119), Juan Picca,
Katerina, Matthew Fernandez, Shane McDonald, Eric Desrochers, Remi Duraffort,
Sakirnth Nagarasa, Ambady Anand S., Abhijith Sheheer, Helen Koike, Sven Hartge,
Priyanka Saggu, Sebastian Holtermann, Jamie Bliss, David Hart, James Tocknell,
Julien Schueller, Matt Hsiao, Jafar Al-Gharaibeh, Matthias Blümel, Dustin
Kirkland, Gao Xiang , Alberto Leiva Popper, Benjamin Hof, Antonio Russo, Jérôme
Lebleu, Ramon Fried, Evangelos Rigas, Adam Cecile, Martin Habovstiak, Lucas
Kanashiro, Alessandro Grassi, Estebanb, James Price, Cyril Richard, John Scott,
David Bürgin, Beowulf, Igor Petruk, Thomas Dettbarn, Vifly, Lajos Veres,
Andrzej Urbaniak, Phil Wyett, Christian Barcenas, Johannes Schilling, Josh
Steadmon, Sven Hesse, Gert Wollny, suman rajan, kokoye2007, Kei Okada, Jonathan
Tan, David Rodriguez, David Krauser, Norbert Schlia, Pranav Ballaney, Steve
Meliza, Fabian Grünbichler, Sao I Kuan, Will Thompson, Abraham Raji, Andre
Moreira Magalhaes, Lorenzo Puliti, Dmitry Baryshkov, Leon Marz, Ryan Pavlik,
William Desportes, Michael Elterman, Simon, Schmeisser, Fabrice Bauzac, Pierre
Gruet, Mattia Biondi, Taowa Munene-Tardif, Sepi Gair, Piper McCorkle, Alois
Micard, xiao sheng wen, Roman Ondráček, Abhinav Krishna C K, Konstantin Demin,
Pablo Mestre Drake, Harley Swick, Robin Gustafsson, Hamid Nassiby, Étienne
Mollier, Karthik, Ben Fiedler, Jair Reis, Jordi Sayol, Emily Shaffer, Fabio dos
Santos Mendes, Bruno Naibert de Campos, Junior Figueredo, Marcelo Vinicius
Campos Amedi, Tiago Henrique Vercosa de Lima, Deivite Huender Ribeiro Cardoso,
Guilherme de Paula Xavier Segundo, Celio Roberto Pereira, Tiago Rocha, Leandro
Ramos, Filipi Souza, Leonardo Santiago Sidon da Rocha, Asael da Silva Vaz,
Regis Fernandes Gontijo, Carlos Henrique Lima Melara, Alex Rosch de Faria, Luis
Paulo Linares, Jose Nicodemos Vitoriano de Oliveira, Marcio Demetrio Bacci,
Fabio Augusto De Muzio Tobich, Natan Mourao, Wilkens Lenon, Paulo Farias,
Leonardo Rodrigues Pereira, Elimar Henrique da Silva, Delci Silva Junior, Paulo
Henrique Hebling Correa, Gleisson Jesuino Joaquim Cardoso, Joao Paulo Lima de
Oliveira, Jesus Ali Rios, Jaitony de Sousa, Leonardo Santos, Aristo Chen,
Olivier Humbert, Roberto De Oliveira, Martyn Welch, Arun Kumar Pariyar, Vasyl
Gello, Antoine Latter, Ken VanDine, Francisco M Neto, Dhavan Vaidya, Raphael
Medaer, Evangelos Ribeiro Tzaras, Richard Hansen, Joachim Langenbach, Jason
Hedden, Boian Bonev, Kay Thriemer, Sławomir Wójcik, Lukas Märdian, Kevin Duan,
zhao feng, Leandro Cunha, Cocoa, J0J0 T, Johannes Tiefenbacher, Iain Parris,
Guobang Bi, Lasse Flygenring-Harrsen, Emanuel Krivoy, Qianqian Fang, Rafael
Onetta Araujo, Shruti Sridhar, Alvin Chen, Brian Murray et Nick Gasson.
</p>

<p><b>Nouveaux responsables Debian</b></p>
##Run the ./dd-dm-from-keyring.pl script locally for new DM and DD
##./dd-dm-from-keyring.pl YYYMMDD
<p>
Bienvenue à Hans van Kranenburg, Scarlett Moore, Nikos Tsipinakis, Joan
Lledó, Baptiste Beauplat, Jianfeng Li, Denis Danilov, Joachim Falk, Thomas
Perret, William Grzybowski, Lars Tangvald, Alberto Molina Coballes, Emmanuel
Arias, Hsieh-Tseng Shen, Jamie Strandboge, Sven Geuer, Håvard Flaget Aasen,
Marco Trevisan, Dennis Braun, Stephane Neveu, Seunghun Han, Alexander Johan
Georg Kjäll, Friedrich Beckmann, Diego M. Rodriguez, Nilesh Patra, Hiroshi
Yokota, Shayan Doust, Chirayu Desai, Arnaud Ferraris, Francisco Vilmar Cardoso
Ruviaro, Patrick Franz, François Mazen, Kartik Kulkarni, Fritz Reichwald, Nick
Black et Octavio Alvarez.
</p>

<p><b>Nouveaux développeurs Debian</b></p>
<p>
Bienvenue à Keng-Yu Lin, Judit Foglszinger, Teus Benschop, Nick Morrott,
Ondřej Kobližek, Clément Hermann, Gordon Ball, Louis-Philippe Véronneau, Olek
Wojnar, Sven Eckelmann, Utkarsh Gupta, Robert Haist, Gard Spreemann, Jonathan
Bustillos, Scott Talbert, Paride Legovini, Ana Custura, Felix Lechner, Richard
Laager, Thiago Andrade Marques, Vincent Prat, Michael Robin Crusoe, Jordan
Justen, Anuradha Weeraman, Bernelle Verster, Gabriel F. T. Gomes, Kurt
Kremitzki, Nicolas Mora, Birger Schacht et Sudip Mukherjee.
</p>

<p><b>Contributeurs</b></p>

## Visit the link below and pull the information manually.

<p>
1375 personnes et 9 équipes sont actuellement recensées dans la page des
<a href="https://contributors.debian.org/">contributeurs</a> Debian pour 2020.
</p>


<p><b>Nouveaux paquets ou paquets dignes d'intérêt</b></p>

<p>
Voici quelques-uns des nombreux paquets <a href="https://packages.debian.org/unstable/main/newpkg">
introduits dans l'archive de Debian unstable</a> ces dernières semaines :</p>

<ul>
<li><a href="https://packages.debian.org/unstable/main/bpftool">bpftool – inspection et manipulation simple de programmes et cartes BPF ;</a></li>
<li><a href="https://packages.debian.org/unstable/main/elpa-magit-forge">elpa-magit-forge – interaction avec les forges de Git dans le confort de Magit ;</a></li>
<li><a href="https://packages.debian.org/unstable/main/libasmjit0">libasmjit0 – assembleur complet JIT et AOT x86/x64 pour C++ ;</a></li>
<li><a href="https://packages.debian.org/sid/libraritan-rpc-perl">libraritan-rpc-perl – module Perl pour l'interface avec Raritan JSON-RPC ;</a></li>
<li><a href="https://packages.debian.org/unstable/main/python3-rows">python3-rows – bibliothèque de données tabulaires, indépendante du format.</a></li>
</ul>

<p><b>Il était une fois dans Debian :</b></p>

## Items pulled from the Timeline https://timeline.debian.net
## Jump to any random year/ same month/ same week.
## Provide link and link description.
## This may work better with a script at some point, but for now let us see
## what the ease of work is.

## Format - YYYY-MM-DD text

<ul>

<li>le 2 septembre 2007, <a href="https://lists.debian.org/debian-devel-announce/2007/09/msg00001.html">mise à jour de packages.debian.org</a></li>

<li>le 4 septembre 2006, <a href="https://lists.debian.org/debian-devel-announce/2006/09/msg00002.html">cdrkit (dérivé de cdrtools) chargé dans l'archive Debian</a></li>

<li>le 4 septembre 2009, <a href="https://lists.debian.org/debian-devel-announce/2009/09/msg00002.html">le paquet grub est désormais basé sur GRUB2</a></li>

<li>le 6 septembre 1997, <a href="https://lists.debian.org/debian-devel-announce/1997/09/msg00000.html">implémentation du suivi de sévérité dans le BTS</a></li>

<li>le 9 septembre 2008, <a href="https://lists.debian.org/debian-devel-announce/2008/09/msg00001.html">l'architecture m68k introduite dans debian-ports.org</a></li>

</ul>

<toc-add-entry name="continuedpn">Continuer à lire les Nouvelles du projet Debian</toc-add-entry>
<continue-dpn />

<p><a href="https://lists.debian.org/debian-news/">S'inscrire ou se désinscrire </a> de la liste de diffusion Debian News</p>

#use wml::debian::projectnews::footer editor="l'équipe en charge de la publicité avec des contributions de Laura Arjona Reina, Jean-Pierre Giraud, Paulo Henrique de Lima Santana, Jean-Philippe Mengual, Donald Norwood" translator="Jean-Pierre Giraud, Jean-Paul Guillonneau, l\'équipe francophone de traduction"
