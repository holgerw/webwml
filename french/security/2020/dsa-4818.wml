#use wml::debian::translation-check translation="3f0f1a56a7218bc43e4769bf32b0352523c9a436" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité</define-tag>
<define-tag moreinfo>
<p>Plusieurs vulnérabilités ont été découvertes dans Sympa, un gestionnaire
de listes de diffusion, qui pourraient provoquer une élévation locale de
privilèges, un déni de service ou un accès non autorisé au moyen de
l'interface de programmation SOAP.</p>

<p>En complément, pour atténuer le 
<a href="https://security-tracker.debian.org/tracker/CVE-2020-26880">\
CVE-2020-26880</a>, l'exécutable sympa_newaliases-wrapper n'est plus
installé par défaut avec les privilèges du superutilisateur. Une nouvelle
question est introduite dans les écrans de debconf pour permettre les
installations avec setuid dans les configurations quand cela est
nécessaire.</p>

<p>Pour la distribution stable (Buster), ces problèmes ont été corrigés
dans la version 6.2.40~dfsg-1+deb10u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets sympa.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de sympa, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/sympa">\
https://security-tracker.debian.org/tracker/sympa</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2020/dsa-4818.data"
# $Id: $
