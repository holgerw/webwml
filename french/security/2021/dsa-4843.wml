#use wml::debian::translation-check translation="7d4e416de9b0d5870d3b56d250bdbed4f5cdde8b" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité</define-tag>
<define-tag moreinfo>
<p>Plusieurs vulnérabilités ont été découvertes dans le noyau Linux qui
pourraient conduire à une élévation de privilèges, un déni de service ou
des fuites d'informations.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-27815">CVE-2020-27815</a>

<p>Un défaut a été signalé dans le code du système de fichiers JFS
permettant à un attaquant local avec la capacité de configurer des
attributs étendus de provoquer un déni de service.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-27825">CVE-2020-27825</a>

<p>Adam <q>pi3</q> Zabrocki a signalé un défaut d'utilisation de mémoire
après libération dans la logique de redimensionnement du tampon circulaire
de ftrace à cause d'une situation de compétition, qui pourrait avoir pour
conséquence un déni de service ou une fuite d'informations.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-27830">CVE-2020-27830</a>

<p>Shisong Qin a signalé un défaut de déréférencement de pointeur NULL dans
le pilote principal du lecteur d'écran Speakup.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-28374">CVE-2020-28374</a>

<p>David Disseldorp a découvert que l'implémentation de la cible SCSI LIO
réalisait une vérification insuffisante dans certaines requêtes XCOPY. Un
attaquant avec accès à un Logical Unit Number et la connaissance des
affectations d'Unit Serial Number peut tirer avantage de ce défaut pour
lire et écrire sur n'importe quel « backstore » de LIO, indépendamment des
réglages de transport de SCSI.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-29568">CVE-2020-29568 (XSA-349)</a>

<p>Michael Kurth et Pawel Wieczorkiewicz ont signalé que les interfaces
peuvent déclencher un « Out of Memory » dans les dorsaux en mettant à jour
un chemin surveillé.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-29569">CVE-2020-29569 (XSA-350)</a>

<p>Olivier Benjamin et Pawel Wieczorkiewicz ont signalé un défaut
d'utilisation de mémoire après libération qui peut être déclenché par une
interface bloc dans blkback de Linux. Un client qui se comporte mal peut
déclencher un plantage de dom0 en se connectant et se déconnectant
continuellement à une interface bloc.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-29660">CVE-2020-29660</a>

<p>Jann Horn a signalé un problème d'incompatibilité de verrouillage dans le
sous-système tty qui peut permettre à un attaquant local de monter une
attaque de lecture après libération à l'encontre de TIOCGSID.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-29661">CVE-2020-29661</a>

<p>Jann Horn a signalé un problème de verrouillage dans le sous-système tty
qui peut provoquer une utilisation de mémoire après libération. Un
attaquant local peut tirer avantage de ce défaut pour une corruption de
mémoire ou une élévation de privilèges.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-36158">CVE-2020-36158</a>

<p>Un défaut de dépassement de tampon a été découvert dans le pilote WiFi
mwifiex qui pourrait provoquer un déni de service ou l'exécution de code
arbitraire à l'aide d'une longue valeur de SSID.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-3347">CVE-2021-3347</a>

<p>Les futex de PI sont sujets à une utilisation de mémoire après
libération de la pile du noyau durant le traitement de défaut. Un
utilisateur non privilégié pourrait utiliser ce défaut pour le plantage du
noyau (provoquant un déni de service) ou pour une élévation de privilèges.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-20177">CVE-2021-20177</a>

<p>Un défaut a été découvert dans l'implémentation de Linux de la
correspondance de chaîne dans un paquet. Un utilisateur privilégié (avec la
capacité de superutilisateur ou CAP_NET_ADMIN) peut tirer avantage de ce
défaut pour provoquer un « kernel panic » lors de l'insertion de règles
iptables.</p></li>

</ul>

<p>Pour la distribution stable (Buster), ces problèmes ont été corrigés
dans la version 4.19.171-2.</p>

<p>Nous vous recommandons de mettre à jour vos paquets linux.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de linux, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/linux">\
https://security-tracker.debian.org/tracker/linux</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2021/dsa-4843.data"
# $Id: $
