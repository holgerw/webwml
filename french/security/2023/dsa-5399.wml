#use wml::debian::translation-check translation="436ea085e4ef9d07e4bcbb712e28c2d5b2d750d6" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité</define-tag>
<define-tag moreinfo>
<p>Plusieurs vulnérabilités ont été découvertes dans odoo, une suite
d'applications commerciales basée sur le web au code source ouvert.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-44775">CVE-2021-44775</a>,
<a href="https://security-tracker.debian.org/tracker/CVE-2021-26947">CVE-2021-26947</a>,
<a href="https://security-tracker.debian.org/tracker/CVE-2021-45071">CVE-2021-45071</a>,
<a href="https://security-tracker.debian.org/tracker/CVE-2021-26263">CVE-2021-26263</a>

<p>Script intersite (XSS) permettant à un attaquant distant d'injecter des
commandes arbitraires.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-45111">CVE-2021-45111</a>

<p>Contrôle d'accès incorrect permettant à un utilisateur distant
authentifié de créer des comptes utilisateurs et d'accéder à des données à
accès restreint.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-44476">CVE-2021-44476</a>,
<a href="https://security-tracker.debian.org/tracker/CVE-2021-23166">CVE-2021-23166</a>

<p>Contrôle d'accès incorrect permettant à un administrateur distant
authentifié d'accéder à des fichiers locaux sur le serveur.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-23186">CVE-2021-23186</a>

<p>Contrôle d'accès incorrect permettant à un administrateur distant
authentifié de modifier le contenu de bases de données d'autres entités.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-23178">CVE-2021-23178</a>

<p>Contrôle d'accès incorrect permettant à un utilisateur distant
authentifié d'utiliser la méthode de paiement d'un autre utilisateur.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-23176">CVE-2021-23176</a>

<p>Contrôle d'accès incorrect permettant à un utilisateur distant
authentifié d'accéder à des informations comptables.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-23203">CVE-2021-23203</a>

<p>Contrôle d'accès incorrect permettant à un utilisateur distant
authentifié d'accéder à des documents arbitraires au moyen d'exportations
de PDF.</p></li>

</ul>

<p>Pour la distribution stable (Bullseye), ces problèmes ont été corrigés
dans la version 14.0.0+dfsg.2-7+deb11u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets odoo.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de odoo, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/odoo">\
https://security-tracker.debian.org/tracker/odoo</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2023/dsa-5399.data"
# $Id: $
