#use wml::debian::translation-check translation="0fe7c659f55b531c52b6010f55174a75701e38d1" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité</define-tag>
<define-tag moreinfo>
<p>Plusieurs vulnérabilités ont été découvertes dans Samba, qui pouvaient
avoir pour conséquences la divulgation d'informations, un déni de service
ou une  application insuffisante de directives de configuration concernant
la sécurité.</p>

<p>La version de Samba dans la distribution oldstable (Bullseye) ne peut
plus être complètement prise en charge : si vous utilisez Samba comme
contrôleur de domaine, vous devez soit mettre à niveau vers la distribution
stable ou, si ce n'est pas une option immédiate, envisager de migrer vers
la version de Samba fournie par bullseye-backports (qui continuera à être
mise à jour par rapport à la version de stable). L'utilisation de Samba
comme serveur de fichiers ou d'impression continue à être prise en charge,
une DSA particulière fournira une mise à jour accompagnée d'une
documentation sur la portée de la prise en charge qui se poursuit.</p>

<p>Pour la distribution stable (Bookworm), ces problèmes ont été corrigés
dans la version 2:4.17.10+dfsg-0+deb12u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets samba.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de samba, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/samba">\
https://security-tracker.debian.org/tracker/samba</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2023/dsa-5477.data"
# $Id: $
