#use wml::debian::translation-check translation="e7b1217a8fd0eb5cc3b72c8f2ebb1e62a4b0475f" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité</define-tag>
<define-tag moreinfo>
<p>Plusieurs vulnérabilités ont été découvertes dans BIND, une
implémentation de serveur DNS.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-2828">CVE-2023-2828</a>

<p>Shoham Danino, Anat Bremler-Barr, Yehuda Afek et Yuval Shavitt
ont découvert qu'un défaut dans l'algorithme de nettoyage de cache utilisé
dans named peut faire que la limite de la taille du cache configurée dans
named puisse être dépassée de façon significative, provoquant
éventuellement un déni de service.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-2911">CVE-2023-2911</a>

<p>Un défaut dans le traitement des quotas de clients récursifs peut avoir
pour conséquence un déni de service (plantage du démon named).</p></li>

</ul>

<p>Pour la distribution oldstable (Bullseye), ces problèmes ont été
corrigés dans la version 1:9.16.42-1~deb11u1.</p>

<p>Pour la distribution stable (Bookworm), ces problèmes ont été corrigés
dans la version 1:9.18.16-1~deb12u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets bind9.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de bind9, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/bind9">\
https://security-tracker.debian.org/tracker/bind9</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2023/dsa-5439.data"
# $Id: $
