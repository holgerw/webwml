#use wml::debian::translation-check translation="7000f208a4e5ab2d7a872d4439db6ec5dcba9270" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité</define-tag>
<define-tag moreinfo>
<p>Plusieurs vulnérabilités ont été découvertes dans Samba, un serveur de
fichier SMB/CIFS, d'impression et de connexion pour UNIX.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-44142">CVE-2021-44142</a>

<p>Orange Tsai a signalé une vulnérabilité d'écriture de tas hors limites
dans le module VFS vfs_fruit, qui pouvait avoir pour conséquence
l'exécution à distance de code arbitraire en tant que superutilisateur.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-0336">CVE-2022-0336</a>

<p>Kees van Vloten a signalé que les utilisateurs de Samba AD dotés de la
permission d'écriture dans un compte pouvaient se faire passer pour des
services arbitraires.</p></li>

</ul>

<p>Pour la distribution oldstable (Buster), ces problèmes ont été corrigés
dans la version 2:4.9.5+dfsg-5+deb10u3. Conformément à la DSA 5015-1,
<a href="https://security-tracker.debian.org/tracker/CVE-2022-0336">\
CVE-2022-0336</a> ils n'ont pas été traités dans la distribution oldstable
(Buster).</p>

<p>Pour la distribution stable (Bullseye), ces problèmes ont été corrigés
dans la version 2:4.13.13+dfsg-1~deb11u3. Par ailleurs, quelques correctifs
complémentaires pour le 
<a href="https://security-tracker.debian.org/tracker/CVE-2020-25717">CVE-2020-25717</a>
sont inclus dans cette mise à jour (Cf. nº1001068).</p>

<p>Nous vous recommandons de mettre à jour vos paquets samba.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de samba, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/samba">\
https://security-tracker.debian.org/tracker/samba</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2022/dsa-5071.data"
# $Id: $
