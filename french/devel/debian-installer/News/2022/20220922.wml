#use wml::debian::translation-check translation="4f8ee0d8b39126e64615a5dccbc22eb01dc4ce32" maintainer="Jean-Pierre Giraud"
<define-tag pagetitle>Publication de l'installateur Debian Bookworm Alpha 1</define-tag>
<define-tag release_date>2022-09-22</define-tag>
#use wml::debian::news

<p>
L'<a href="https://wiki.debian.org/DebianInstaller/Team">équipe</a> du
programme d'installation de Debian a le plaisir d'annoncer la parution
de la première version alpha de l’installateur pour Debian 12 <q>Bookworm</q>.
</p>

<p>
Un certain nombre de modifications ont été soumises par Debian Janitor et ont
été intégrées aux nombreux composants à partir desquels l'installateur est
assemblé, et elles ne sont pas documentées dans la mesure où elles concernent
habituellement des rattrapages de debhelper et d'autres pratiques exemplaires
au moment de la construction.
</p>

<p>
Nous souhaitons remercier pour leurs premières contributions au paquet source
principal de debian-installer :
</p>

<ul>
  <li>Youngtaek Yoon</li>
  <li>Sophie Brun</li>
  <li>Roland Clobus</li>
</ul>


<h2>Améliorations dans cette version de l'installateur</h2>

<ul>
  <li>alsa-lib :
  <ul>
    <li>installation du répertoire <code>/usr/share/alsa/ctl</code> manquant
    dans libasound2-udeb
    (<a href="https://bugs.debian.org/992536">nº 992536</a>).</li>
  </ul>
  </li>
  <li>anna :
  <ul>
    <li>installation possible avec un noyau qui ne correspond pas (<a href="https://bugs.debian.org/998668">nº 998668</a>).</li>
  </ul>
  </li>
  <li>apt-setup :
  <ul>
    <li>installation de ca-certificates quand le protocole détecté est https,
    ainsi la cible peut valider les certificats (<a href="https://bugs.debian.org/1015887">nº 1015887</a>).</li>
  </ul>
  </li>
  <li>brltty :
  <ul>
    <li>menu principal et debconf interrompus, sinon les versions graphiques
    demeurent en arrière plan et remplissent les journaux </li>
    <li>ajout d'un titre approprié au menu ;</li>
    <li>activation du lecteur d'écran dans Cinnamon (<a href="https://bugs.debian.org/992169">nº 992169</a>) ;</li>
    <li>désactivation de la prise en charge de liblouis et de hid dans udeb ;</li>
    <li>mise à jour des règles d'udev ;</li>
    <li>coupure automatique des lignes à 80 colonnes parce que cela est mieux
    adapté aux périphériques Braille.</li>
  </ul>
  </li>
  <li>busybox :
  <ul>
    <li>activation des applets pour l'installateur : awk, base64, less (<a href="https://bugs.debian.org/949626">nº 949626</a>),
    stty (<a href="https://bugs.debian.org/891806">nº 891806</a>).</li>
  </ul>
  </li>
  <li>cdebconf :
  <ul>
    <li>text : étapes interruptibles (<a href="https://bugs.debian.org/998424">nº 998424</a>) ;</li>
    <li>text : utilisation de libreadline et d'history pour permettre les choix
    avec les flèches.</li>
  </ul>
  </li>
  <li>cdrom-detect :
  <ul>
    <li>prise en charge des images de l'installateur sur les disques ordinaires
    (<a href="https://bugs.debian.org/851429">nº 851429</a>).</li>
  </ul>
  </li>
  <li>choose-mirror :
  <ul>
    <li>récupération de la liste des miroirs à partir de mirror-master.debian.org ;</li>
    <li>tri de deb.debian.org en premier, suivi par ftp*.*.debian.org, puis les
    autres.</li>
  </ul>
  </li>
  <li>console-setup :
  <ul>
    <li>correction de la traduction des symboles X en symboles du noyau pour
    les points de code élevés Unicode (<a href="https://bugs.debian.org/968195">nº 968195</a>).</li>
  </ul>
  </li>
  <li>debian-installer :
  <ul>
    <li>démarrage automatique de la synthèse vocale après un délai de
    30 secondes ;</li>
    <li>ajout de la prise en charge de composants multiples dans UDEB_COMPONENTS ;</li>
    <li>passage de l'ABI du noyau Linux à la version 5.19.0-1 ;</li>
    <li>installation de Bookworm, avec les paquets udeb de Bookworm ;</li>
    <li>contournement de FTBFS sur armel et mipsel, quand libcrypto3-udeb
    dépend de libatomic1, en copiant les fichiers à partir de l'hôte ;</li>
    <li>harmonisation des menus d'amorçage de UEFI (grub) et du BIOS (syslinux) :
    certaines étiquettes et règles d'inclusion pour la synthèse vocale ;</li>
    <li>plus de codage en dur du nom de la distribution dans le fichier
    <code>menu.cfg</code> de syslinux ;</li>
    <li>correction d'erreur de construction reproductive.</li>
  </ul>
  </li>
  <li>debootstrap :
  <ul>
    <li>ajout de (Debian) Trixie comme lien symbolique vers sid ;</li>
    <li>ajout de usr-is-merged à l'ensemble requis sur testing/unstable (voir
    <a href="https://lists.debian.org/debian-devel-announce/2022/09/msg00001.html">la transition vers usrmerge a démarré</a>).
    </li>
  </ul>
  </li>
  <li>espeakup :
  <ul>
    <li>affichage du numéro de la carte ALSA lors du choix des cartes ;</li>
    <li>rectification d'approximations de langages ;</li>
    <li>ajout de la prise en charge des voix de mbrola, évitant en1mrpa et us1mrpa ;</li>
    <li>installation de la voix mbrola utilisée pendant le processus
    d'installation en même temps qu'espeak-ng.</li>
  </ul>
  </li>
  <li>finish-install :
  <ul>
    <li>amélioration de l'intelligibilité de l'écran de redémarrage (<a href="https://bugs.debian.org/982640">nº 982640</a>) ;</li>
    <li>activation du lecteur d'écran dans Cinnamon (<a href="https://bugs.debian.org/992169">nº 992169</a>) ;</li>
    <li>création des liens symboliques anciens de <code>/etc/mtab</code> avec
    la même destination que celle utilisée par systemd.</li>
  </ul>
  </li>
  <li>freetype :
  <ul>
    <li>construction du paquet udeb sans librsvg.</li>
  </ul>
  </li>
  <li>gdk-pixbuf :
  <ul>
    <li>construction du chargeur de PNG directement dans la bibliothèque.</li>
  </ul>
  </li>
  <li>glibc :
  <ul>
    <li>ajustement du paquet udeb pour la nouvelle disposition (presque tous
    les liens symboliques ont disparu).</li>
  </ul>
  </li>
  <li>hw-detect :
  <ul>
    <li>remplacement de <code>/etc/pcmcia/</code> par <code>/etc/pcmciautils/</code> (<a href="https://bugs.debian.org/980271">nº 980271</a>) ;</li>
    <li>retrait de la prise en charge expérimentale de dmraid ;</li>
    <li>installation du paquet opal-prd sur les machines OpenPOWER.</li>
  </ul>
  </li>
  <li>installation-report :
  <ul>
    <li>inclusion des cartes ALSA détectées dans le rapport sur le matériel ;</li>
    <li>reformulation du dialogue pour la sauvegarde des journaux (<a href="https://bugs.debian.org/683203">nº 683203</a>).</li>
  </ul>
  </li>
  <li>kmod :
  <ul>
    <li>implémentation de la génération d'un fichier shlibs moins strict.</li>
  </ul>
  </li>
  <li>linux :
  <ul>
    <li>davantage de modules de compression dans le paquet principal de
    l'installateur (<a href="https://bugs.debian.org/992221">nº 992221</a>) ;</li>
    <li>udeb : ajout de essiv à crypto-modules (<a href="https://bugs.debian.org/973378">nº 973378</a>) ;</li>
    <li>udeb : ajout de gestionnaires de périphérique SCSI à multipath-modules (<a href="https://bugs.debian.org/989079">nº 989079</a>) ;</li>
    <li>udeb : passage de crc64 à crc-modules, et scsi-core-modules en devient une
    dépendance.</li>
  </ul>
  </li>
  <li>localechooser :
  <ul>
    <li>correction de la détection de la variable <q>LEVEL</q> (<a href="https://bugs.debian.org/1011254">nº 1011254</a>) ;</li>
    <li>correction de la détection de la langue quand un code de nom de langue
    à deux lettres est un préfixe d'un code de langue à trois lettres.</li>
  </ul>
  </li>
  <li>lvm2 :
  <ul>
    <li>désactivation de l'utilisation de systemd dans le paquet udeb (<a href="https://bugs.debian.org/1015174">nº 1015174</a>).</li>
  </ul>
  </li>
  <li>multipath-tools :
  <ul>
    <li>amélioration de la prise en charge dans l'installateur : fourniture d'un
    fichier de configuration et de règles udev par défaut pour faciliter la
    détection des périphériques multipath.</li>
  </ul>
  </li>
  <li>nano :
  <ul>
    <li>construction du paquet udeb avec libncursesw6-udeb, dans la mesure où
    la prise en charge de S-Lang a été supprimée (<a href="https://bugs.debian.org/976275">nº 976275</a>).</li>
  </ul>
  </li>
  <li>net-retriever :
  <ul>
    <li>correction de la prise en charge du boutisme dans netcfg_gateway_reachable (<a href="https://bugs.debian.org/1007929">nº 1007929</a>) ;</li>
    <li>ajout de la prise en charge de pointopoint préconfiguré ;</li>
    <li>ajout de la prise en charge des adresses fe80 comme passerelle.</li>
  </ul>
  </li>
  <li>nvme :
  <ul>
    <li>construction de nvme-cli-udeb pour l'utiliser dans l'installateur.</li>
  </ul>
  </li>
  <li>openssl :
  <ul>
    <li>ajout de ossl-modules au paquet udeb de libcrypto.</li>
  </ul>
  </li>
  <li>os-prober :
  <ul>
    <li>ajout de la détection de Windows 11 ;</li>
    <li>ajout de la prise en charge de plusieurs chemins d'initrd ;</li>
    <li>ajout de la détection de Linux Exherbo (<a href="https://bugs.debian.org/755804">nº 755804</a>) ;</li>
    <li>tri des noyaux Linux par ordre inverse de versions si aucun fichier de
    configuration de chargeur de démarrage n'est trouvé (<a href="https://bugs.debian.org/741889">nº 741889</a>) ;</li>
    <li>détection de ntfs3 (noyaux 5.15+) en plus de ntfs et ntfs-3g ;</li>
    <li>correction d'une régression introduite en appelant <q>dmraid -r</q> une
    fois ;</li>
    <li>ajout de la détection des fichiers initramfs d'Alpine ;</li>
    <li>ajout de la lecture de <code>/usr/lib/os-release</code> comme solution
    de repli.</li>
  </ul>
  </li>
  <li>partman-auto :
  <ul>
    <li>retrait de la prise en charge expérimentale de dmraid.</li>
  </ul>
  </li>
  <li>partman-base :
  <ul>
    <li>retrait de la prise en charge expérimentale de dmraid.</li>
  </ul>
  </li>
  <li>partman-jfs :
  <ul>
    <li>retrait du test de validité de JFS comme système de fichiers de
    démarrage ou racine.</li>
  </ul>
  </li>
  <li>readline :
  <ul>
    <li>ajout de libreadline8-udeb et readline-common-udeb, nécessaires au
    frontal texte de cdebconf (utilisé pour l'accessibilité avec Speakup).</li>
  </ul>
  </li>
  <li>rescue :
  <ul>
    <li>détection des situations où le montage de <code>/usr</code> pourrait
    être nécessaire et interrogation à son propos
    (<a href="https://bugs.debian.org/1000239">nº 1000239</a>) ;</li>
    <li>montage de systèmes de fichiers distincts avec des options de montage
    issues de fstab (nécessaire par exemple avec les sous-volumes btrfs) ;</li>
    <li>corrections de divers problèmes lors du montage de plusieurs systèmes
    de fichiers distincts ;</li>
    <li>réusinage de diverses opérations de montage et de démontage pour
    <code>/target</code>.</li>
  </ul>
  </li>
  <li>rootskel :
  <ul>
    <li>lors de la réouverture de la console Linux, utilisation de tty1 à la
    place de tty0, corrigeant Ctrl-c.</li>
  </ul>
  </li>
  <li>s390-dasd :
  <ul>
    <li>plus d'option -f obsolète passée à dasdfmt (<a href="https://bugs.debian.org/1004292">nº 1004292</a>).</li>
  </ul>
  </li>
  <li>s390-tools :
  <ul>
    <li>installation de hsci, utilisé pour montrer et contrôler les HiperSockets
    Converged Interfaces.</li>
  </ul>
  </li>
  <li>systemd :
  <ul>
    <li>abandon de la construction distincte du paquet udeb ;</li>
    <li>udev-udeb : fragment de code de modprobe.d fourni pour imposer
    scsi_mod.scan=sync dans l'installateur ;</li>
    <li>passage de la priorité de systemd-timesyncd à standard, pour assurer
    qu'il est installé par défaut (<a href="https://bugs.debian.org/986651">nº 986651</a>, <a href="https://bugs.debian.org/993947">nº 993947</a>).</li>
  </ul>
  </li>
  <li>wireless-regdb :
  <ul>
    <li>retrait des fichiers standards déployés par l'installateur (<a href="https://bugs.debian.org/1012601">nº 1012601</a>).</li>
  </ul>
  </li>
  <li>x11-xkb-utils :
  <ul>
    <li>correction du plantage de setxkbmap dans l'installateur (<a href="https://bugs.debian.org/1010161">nº 1010161</a>).</li>
  </ul>
  </li>
</ul>


<h2>Modifications de la prise en charge matérielle</h2>

<ul>
  <li>debian-installer :
  <ul>
    <li>armhf : ajout de la prise en charge de Bananapi_M2_Ultra (<a href="https://bugs.debian.org/982913">nº 982913</a>) ;</li>
    <li>armhf : mise à jour du nom de fichier MX53LOCO avec les images U-Boot
    récentes.</li>
  </ul>
  </li>
  <li>flash-kernel :
  <ul>
    <li>flash-kernel évité dans tous les systèmes EFI ;</li>
    <li>ajout de la prise en charge d'ODROID-C4, -HC4, -N2, -N2Plus (<a href="https://bugs.debian.org/982369">nº 982369</a>) ;</li>
    <li>ajout de Librem5r4 (Evergreen) ;</li>
    <li>ajout de SiFive HiFive Unmatched A00 (<a href="https://bugs.debian.org/1006926">nº 1006926</a>) ;</li>
    <li>ajout de la carte BeagleV Starlight Beta ;</li>
    <li>ajout du kit PolarFire-SoC Icicle de Microchip ;</li>
    <li>ajout de MNT Reform 2.</li>
  </ul>
  </li>
  <li>linux :
  <ul>
    <li>arm64 : inclusion de panel-edp au paquet udeb fb-modules ;</li>
    <li>arm64 : ajout de nvmem-rockchip-efuse et phy-rockchip-inno-hdmi au
    paquet udeb fb-modules ;</li>
    <li>arm64 : ajout de pwm-imx27, nwl-dsi, ti-sn65dsi86, imx-dcss, mxsfb,
    mux-mmio et imx8mq-interconnect au paquet udeb fb-modules pour le MNT
    Reform 2 ;</li>
    <li>mips* : unification des variantes de l'installateur ;</li>
    <li>mips* : ajout d'une plateforme générique et retrait de 5kc-malta
    des portages 32 bits.</li>
  </ul>
  </li>
  <li>oldsys-preseed :
  <ul>
    <li>suppression de la prise en charge d'arm*/ixp4xx et d'arm*/iop32x
    (plus pris en charge par le noyau Linux).</li>
  </ul>
</li>
</ul>


<h2>État de la localisation</h2>

<ul>
  <li>78 langues sont prises en charge dans cette version.</li>
  <li>La traduction est complète pour 30 de ces langues.</li>
</ul>


<h2>Problèmes connus dans cette version</h2>

<p>
Veuillez consulter les <a href="$(DEVEL)/debian-installer/errata">errata</a>
pour plus de détails et une liste complète des problèmes connus.
</p>


<h2>Retours d'expérience pour cette version</h2>

<p>
Nous avons besoin de votre aide pour trouver des bogues et améliorer encore
l'installateur, merci de l'essayer. Les CD, les autres supports d'installation,
et tout ce dont vous pouvez avoir besoin sont disponibles sur notre
<a href="$(DEVEL)/debian-installer">site web</a>.
</p>


<h2>Remerciements</h2>

<p>
L'équipe du programme d'installation Debian remercie toutes les personnes ayant
pris part à cette publication.
</p>
