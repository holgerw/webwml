#use wml::debian::template title="Informations sur la version « Squeeze » de Debian"
#include "$(ENGLISHDIR)/releases/info"
#use wml::debian::translation-check translation="476ef1924df8fdb75ad78952645435d512788254" maintainer="Jean-Paul Guillonneau"

<p>La version de Debian <current_release_squeeze> a été publiée le
<a href="$(HOME)/News/<current_release_newsurl_squeeze/>"><current_release_date_squeeze></a>.
<ifneq "6.0.0" "<current_release>"
  "La version 6.0.0 a été initialement publiée le <:=spokendate('2011-02-06'):>."
/>
Cette version comprend de nombreuses modifications décrites dans notre
<a href="$(HOME)/News/2011/20110205a">communiqué de presse</a> et nos
<a href="releasenotes">notes de publication</a>.</p>

<p><strong>Debian 6 été remplacée par
<a href="../wheezy/">Debian 7 (<q>Wheezy</q>)</a>.
La prise en charge des mises à jour de sécurité a cessé depuis le <:=spokendate('2014-05-31'):>.
</strong></p>

<p><strong>Squeeze a bénéficié de la prise en charge à long terme (LTS) jusqu’à
la fin février 2016. Cette prise en charge était limitée aux architectures
i386 et amd64.
Pour plus d’informations, veuillez consulter la <a
href="https://wiki.debian.org/LTS">section LTS du wiki de Debian</a>.
</strong></p>

<p>Pour obtenir et installer Debian, veuillez vous reporter à la page des
<a href="debian-installer/">informations d'installation</a> et au <a
href="installmanual">guide d'installation</a>. Pour mettre à niveau à
partir d'une ancienne version de Debian, veuillez vous reporter aux
instructions des <a href="releasenotes">notes de publication</a>.</p>

<p>Les architectures suivantes étaient prises en charge :</p>

<ul>
<li><a href="../../ports/amd64/">PC 64 bits (amd64)</a>
<li><a href="../../ports/i386/">PC 32 bits (i386)</a>
<li><a href="../../ports/armel/">EABI ARM (armel)</a>
<li><a href="../../ports/powerpc/">PowerPC</a>
<li><a href="../../ports/sparc/">SPARC</a>
<li><a href="../../ports/ia64/">Intel Itanium IA-64</a>
<li><a href="../../ports/mipsel/">MIPS (petit-boutiste)</a>
<li><a href="../../ports/kfreebsd-amd64/">kFreeBSD PC 64 bits (amd64)</a>
<li><a href="../../ports/kfreebsd-i386/">kFreeBSD PC 32 bits (i386)</a>
<li><a href="../../ports/s390/">IBM S/390</a>
<li><a href="../../ports/mips/">MIPS (gros-boutiste)</a>
</ul>

<p>
Contrairement à nos souhaits, certains problèmes pourraient toujours exister
dans cette version, même si elle est déclarée <em>stable</em>. Nous avons
réalisé <a href="errata">une liste des principaux problèmes connus</a>, et vous
pouvez toujours nous <a href="../reportingbugs">signaler d'autres problèmes</a>.
</p>
