#use wml::debian::translation-check translation="ab3c0fa63d12dbcc8e7c3eaf4a72beb7b56d9741" maintainer="Baptiste Jammet"
#use wml::debian::template title="Debian&nbsp;Trixie &mdash; Manuel d'installation" BARETITLE=true
#use wml::debian::release
#include "$(ENGLISHDIR)/releases/info"
#include "$(ENGLISHDIR)/releases/arches.data"
#include "$(ENGLISHDIR)/releases/trixie/release.data"

<if-stable-release release="bookworm">
<p>Ceci est une <strong>version bêta</strong> du manuel d'installation de
Debian 13, surnommée Trixie, qui n'a pas encore été publiée. Les
informations présentées ici peuvent être dépassées et/ou incorrectes en
raison des changements effectués sur l'installateur. Vous pouvez être
intéressé par le
<a href="../Bookworm/installmanual">manuel d'installation de Debian 12,
surnommée Bookworm</a>, qui est la dernière version publiée de Debian, ou
par la <a href="https://d-i.debian.org/manual/">\
version de développement du manuel d'installation</a>,
qui est la version la plus à jour de ce document.</p>
</if-stable-release>

<p>Les instructions d'installation, ainsi que les fichiers
téléchargeables, sont disponibles pour chacune des architectures :</p>

<ul>
<:= &permute_as_list('', 'Manuel d\'installation'); :>
</ul>

<p>Si vous avez configuré correctement les options de langue de votre
navigateur, vous pouvez utiliser le lien ci-dessus pour avoir
automatiquement la bonne page HTML – voir les explications concernant la
<a href="$(HOME)/intro/cn">négociation de contenu</a>.
Sinon, choisissez l'architecture adéquate, la langue et le format que
vous souhaitez dans le tableau ci-dessous.</p>

<div class="centerdiv">
<table class="reltable">
<tr>
  <th align="left"><strong>Architecture</strong></th>
  <th align="left"><strong>Format</strong></th>
  <th align="left"><strong>Langues</strong></th>
</tr>
<: &permute_as_matrix_new( file => 'install', langs => \%langsinstall,
			   formats => \%formats, arches => \@arches,
			   html_file => 'index', namingscheme => sub {
			   "$_[0].$_[1].$_[2]" } ); :>
</table>
</div>
