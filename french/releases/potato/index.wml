#use wml::debian::template title="Informations sur la version Debian GNU/Linux 2.2 « Potato »" BARETITLE=yes
#use wml::debian::release
#include "$(ENGLISHDIR)/releases/info"
#use wml::debian::translation-check translation="dc28c67a4a9013b56f1b40512f11f8af875d6a6c" maintainer="Jean-Paul Guillonneau"

<p>La version de Debian GNU/Linux 2.2 (Potato) a été publiée le
<:=spokendate ("2000-08-14"):>. La dernière publication intermédiaire de
Debian 2.2 est la version <current_release_potato>, publiée le
<a href="$(HOME)/News/<current_release_newsurl_potato/>"><current_release_date_potato></a>.</p>

<p><strong>Debian GNU/Linux 2.2 a été remplacée par
<a href="../woody/">Debian GNU/Linux 3.0 (« Woody »)</a>.
La prise en charge des mises à jour de sécurité a cessé depuis le 30 juin 2003.</strong>
Veuillez consulter les
<a href="https://lists.debian.org/debian-devel-announce/2003/debian-devel-announce-200302/msg00010.html">\
résultats de l’enquête de l’équipe de sécurité</a> pour plus d’informations.</p>

<p>Pour des informations sur les principales modifications apportées dans cette
publication, veuillez lire les <a href="releasenotes">notes de publication</a>
et le <a href="$(HOME)/News/2000/20000815">communiqué de presse</a>
officiel.</p>

<p>Debian GNU/Linux 2.2 est dédié à la mémoire de Joel « Espy » Klecker, un
développeur de Debian qui, sans que la majorité du Projet Debian le sache, a été
cloué au lit et a combattu une maladie appelée dystrophie musculaire de Duchenne
pendant la plus grande durée de son engagement dans Debian. Le Projet Debian ne
réalise que maintenant l’ampleur de son engagement et de sa bienveillance
envers nous. Aussi, comme marque de reconnaissance et en mémoire de sa vie
exemplaire, cette publication de Debian GNU/Linux lui est dédiée.</p>

<p>Debian GNU/Linux 2.2 est disponible depuis Internet ou auprès de fournisseurs
de CD, veuillez consulter notre page <a href="$(HOME)/distrib/">Obtenir
Debian</a> pour plus d’informations.</p>

<p>Les architectures suivantes étaient prises en charge dans cette publication :</p>

<ul>
<li><a href="/ports/alpha/">alpha</a>
<li><a href="/ports/arm/">arm</a>
<li><a href="/ports/i386/">i386</a>
<li><a href="/ports/m68k/">m68k</a>
<li><a href="/ports/powerpc/">powerpc</a>
<li><a href="/ports/sparc/">sparc</a>
</ul>

<p>Avant d’installer Debian, veuillez lire le guide d’installation. Celui-ci
contient pour votre architecture des instructions et des liens pour tous les
fichiers nécessaires pour cette installation. Vous pouvez aussi être intéressé
par le guide d’installation pour Debian 2.2, qui est un tutoriel en ligne.</p>

<p>Si vous utilisez APT, vous pouvez ajouter les lignes suivantes dans votre
fichier <code>/etc/apt/sources.list</code> pour accéder aux paquets de Potato :
</p>

<pre>
  deb http://archive.debian.org potato main contrib non-free
  deb http://non-us.debian.org/debian-non-US potato/non-US main non-free
</pre>

<p>Veuillez lire les pages de manuel de <code>apt-get</code>(8) et de
<code>sources.list</code>(5) pour plus d’informations.</p>

<p>
Contrairement à nos souhaits, certains problèmes pourraient toujours exister
dans cette version, même si elle est déclarée <em>stable</em>. Nous avons
réalisé <a href="errata">une liste des principaux problèmes connus</a>, et vous
pouvez toujours nous signaler d'autres problèmes.
</p>
