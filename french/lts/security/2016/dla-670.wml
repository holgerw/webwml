#use wml::debian::translation-check translation="55d1ac616e9ec6fe42ad1680e45c2ce133b85547" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Plusieurs vulnérabilités ont été découvertes dans le noyau Linux qui
pourraient conduire à une augmentation de droits, un déni de service ou
à des fuites d'informations.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2015-8956">CVE-2015-8956</a>

<p>L'absence de vérification des entrées dans le traitement de sockets
RFCOMM Bluetooth peut avoir pour conséquence un déni de service ou une
fuite d'informations.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-5195">CVE-2016-5195</a>

<p>Une situation de compétition dans le code de gestion de mémoire peut
être utilisée pour une augmentation de droits locale. Cela n'affecte pas
les noyaux construits avec PREEMPT_RT activé.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-7042">CVE-2016-7042</a>

<p>Ondrej Kozina a découvert qu'une allocation de tampon incorrecte dans la
fonction proc_keys_show() peut avoir pour conséquence un déni de service
local.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-7425">CVE-2016-7425</a>

<p>Marco Grassi a découvert un dépassement de tampon dans le pilote SCSI
arcmsr qui peut avoir pour conséquence un déni de service local, ou
éventuellement, l'exécution de code arbitraire.</p></li>

</ul>

<p>Pour Debian 7 <q>Wheezy</q>, ces problèmes ont été corrigés dans la
version 3.2.82-1. Cette version inclut aussi des correctifs de bogue issus
de la version amont 3.2.82 et des mises à jour de l'ensemble de fonctions
PREEMPT_RT vers la version 3.2.82-rt119.</p>

<p>Pour Debian 8 <q>Jessie</q>, ces problèmes ont été corrigés dans la
version 3.16.36-1+deb8u2.</p>

<p>Nous vous recommandons de mettre à jour vos paquets linux.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS,
comment appliquer ces mises à jour dans votre système et les questions
fréquemment posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2016/dla-670.data"
# $Id: $
