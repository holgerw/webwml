#use wml::debian::translation-check translation="ce41d997301872adfc27a79ea546429856226b67" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Cette mise à jour corrige les CVE décrits ci-dessous.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-0821">CVE-2016-0821</a>

<p>Solar Designer a remarqué que la fonctionnalité de liste
<q>poisoning</q>, destinée à réduire les effets des bogues dans la
manipulation de liste dans le noyau, utilisait des valeurs de poison dans
la plage d'adresses virtuelles qui peuvent être allouées par les processus
de l'utilisateur.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-1583">CVE-2016-1583</a>

<p>Jann Horn du Project Zero de Google a signalé que le système de fichiers
eCryptfs pourrait être utilisé avec le système de fichiers proc pour
provoquer un dépassement de pile du noyau. Si le paquet ecryptfs-utils est
installé, des utilisateurs locaux pourraient exploiter cela, grâce au
programme mount.ecryptfs_private, pour provoquer un déni de service
(plantage) ou éventuellement une élévation de privilèges.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-2184">CVE-2016-2184</a>,
<a href="https://security-tracker.debian.org/tracker/CVE-2016-2185">CVE-2016-2185</a>,
<a href="https://security-tracker.debian.org/tracker/CVE-2016-2186">CVE-2016-2186</a>,
<a href="https://security-tracker.debian.org/tracker/CVE-2016-2187">CVE-2016-2187</a>,
<a href="https://security-tracker.debian.org/tracker/CVE-2016-3136">CVE-2016-3136</a>,
<a href="https://security-tracker.debian.org/tracker/CVE-2016-3137">CVE-2016-3137</a>,
<a href="https://security-tracker.debian.org/tracker/CVE-2016-3138">CVE-2016-3138</a>,
<a href="https://security-tracker.debian.org/tracker/CVE-2016-3140">CVE-2016-3140</a>

<p>Ralf Spenneberg de OpenSource Security a signalé que plusieurs pilotes
USB ne vérifiaient pas correctement les descripteurs USB. Cela permettait
à un utilisateur physiquement présent avec un périphérique USB conçu pour
l'occasion de provoquer un déni de service (plantage). Tous les pilotes
n'ont pas encore été corrigés.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-3134">CVE-2016-3134</a>

<p>L'équipe du Project Zero de Google a découvert que le sous-système netfilter
ne vérifiait pas correctement les entrées de la table filter. Un
utilisateur doté de la capacité CAP_NET_ADMIN pourrait utiliser cela pour
un déni de service (plantage) ou éventuellement une élévation de
privilèges.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-3157">CVE-2016-3157</a> / XSA-171

<p>Andy Lutomirski a découvert que l'implémentation du changement de tâches
x86_64 (amd64) n'actualisait pas correctement le niveau de droit des
entrées sorties lors d'une exécution comme client Xen paravirtuel (PV).
Avec certaines configurations cela pourrait permettre à des utilisateurs
locaux de provoquer un déni de service (plantage) ou une élévation de
privilèges dans le client.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-3672">CVE-2016-3672</a>

<p>Hector Marco et Ismael Ripoll ont noté qu'il était possible de
désactiver l'ASLR (<q>Address Space Layout Randomisation</q>) pour les
programmes x86_32 (i386) en supprimant la limite de ressource de la pile.
Cela facilite l'exploitation par des utilisateurs locaux de défauts de
sécurité dans les programmes réglés avec les drapeaux setuid ou setgid.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-3951">CVE-2016-3951</a>

<p>Le pilote cdc_ncm libérait prématurément la mémoire si certaines erreurs
survenaient durant son initialisation. Cela permettait à un utilisateur
physiquement présent avec un périphérique USB conçu pour l'occasion de
provoquer un déni de service (plantage) ou éventuellement pour une
élévation de privilèges.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-3955">CVE-2016-3955</a>

<p>Ignat Korchagin a signalé que le sous-système usbip ne vérifiait pas la
longueur des données reçues pour un tampon USB. Cela permettait un déni de
service (plantage) ou une augmentation de droits sur un système configuré
en client usbip, de la part du serveur usbip ou d'un attaquant capable
d'usurper son identité sur le réseau. Un système configuré en serveur usbip
pourrait être de la même manière vulnérable à des utilisateurs physiquement
présents.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-3961">CVE-2016-3961</a> / XSA-174

<p>Vitaly Kuznetsov de Red Hat a découvert que Linux permettait
l'utilisation de hugetlbfs sur les systèmes x86 (i386 et amd64) même
exécutés comme client Xen paravirtuel (PV), bien que Xen ne prend pas en
charge les très grandes pages. Cela permettait à des utilisateurs dotés
d'un accès à /dev/hugepages de provoquer un déni de service (plantage) dans
le client.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-4482">CVE-2016-4482</a>,
<a href="https://security-tracker.debian.org/tracker/CVE-2016-4485">CVE-2016-4485</a>,
<a href="https://security-tracker.debian.org/tracker/CVE-2016-4486">CVE-2016-4486</a>,
<a href="https://security-tracker.debian.org/tracker/CVE-2016-4569">CVE-2016-4569</a>,
<a href="https://security-tracker.debian.org/tracker/CVE-2016-4578">CVE-2016-4578</a>,
<a href="https://security-tracker.debian.org/tracker/CVE-2016-4580">CVE-2016-4580</a>,
<a href="https://security-tracker.debian.org/tracker/CVE-2016-5243">CVE-2016-5243</a>,
<a href="https://security-tracker.debian.org/tracker/CVE-2016-5244">CVE-2016-5244</a>

<p>Kangjie Lu a signalé que les fonctions devio, llc, rtnetlink, ALSA
timer, x25, tipc et rds d'USB divulguaient des informations de la pile du
noyau.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-4565">CVE-2016-4565</a>

<p>Jann Horn du Project Zero de Google a signalé que plusieurs composants de
la pile InfiniBand implémentaient des sémantiques inhabituelles pour
l'opération write(). Sur un système où les pilotes InfiniBand sont chargés,
des utilisateurs locaux pourraient utiliser cela pour provoquer un déni de
service ou une élévation de privilèges.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-4913">CVE-2016-4913</a>

<p>Al Viro a découvert que l'implémentation du système de fichiers ISO9660
ne mesurait pas correctement la longueur de certaines entrées de nom
incorrectes. La lecture d'un répertoire contenant de telles entrées de nom
pourrait divulguer des informations de la mémoire du noyau. Des
utilisateurs autorisés à monter des disques ou des images disque pourraient
utiliser cela pour obtenir des informations sensibles de la mémoire du
noyau.</p></li>

</ul>

<p>Pour Debian 7 <q>Wheezy</q>, ces problèmes ont été corrigés dans la
version 3.2.81-1.</p>

<p>Cette mise à jour corrige aussi le bogue nº 627782, qui provoquait une
corruption de données dans certaines applications exécutées sur un système
de fichiers aufs, et comprend beaucoup d'autres correctifs de bogue
à partir des mises à jour stables amont 3.2.79, 3.2.80 et 3.2.81.</p>

<p>Pour Debian 8 <q>Jessie</q>, ces problèmes seront bientôt corrigés.</p>

<p>Nous vous recommandons de mettre à jour vos paquets linux.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS,
comment appliquer ces mises à jour dans votre système et les questions
fréquemment posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2016/dla-516.data"
# $Id: $
