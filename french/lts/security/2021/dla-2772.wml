#use wml::debian::translation-check translation="c93bb9ffbb78f1b85b6f29d2686cbbd40e15ab28" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Plusieurs problèmes ont été corrigés dans TagLib, une bibliothèque pour
lire et éditer des métadonnées audio.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-12678">CVE-2017-12678</a>

<p>Un fichier audio contrefait pourrait aboutir à un plantage.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-11439">CVE-2018-11439</a>

<p>Un fichier audio contrefait pourrait aboutir à une divulgation
d'informations.</p></li>

<li><p>De plus, un bogue pouvant conduire à une corruption des fichiers ogg
a été corrigé.</p></li>

</ul>

<p>Pour Debian 9 « Stretch », ces problèmes ont été corrigés dans
la version 1.11.1+dfsg.1-0.3+deb9u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets taglib.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de taglib,
veuillez consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/taglib">\
https://security-tracker.debian.org/tracker/taglib</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment
posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2021/dla-2772.data"
# $Id: $
