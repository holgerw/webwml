#use wml::debian::translation-check translation="9aea6879e653c67617df94d6b9479fa2246cca1e" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Un problème de sécurité a été découvert dans sssd.</p>

<p>La commande sssctl était vulnérable à une injection de commande
d’interpréteur à l’aide des sous-commandes logs-fetch et cache-expire. Ce
défaut permettait à un attaquant d’amener le superutilisateur à exécuter une
commande sssctl contrefaite pour l'occasion, par exemple à l’aide de sudo,
pour obtenir un accès superutilisateur. Le plus grave danger de cette
vulnérabilité concerne la confidentialité, l’intégrité ainsi que la
disponibilité du système.</p>

<p>Pour Debian 9 « Stretch », ce problème a été corrigé dans
la version 1.15.0-3+deb9u2.</p>

<p>Nous vous recommandons de mettre à jour vos paquets sssd.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de sssd,
veuillez consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/sssd">\
https://security-tracker.debian.org/tracker/sssd</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment
posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2021/dla-2758.data"
# $Id: $
