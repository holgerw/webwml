#use wml::debian::translation-check translation="3680e37f30e4fc64894f81129852342a5d4630a8" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Plusieurs vulnérabilités de sécurité ont été découvertes dans aom, la
bibliothèque de codec vidéo AV1. Des dépassements de tampon, une utilisation de
mémoire après libération et des déréférencements de pointeur NULL pouvaient
causer un déni de service ou avoir un autre impact non précisé si un
fichier multimédia mal formé était traité.</p>

<p>Pour Debian 10 <q>Buster</q>, ces problèmes ont été corrigés dans
la version 1.0.0-3+deb10u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets aom.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de aom,
veuillez consulter sa page de suivi de sécurité à l'adresse :
<a rel="nofollow" href="https://security-tracker.debian.org/tracker/aom">\
https://security-tracker.debian.org/tracker/aom</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment
posées peuvent être trouvées sur : <a rel="nofollow
"href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3556.data"
# $Id: $
