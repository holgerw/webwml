#use wml::debian::translation-check translation="1c0f94490e6d66f504211dcb7a46bd61678a1e8f" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Santos Gallegos a découvert une inclusion inaperçue de fichier local dans
python-git, une bibliothèque Python pour interagir avec les dépôts Git, qui
pouvait conduire à un déni de service ou éventuellement à une divulgation
d'informations.</p>

<p>Dans le but de résoudre quelques références de git, python-git lit des
fichiers du répertoire <q><code>.git</code></q>, mais, à cause d’une
vérification incorrecte d’emplacement, un attaquant pouvait passer un fichier
situé en dehors de ce répertoire, et, par conséquent, python-git lisait un
fichier arbitraire sur le système.</p>

<p>Il n’est pas certain que l’attaquant pouvait accéder au contenu réel d’un
fichier, mais un déni de service pouvait être obtenu en passant un fichier très
grand ou sans fin tel que <code>/dev/random</code>.</p>

<p>Pour Debian 10 <q>Buster</q>, ce problème a été corrigé dans
la version 2.1.11-1+deb10u2.</p>

<p>Nous vous recommandons de mettre à jour vos paquets python-git.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de python-git,
veuillez consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/python-git">\
https://security-tracker.debian.org/tracker/python-git</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment
posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3589.data"
# $Id: $
