#use wml::debian::translation-check translation="a6ffbb62ebc46d5fcde7dcb1ed9ad7366fb499ae" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Batik est une boîte à outils pour des applications ou appliquettes voulant
utiliser des images au format SVG (Scalable Vector Graphics) pour diverses
utilisations telles que l’affichage, la création ou la manipulation.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-11987">CVE-2020-11987</a>

<p>Une contrefaçon de requête côté serveur (SSRF) a été découverte, provoquée
par une validation impropre d’entrée par NodePickerPanel. En utilisant un
argument contrefait pour l'occasion, un attaquant pouvait exploiter cette
vulnérabilité pour que le serveur sous-jacent fasse des requêtes GET
arbitraires.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-38398">CVE-2022-38398</a>

<p>Une vulnérabilité de contrefaçon de requête côté serveur (SSRF) a été
découverte qui permettait à un attaquant de charger une URL à l’aide du
protocole jar.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-38648">CVE-2022-38648</a>

<p>Une vulnérabilité de contrefaçon de requête côté serveur (SSRF) a été
découverte qui permettait à un attaquant de récupérer des ressources externes.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-40146">CVE-2022-40146</a>

<p>Une vulnérabilité de contrefaçon de requête côté serveur (SSRF) a été
découverte qui permettait à un attaquant d’accéder à des fichiers à l’aide d’une
URL Jar.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-44729">CVE-2022-44729</a>

<p>Une vulnérabilité de contrefaçon de requête côté serveur (SSRF) a été
découverte. Un fichier SVG malveillant pouvait déclencher un chargement de
ressources externes par défaut, provoquant une utilisation de ressources et
dans certains cas une divulgation d'informations.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-44730">CVE-2022-44730</a>

<p>Une vulnérabilité de contrefaçon de requête côté serveur (SSRF) a été
découverte. Un fichier SVG malveillant pouvait examiner le profil ou les données
d’un utilisateur et les envoyer directement comme paramètre à une URL.</p></li>

</ul>

<p>Pour Debian 10 <q>Buster</q>, ces problèmes ont été corrigés dans
la version 1.10-2+deb10u3.</p>

<p>Nous vous recommandons de mettre à jour vos paquets batik.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de batik,
veuillez consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/batik">\
https://security-tracker.debian.org/tracker/batik</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment
posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3619.data"
# $Id: $
