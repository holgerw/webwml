#use wml::debian::translation-check translation="51e3dfb0dfd2c226afd4811b348ee0d3aafdb416" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Un défaut a été découvert dans l’accroche <q>/v2/_catalog</q> dans
<q>distribution/distribution</q> qui accepte un paramètre pour contrôler le
nombre maximal d’enregistrements renvoyés (chaine demandée : <q>n</q>). Cette
vulnérabilité permettait à un utilisateur malveillant de soumettre une valeur
anormalement grande pour <q>n</q>, causant une allocation d’un tableau de
chaines massif, pouvant provoquer un déni de service à l’aide d’une utilisation
excessive de mémoire.</p>

<p>Pour Debian 10 <q>Buster</q>, ce problème a été corrigé dans
la version 2.6.2~ds1-2+deb10u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets docker-registry.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de docker-registry,
veuillez consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/docker-registry">\
https://security-tracker.debian.org/tracker/docker-registry</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment
posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3473.data"
# $Id: $
