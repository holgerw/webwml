#use wml::debian::translation-check translation="d9e56ea040e86facc335fcee27c1919d7d3aeb0d" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>

<p>Il a été découvert qu’il existait une vulnérabilité potentielle d’exécution
de code arbitraire dans <code>libwebp</code>, une bibliothèque de prise en
charge du format de compression d’image WebP.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-1999">CVE-2023-1999</a></li>

</ul>

<p>Pour Debian 10 <q>Buster</q>, ce problème a été corrigé dans
la version 0.6.1-2+deb10u2.</p>

<p>Nous vous recommandons de mettre à jour vos paquets libwebp.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment
posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3439.data"
# $Id: $
