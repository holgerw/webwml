#use wml::debian::translation-check translation="28ace4d40c3ae5090af250b5d9406e1292642b3f" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Stefan Walter a trouvé qu’udisks2, un service pour accéder et manipuler des
appareils de stockage, pouvait causer un déni de service en plantant le système
si un appareil ext2/3/4 corrompu ou contrefait pour l'occasion, ou une image,
était monté, ce qui pouvait survenir automatiquement dans certains
environnements.</p>

<p>Pour Debian 10 <q>Buster</q>, ce problème a été corrigé dans
la version 2.8.1-4+deb10u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets udisks2.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de udisks2,
veuillez consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/udisks2">\
https://security-tracker.debian.org/tracker/udisks2</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment
posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3387.data"
# $Id: $
