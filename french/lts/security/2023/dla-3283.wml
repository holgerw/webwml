#use wml::debian::translation-check translation="059bb2aea48fabb506d90bc786468c921c152681" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Plusieurs problèmes ont été trouvés dans modsecurity-apache, un moteur de
pare-feu d’application web (WAF) multiplateforme au code source ouvert pour
Apache, qui permettaient à des attaquants distants de contourner le pare-feu
d’application ou d’avoir un autre impact non précisé.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-48279">CVE-2022-48279</a>

<p>Dans ModSecurity avant la version 2.9.6 et les versions 3.x avant 3.0.8,
les requêtes HTTP multiparties étaient analysées incorrectement et pouvaient
contourner le pare-feu d’application web.
Remarque : cela est relatif au
<a href="https://security-tracker.debian.org/tracker/CVE-2022-39956">CVE-2022-39956</a>,
mais peut être considéré indépendant des modifications du code de base de
 ModSecurity (langage C).</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-24021">CVE-2023-24021</a>

<p>Le traitement incorrect de l’octet NULL dans le téléversement de fichier
dans ModSecurity avant la version 2.9.7 pouvait permettre des contournements
de pare-feu d’application web et des dépassements de tampon dans les pare-feux
d’application web lors de l’exécution de règles lisant la collection
FILES_TMP_CONTENT.</p></li>

</ul>

<p>Pour Debian 10 <q>Buster</q>, ces problèmes ont été corrigés dans
la version 2.9.3-1+deb10u2.</p>

<p>Nous vous recommandons de mettre à jour vos paquets modsecurity-apache.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de modsecurity-apache,
veuillez consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/modsecurity-apache">\
https://security-tracker.debian.org/tracker/modsecurity-apache</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment
posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3283.data"
# $Id: $
