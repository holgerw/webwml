#use wml::debian::translation-check translation="61231d89347fb764a3543b766c54d8fc3bcc64fb" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Plusieurs vulnérabilités ont été découvertes dans le langage de programmation
Go. Un attaquant pouvait déclencher un déni de service (DoS), un calcul erroné
du chiffrement, une fuite d'informations ou une exécution de code arbitraire sur
l’ordinateur du développeur dans certaines situations.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-28367">CVE-2020-28367</a>

<p>Une injection de code dans la commande go avec cgo permettait l’exécution
de code arbitraire au moment de la construction à l’aide de drapeaux gcc
malveillants à l'aide d'une directive #cgo.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-33196">CVE-2021-33196</a>

<p>Dans archive/zip, un compte de fichiers contrefait (dans l’en-tête) de
l’archive pouvait provoquer une panique NewReader ou OpenReader.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-36221">CVE-2021-36221</a>

<p>Go avait une situation de compétition qui pouvait conduire à une panique
net/http/httputil de ReverseProxy lors d’une interruption ErrAbortHandler.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-38297">CVE-2021-38297</a>

<p>Go avait un dépassement de tampon à l’aide de grands arguments dans une
invocation de fonction d’un module WASM quand GOARCH=wasm GOOS=js était utilisé.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-39293">CVE-2021-39293</a>

<p>Ce problème existait à cause d’un correctif incomplet pour le
<a href="https://security-tracker.debian.org/tracker/CVE-2021-33196">CVE-2021-33196</a>.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-41771">CVE-2021-41771</a>

<p>ImportedSymbols dans debug/macho (pour Open ou OpenFat) accédait à un
emplacement de mémoire après la fin du tampon, c’est-à-dire une situation
de partition hors limites.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-44716">CVE-2021-44716</a>

<p>net/http permettait une consommation de mémoire hors contrôle dans le cache
de canonisation d’en-tête à l’aide de requêtes HTTP/2.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-44717">CVE-2021-44717</a>

<p>Go dans UNIX permettait des opérations d’écriture dans un fichier non prévues
ou une connexion réseau non prévue conséquemment à une fermeture de fichier
du descripteur 0 de fichier après un épuisement de descripteurs de fichier.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-23806">CVE-2022-23806</a>

<p>Curve.IsOnCurve dans crypto/elliptic pouvait incorrectement renvoyer
<q>true</q> dans des situations avec une valeur big.Int qui n’était pas un
élément valable de champ.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-24921">CVE-2022-24921</a>

<p>regexp.Compile permettait un épuisement de pile à l'aide d’une expression
profondément imbriquée.</p></li>

</ul>

<p>Pour Debian 10 <q>Buster</q>, ces problèmes ont été corrigés dans
la version 1.11.6-1+deb10u6.</p>

<p>Nous vous recommandons de mettre à jour vos paquets golang-1.11.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de golang-1.11,
veuillez consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/golang-1.11">\
https://security-tracker.debian.org/tracker/golang-1.11</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment
posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3395.data"
# $Id: $
