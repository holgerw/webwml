#use wml::debian::translation-check translation="5d13d096403e47ec16bdd7c89fe336b7f4f1f96a" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Plusieurs vulnérabilités ont été corrigées dans Ceph, un système de stockage
distribué au code source ouvert s’exécutant sur du matériel commoditaire et
fournissant du stockage Bloc, Fichiers &amp; Objet.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-10222">CVE-2019-10222</a>

<p>Un déni de service a été corrigé : un attaquant non authentifié pouvait
planter le serveur Ceph RGW en envoyant un en-tête HTTP autorisé et terminant
la connexion, aboutissant à un déni de service distant pour les clients Ceph
RGW.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-1700">CVE-2020-1700</a>

<p>Un déni de service a été corrigé : un défaut a été découvert dans la façon
dont le frontal Ceph RGW gérait les déconnexions. Un attaquant authentifié
pouvait utiliser ce défaut en réalisant plusieurs essais de déconnexions
aboutissant à une perte permanente de connexion de socket par radosgw. Ce défaut
pouvait conduire à une condition de déni de service par accumulation de sockets
CLOSE_WAIT, conduisant finalement à l’épuisement des ressources disponibles,
empêchant les utilisateurs légitimes de se connecter au système.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-1760">CVE-2020-1760</a>

<p>Une attaque XSS a été corrigée : un défaut a été découvert dans la passerelle
Object de Ceph où étaient prises en charge les requêtes envoyées par les
utilisateurs anonymes dans Amazon S3. Ce défaut pouvait conduire à une attaque
XSS potentielle à cause de l’absence de neutralisation correcte de l’entrée non
sûre.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-10753">CVE-2020-10753</a>

<p>Une attaque par injection d’en-tête a été corrigée : il était possible
d’injecter des en-têtes HTTP à l'aide d'une balise CORS ExposeHeader dans le
compartiment d’Amazon S3. Un caractère de nouvelle ligne dans la balise
ExposeHeader dans le fichier de configuration de CORS générait une injection
d’en-tête dans la réponse quand la requête CORS était faite.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-12059">CVE-2020-12059</a>

<p>Un déni de service a été corrigé : une requête POST avec un balisage XML non
autorisé pouvait planter le processus RGW en déclenchant une exception de
pointeur NULL.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-25678">CVE-2020-25678</a>

<p>Une divulgation d'informations a été corrigée : ceph stocke les mots de passe
du module mgr en clair. Cela pouvait être découvert en parcourant les journaux
de mgr pour grafana et dashboard, avec les mots de passe visibles.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-27781">CVE-2020-27781</a>

<p>Une élévation des privilèges a été corrigée : les identifiants d’utilisateurs
pouvaient être manipulés et dérobés par des clients natifs de CephFS
d’OpenStack Manila, aboutissant à une élévation potentielle des privilèges. Un
utilisateur d’OpenStack Manila pouvait demander l’accès à un partage d’un
utilisateur cephx arbitraire, incluant les utilisateurs existants. La clé
d’accès était récupérée à l’aide des pilotes d’interface. Alors, tous les
utilisateurs du projet OpenStack concerné pouvaient voir la clé d’accès. Cela
permettait à l’attaquant de cibler n’importe quelles ressources accessibles par
l’utilisateur. Cela pouvait être réalisé même pour les utilisateurs
<q>admin</q>, compromettant l’administrateur de ceph.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-3524">CVE-2021-3524</a>

<p>De manière semblable au
<a href="https://security-tracker.debian.org/tracker/CVE-2020-10753">CVE-2020-10753</a>,
une attaque par injection d’en-tête a été corrigée : il était possible
d’injecter des en-têtes à l'aide d'une balise CORS ExposeHeader dans le
compartiment Amazon S3.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-3531">CVE-2021-3531</a>

<p>Un déni de service a été corrigé : lors du traitement d’une requête GET
dans le stockage RGW de Ceph pour un localisateur de Swift se terminant par deux
barres obliques, le rgw pouvait planter, aboutissant à un déni de service.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-3979">CVE-2021-3979</a>

<p>Une perte de confidentialité a été corrigée : un défaut de longueur de clé
a été découvert dans le stockage de Ceph. Un attaquant pouvait exploiter le fait
que la longueur de clé était passée incorrectement à un algorithme de
chiffrement pour créer une clé aléatoire plus faible et qui pouvait être
utilisée pour une perte de confidentialité et d’intégrité de disques chiffrés.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-20288">CVE-2021-20288</a>

<p>Une élévation potentielle des privilèges a été corrigée : lors du traitement
de requêtes CEPHX_GET_PRINCIPAL_SESSION_KEY, ignorance de CEPH_ENTITY_TYPE_AUTH
dans CephXServiceTicketRequest::keys.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-43040">CVE-2023-43040</a>

<p>Un défaut a été découvert dans Ceph RGW. Un utilisateur non privilégié
pouvait écrire dans n’importe quel compartiment accessible avec une clé donnée
si un <q>form-data</q> de POST contenait une clé appelée <q>bucket</q> avec une
valeur correspondant au compartiment (bucket) utilisé pour signer la requête. Il
en résultait qu’un utilisateur pouvait vraiment téléverser dans
n’importe quel compartiment accessible avec la clé d’accès indiquée aussi
longtemps que le compartiment dans la politique POST correspondait à celui de la
partie form de POST.</p></li>

</ul>

<p>Pour Debian 10 <q>Buster</q>, ces problèmes ont été corrigés dans
la version 12.2.11+dfsg1-2.1+deb10u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets ceph.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de ceph,
veuillez consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/ceph">\
https://security-tracker.debian.org/tracker/ceph</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment
posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3629.data"
# $Id: $
