#use wml::debian::translation-check translation="abf8c721917ac1d0fdfe7349609bc6e7f86e75c5" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Deux vulnérabilités ont été corrigées dans Apache Traffic Server, un serveur
mandataire direct et inverse.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-41752">CVE-2023-41752</a>

<p>s3_auth plugin expose AWSAccessKeyId.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-44487">CVE-2023-44487</a>

<p>Déni de service <q>Rapid Reset</q> HTTP/2.<p></li>

</ul>

<p>Pour Debian 10 <q>Buster</q>, ces problèmes ont été corrigés dans
la version 8.1.7-0+deb10u3.</p>

<p>Nous vous recommandons de mettre à jour vos paquets trafficserver.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de trafficserver,
veuillez consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/trafficserver">\
https://security-tracker.debian.org/tracker/trafficserver</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment
posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3645.data"
# $Id: $
