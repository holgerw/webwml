#use wml::debian::translation-check translation="3f11935a0eca28a732b32c0372f126b8629bd7ae" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Un dépassement de tampon pouvait se produire lors du calcul de la valeur
quantile en utilisant la bibliothèque de statistiques de GSL (GNU Scientific
Library). Le traitement de données d’entrée malveillante par la fonction
gsl_stats_quantile_from_sorted_data de la bibliothèque pouvait conduire à une
fin inattendue de l’application ou à l’exécution de code arbitraire.</p>

<p>Pour Debian 10 <q>Buster</q>, ce problème a été corrigé dans
la version 2.5+dfsg-6+deb10u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets gsl.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de gsl,
veuillez consulter sa page de suivi de sécurité à l'adresse :
<a rel="nofollow" href="https://security-tracker.debian.org/tracker/gsl">\
https://security-tracker.debian.org/tracker/gsl</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment
posées peuvent être trouvées sur : <a rel="nofollow
"href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3576.data"
# $Id: $
