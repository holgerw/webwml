#use wml::debian::translation-check translation="576b90d56ec4bb95d5d983006a255303fe1af6f3" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Kokorin Vsevolod a découvert une vulnérabilité de pollution de prototype dans
node-tough-cookie, une bibliothèque Cookie Jar et Cookies RFC6265 pour node.js.
Le problème est causé par le traitement impropre de Cookies lors de
l’utilisation de CookieJar dans le mode <code>rejectPublicSuffixes=false</code>.</p>

<p>Pour Debian 10 <q>Buster</q>, ce problème a été corrigé dans
la version 2.3.4+dfsg-1+deb10u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets node-tough-cookie.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de node-tough-cookie,
veuillez consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/node-tough-cookie">\
https://security-tracker.debian.org/tracker/node-tough-cookie</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment
posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3488.data"
# $Id: $
