#use wml::debian::translation-check translation="e6ebd1a674e7b670e0fcb1630528d2d7be1c43c0" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Des problèmes ont été trouvés dans lua5.3, un langage de programmation
puissant et léger conçu pour étendre des applications, qui pouvaient aboutir à
un déni de service.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-6706">CVE-2019-6706</a>

<p>Fady Osman a découvert une vulnérabilité d’utilisation après libération de
tas dans <code>lua_upvaluejoin()</code> dans <code>lapi.c</code>, qui pouvait
aboutir à un déni de service ou, éventuellement, à une exécution de code
arbitraire lors de l’appel à <code>debug.upvaluejoin()</code> avec des arguments
particuliers.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-24370">CVE-2020-24370</a>

<p>Yongheng Chen a découvert un problème de débordement de négation et d’erreur
de segmentation dans <code>getlocal()</code> et <code>setlocal()</code>, comme
le montrait <code>getlocal(3,2^31)</code>.</p></li>

</ul>

<p>Pour Debian 10 <q>Buster</q>, ces problèmes ont été corrigés dans
la version 5.3.3-1.1+deb10u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets lua5.3.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de lua5.3,
veuillez consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/lua5.3">\
https://security-tracker.debian.org/tracker/lua5.3</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment
posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3469.data"
# $Id: $
