#use wml::debian::translation-check translation="ab9943b7411933ccc6ce454fe9dad22a7e63aac3" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Deux vulnérabilités dans le protocole MMS à travers HTTP ont été corrigées
dans le lecteur de médias VLC qui a été aussi mis à niveau vers la dernière
version de l’amont.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-47359">CVE-2023-47359</a>

<p>Dépassement de tampon de tas dans le module MMSH.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-47360">CVE-2023-47360</a>

<p>Dépassement d'entier par le bas dans le module MMSH.</p></li>

</ul>

<p>Pour Debian 10 <q>Buster</q>, ces problèmes ont été corrigés dans
la version 3.0.20-0+deb10u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets vlc.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de vlc,
veuillez consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/vlc">\
https://security-tracker.debian.org/tracker/vlc</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment
posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3679.data"
# $Id: $
