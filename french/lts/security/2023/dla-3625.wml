#use wml::debian::translation-check translation="75d6226d7634428c9c6fdf57aff03b4e6afa9717" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Une fuite de mémoire a été découverte dans ruby-magick, une interface entre
Ruby et ImageMagick, qui pouvait conduire à un déni de service par épuisement
de mémoire.</p>

<p>Pour Debian 10 <q>Buster</q>, ce problème a été corrigé dans
la version 2.16.0-6+deb10u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets ruby-rmagick.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de ruby-rmagick,
veuillez consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/ruby-rmagick">\
https://security-tracker.debian.org/tracker/ruby-rmagick</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment
posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3625.data"
# $Id: $
