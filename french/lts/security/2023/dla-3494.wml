#use wml::debian::translation-check translation="ee854f537daa247031878ff0ad9d1b1bf73feac0" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>

<p>Il a été découvert qu’il existait un problème dans ruby-doorkeeper, un
fournisseur OAuth2 pour les applications Ruby on Rails. Doorkeeper
automatiquement traitait les requêtes d’authentification sans le consentement
de l’utilisateur pour les clients publics qui avaient auparavant été approuvés,
mais les clients publics sont de manière inhérente vulnérables à une usurpation
car leur identité ne peut pas être assurée.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-34246">CVE-2023-34246</a>

<p>Doorkeeper est un fournisseur OAuth2 pour Ruby on Rails/Grape. Avant la
version 5.6.6, Doorkeeper automatiquement traitait les requêtes
d’authentification sans le consentement de l’utilisateur pour les clients
publics qui avaient auparavant été approuvés, mais les clients publics sont de
manière inhérente vulnérables à une usurpation car leur identité ne peut pas
être assurée. Ce problème a été solutionné dans la version 5.6.6.</p></li>

</ul>

<p>Pour Debian 10 <q>Buster</q>, ce problème a été corrigé dans
la version 4.4.2-1+deb10u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets ruby-doorkeeper.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment
posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3494.data"
# $Id: $
