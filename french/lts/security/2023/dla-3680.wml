#use wml::debian::translation-check translation="4ad0cda6b8c4fa3098dccf1c3f9ee88154134b6e" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Un problème (<a href="https://security-tracker.debian.org/tracker/CVE-2022-48521">CVE-2022-48521</a>)
a été découvert dans OpenDKIM jusqu’à la version 2.10.3, et les versions 2.11.x
jusqu’à 2.11.0-Beta2. Il échouait à garder trace des nombres ordinaux lors de la
suppression les champs factices d’en-têtes Authentification-Results. Cela
permettait à un attaquant distant de contrefaire un message de courriel avec une
adresse d’expéditeur factice de telle façon que des programmes reposant sur
Authentification-Results de OpenDKIM traitaient le message comme ayant une
signature DKIM valable alors qu’en fait il n’en existait pas.</p>

<p>Pour Debian 10 <q>Buster</q>, ce problème a été corrigé dans
la version 2.11.0~alpha-12+deb10u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets opendkim.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de opendkim,
veuillez consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/opendkim">\
https://security-tracker.debian.org/tracker/opendkim</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment
posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3680.data"
# $Id: $
