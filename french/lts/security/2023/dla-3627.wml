#use wml::debian::translation-check translation="e5298bb083132842d6bca18461a1c973b20c7a2b" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>

<p>Il a été découvert qu’il existait une vulnérabilité de contournement
d’authentification dans Redis, une base de données populaire clé-valeur
similaire à memcached.</p>

<p>Au démarrage, Redis commence l’écoute d’un socket Unix avant d’adapter ses
permissions à la configuration fournie par l’utilisateur. Si un
<code>umask(2)</code> était utilisé, cela créait une situation de compétition
qui permettait, pendant un très court instant, à un autre processus d’établir
une connexion autrement non autorisée.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-45145">CVE-2023-45145</a>

<p>Redis est une base de données résidant en mémoire et persistante sur le
disque. Au démarrage, Redis commence l’écoute d’un socket Unix avant d’adapter
ses permissions à la configuration fournie par l’utilisateur. Si un
<code>umask(2)</code> était utilisé, cela créait une situation de compétition
qui permettait, pendant un très court instant, à un autre processus d’établir
une connexion autrement non autorisée. Ce problème existait depuis
Redis 2.6.0-RC1 et a été corrigé dans les versions 7.2.2, 7.0.14 et 6.2.14.
Il est conseillé aux utilisateurs de mettre à niveau. En cas d’impossibilité, un
contournement peut être réalisé en désactivant les sockets Unix et démarrant
Redis avec un umask restrictif ou en stockant le fichier de socket Unix dans un
répertoire sécurisé.</p></li>

</ul>

<p>Pour Debian 10 <q>Buster</q>, ce problème a été corrigé dans
la version 5:5.0.14-1+deb10u5.</p>

<p>Nous vous recommandons de mettre à jour vos paquets redis.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment
posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3627.data"
# $Id: $
