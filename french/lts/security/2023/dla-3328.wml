#use wml::debian::translation-check translation="f83e04033298b6ecb60efbb38ca7f60d9be396e6" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Deux vulnérabilités ont été découvertes dans la boîte à outils antivirus
ClamAV, qui pouvaient aboutir à l’exécution de code arbitraire ou à la
divulgation d'informations lors de l’analyse de fichiers malveillants HFS+ ou
DMG.</p>

<p>Pour Debian 10 <q>Buster</q>, ces problèmes ont été corrigés dans
la version 0.103.8+dfsg-0+deb10u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets clamav.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de clamav,
veuillez consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/clamav">\
https://security-tracker.debian.org/tracker/clamav</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment
posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3328.data"
# $Id: $
