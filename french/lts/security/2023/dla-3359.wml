#use wml::debian::translation-check translation="1852009a0a3739c2ddbb426f310ed347889e4d2e" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>libapache2-mod-auth-mellon, un module d’authentification SAML 2.0 pour
Apache, a été signalé comme ayant les vulnérabilités suivantes.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-13038">CVE-2019-13038</a>

<p>mod_auth_mellon avait une redirection ouverte à l’aide de la sous-chaine
login?ReturnTo=, comme démontré en omettant <q>//</q> après http: dans l’URL
cible.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-3639">CVE-2021-3639</a>

<p>mod_auth_mellon ne nettoyait pas les URL logout correctement. Ce défaut
pouvait être utilisé par un attaquant pour faciliter des attaques par
hameçonnage en piégeant les utilisateurs à visiter un URL d’application web de
confiance qui redirigeait vers un serveur externe potentiellement
malveillant.</p></li>

</ul>

<p>Pour Debian 10 <q>Buster</q>, ces problèmes ont été corrigés dans
la version 0.14.2-1+deb10u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets libapache2-mod-auth-mellon.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de libapache2-mod-auth-mellon,
veuillez consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/libapache2-mod-auth-mellon">\
https://security-tracker.debian.org/tracker/libapache2-mod-auth-mellon</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment
posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3359.data"
# $Id: $
