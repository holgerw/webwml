#use wml::debian::translation-check translation="62abf6bfcea795212c2314ebad8e19007b3df76d" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>

<p>Il a été découvert qu’il existait certains problèmes dans graphite-web, un
outil pour des graphiques de statistiques en temps réel, etc.</p>

<p>Une série de vulnérabilités de script intersite (XSS) existait qui pouvait
être exploitée à distance. Ces problèmes existaient dans les composants Cookie
Handler, Template Name Handler et Absolute Time Range Handler.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-4728">CVE-2022-4728</a>

<p>Une vulnérabilité a été trouvée dans Graphite Web et classée comme
problématique. Cette vulnérabilité affecte du code non déterminé dans le
composant Cookie Handler. Sa manipulation conduisait à un script intersite.
VDB-216742 est l’identifiant assigné à cette vulnérabilité.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-4729">CVE-2022-4729</a>

<p>Une vulnérabilité a été trouvée dans Graphite Web et classée comme
problématique. Cette vulnérabilité affecte du code non déterminé dans le
composant Template Name Handler. Sa manipulation conduisait à un script
intersite. VDB-216743 est l’identifiant assigné à cette vulnérabilité.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-4730">CVE-2022-4730</a>

<p>Une vulnérabilité a été trouvée dans Graphite Web et classée comme
problématique. Cette vulnérabilité affecte du code non déterminé dans le
composant Absolute Time Range Handler. Sa manipulation conduisait à un script
intersite. VDB-216744 est l’identifiant assigné à cette vulnérabilité.</p></li>

</ul>

<p>Pour Debian 10 <q>Buster</q>, ces problèmes ont été corrigés dans
la version 1.1.4-3+deb10u2.</p>

<p>Nous vous recommandons de mettre à jour vos paquets graphite-web.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment
posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3309.data"
# $Id: $
