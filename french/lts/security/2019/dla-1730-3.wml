#use wml::debian::translation-check translation="7527b1d2f19a608701196edf0eb89ffbfed075fb" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>

<p>Divers problèmes de sécurité ont été en plus corrigés dans libssh2, une
implémentation de client SSH écrite en C.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-3859">CVE-2019-3859</a>

<p>Lors de l’investigation de l’impact de
 <a href="https://security-tracker.debian.org/tracker/CVE-2019-13115">CVE-2019-13115</a>
dans la version de libssh2 dans Jessie de Debian, il a été découvert que les problèmes
autour de <a href="https://security-tracker.debian.org/tracker/CVE-2019-3859">CVE-2019-3859</a>
n’ont pas été complètement résolus. Une comparaison minutieuse (lecture, analyse et
copie des modifications de code si nécessaire) du code de l’amont et de celui de
la version de libssh2 de Jessie a été réalisée et des vérifications de limites
et protections de dépassement d'entier ont été ajoutées au paquet.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-13115">CVE-2019-13115</a>

<p>Kevin Backhouse de semmle.com a découvert que les correctifs initiaux pour la
série de CVE, <a href="https://security-tracker.debian.org/tracker/CVE-2019-3855">CVE-2019-3855</a>
— 2019-3863, introduisaient plusieurs régressions à propos du signe des valeurs de longueur
renvoyées dans le code de l’amont. Lors de l’étude de la mise à jour de
<a href="https://security-tracker.debian.org/tracker/CVE-2019-3859">CVE-2019-3859</a>
mentionné ci-dessus, il a été fait attention de pas introduire ces régressions
de l’amont enregistrées sous
<a href="https://security-tracker.debian.org/tracker/CVE-2019-13115">CVE-2019-13115</a>.</p></li>

</ul>

<p>Pour Debian 8 <q>Jessie</q>, ces problèmes ont été corrigés dans
la version 1.4.3-4.1+deb8u4.</p>
<p>Nous vous recommandons de mettre à jour vos paquets libssh2.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2019/dla-1730-3.data"
# $Id: $
