#use wml::debian::translation-check translation="f35ee5af684ea87ef5dff444f837db3cababff15" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>La vulnérabilité suivante a été découverte dans le moteur web
WebKitGTK :</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-32886">CVE-2022-32886</a>

<p>P1umer, afang5472 et xmzyshypnc ont découvert que le traitement d'un
contenu web contrefait pouvait conduire à l'exécution de code arbitraire.</p></li>

</ul>

<p>Pour Debian 10 Buster, ce problème a été corrigé dans la version
2.38.0-1~deb10u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets webkit2gtk.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de webkit2gtk, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/webkit2gtk">\
https://security-tracker.debian.org/tracker/webkit2gtk</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS,
comment appliquer ces mises à jour dans votre système et les questions
fréquemment posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2022/dla-3124.data"
# $Id: $
