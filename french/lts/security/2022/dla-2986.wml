#use wml::debian::translation-check translation="720211a4adc17fb2c811055564a25012c91ab144" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Plusieurs vulnérabilités ont été découvertes dans le langage de
programmation Go. Un attaquant pourrait déclencher un déni de service (DoS)
ou un calcul de chiffrement non valable.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-23772">CVE-2022-23772</a>

<p>Rat.SetString dans math/big a un débordement qui peut conduire à une
consommation de mémoire non contrôlée.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-23806">CVE-2022-23806</a>

<p>Curve.IsOnCurve dans crypto/elliptic peut renvoyer <q>true</q> de façon
erronée dans les cas où une valeur big.Int n'est pas un élément de champ
valable.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-24921">CVE-2022-24921</a>

<p>regexp.Compile permet un épuisement de pile à l'aide d'une expression
profondément imbriquée.</p></li>

</ul>

<p>Pour Debian 9 <q>Stretch</q>, ces problèmes ont été corrigés dans la
version 1.8.1-1+deb9u5.</p>

<p>Nous vous recommandons de mettre à jour vos paquets golang-1.8.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de golang-1.8, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/golang-1.8">\
https://security-tracker.debian.org/tracker/golang-1.8</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS,
comment appliquer ces mises à jour dans votre système et les questions
fréquemment posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2022/dla-2986.data"
# $Id: $
