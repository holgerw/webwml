#use wml::debian::translation-check translation="4546340736e4513bb1ad1bb93aa79e6e8af1940a" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Cinq problèmes de sécurité ont été découverts dans libxml2, une boîte à
outils et un analyseur XML en C.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-9318">CVE-2016-9318</a>

<p>Les versions vulnérables n'offrent pas d'étiquette indiquant directement
que le document en cours peut être lu mais que d'autres fichiers ne peuvent
pas être ouverts, ce qui facilite la conduite par des attaquants distants
d'attaques d’injection d'entités externes XML (XXE) à l'aide d'un document
contrefait.</p>
</li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-5130">CVE-2017-5130</a>

<p>Un dépassement d'entier dans le code de débogage de la mémoire permettait
à un attaquant distant d'exploiter éventuellement une corruption de tas à
l'aide d'un fichier XML contrefait.</p>
</li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-5969">CVE-2017-5969</a>

<p>L'analyseur en mode récupération permet à des attaquants distants de
provoquer un déni de service (déréférencement de pointeur NULL) à l'aide
d'un document XML contrefait.</p>
</li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-16932">CVE-2017-16932</a>

<p>Lors de l'expansion d'une entité de paramètre dans une DTD, une récursion
infinie pouvait conduire à une boucle infinie ou à un épuisement de
mémoire.</p>
</li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-23308">CVE-2022-23308</a>

<p>L'application qui valide le code XML en utilisant xmlTextReaderRead()
avec XML_PARSE_DTDATTR et XML_PARSE_DTDVALID activés devient vulnérable à ce
bogue d'utilisation de mémoire après libération. Ce problème peut avoir pour
conséquence un déni de service.</p></li>

</ul>

<p>Pour Debian 9 <q>Stretch</q>, ces problèmes ont été corrigés dans la
version 2.9.4+dfsg1-2.2+deb9u6.</p>

<p>Nous vous recommandons de mettre à jour vos paquets libxml2.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de libxml2, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/libxml2">\
https://security-tracker.debian.org/tracker/libxml2</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS,
comment appliquer ces mises à jour dans votre système et les questions
fréquemment posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2022/dla-2972.data"
# $Id: $
