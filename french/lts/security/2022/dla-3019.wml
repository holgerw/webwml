#use wml::debian::translation-check translation="a5f52f07f00845cbab77ff17cdcb7c06a904b944" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Un problème de sécurité a été découvert dans admesh, un outil pour
calculer des maillages solides triangulés.</p>

<p>Un dépassement de tampon de tas en lecture a été détecté dans
stl_update_connects_remove_1 (appelé à partir de stl_remove_degenerate)
dans connect.c, ce qui pouvait mener à une corruption de mémoire et à
d'autres conséquences potentielles.</p>

<p>Pour Debian 9 <q>Stretch</q>, ce problème a été corrigé dans la version
0.98.2-3+deb9u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets admesh.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de admesh, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/admesh">\
https://security-tracker.debian.org/tracker/admesh</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS,
comment appliquer ces mises à jour dans votre système et les questions
fréquemment posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2022/dla-3019.data"
# $Id: $
