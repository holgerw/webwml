#use wml::debian::translation-check translation="fd5d7515b567c74158ef54ffadb98cbeb272e3d5" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Deux problèmes ont été découverts dans GDAL, une bibliothèque
d'abstraction de données géospatiales, qui pouvaient conduire à un déni de
service au moyen d'un plantage de l'application ou éventuellement à
l'exécution de code arbitraire lors de l'analyse de données contrefaites.</p>

<p>Pour Debian 10 Buster, ces problèmes ont été corrigés dans la version
2.4.0+dfsg-1+deb10u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets gdal.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de gdal, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/gdal">\
https://security-tracker.debian.org/tracker/gdal</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS,
comment appliquer ces mises à jour dans votre système et les questions
fréquemment posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2022/dla-3129.data"
# $Id: $
