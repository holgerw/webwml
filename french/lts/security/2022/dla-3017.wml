#use wml::debian::translation-check translation="55c7991cfa48234f174b9e868a56346683c5e2e7" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Jacek Konieczny a découvert une vulnérabilité d'injection SQL dans le
dorsal back-sql de slapd dans OpenLDAP, une implémentation libre du
protocole « Lightweight Directory Access Protocol » (LDAP), permettant à un
attaquant de modifier la base de données pendant une opération de recherche
de LDAP lors du traitement d'un filtre de recherche contrefait pour
l'occasion.</p>

<p>Pour Debian 9 <q>Stretch</q>, ce problème a été corrigé dans la version
2.4.44+dfsg-5+deb9u9.</p>

<p>Nous vous recommandons de mettre à jour vos paquets openldap.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de openldap, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/openldap">\
https://security-tracker.debian.org/tracker/openldap</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS,
comment appliquer ces mises à jour dans votre système et les questions
fréquemment posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2022/dla-3017.data"
# $Id: $
