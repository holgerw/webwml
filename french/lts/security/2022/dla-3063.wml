#use wml::debian::translation-check translation="630326f43324da9ad3ca61f0dc53db02cbdf6503" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Une vulnérabilité d'utilisation de mémoire de tas après libération a été
découverte dans systemd, un gestionnaire de système et de services, où des
requêtes asynchrones de Polkit sont réalisées pendant le traitement de
messages dbus. Un attaquant local non privilégié peut abuser de ce défaut
pour planter des services de systemd ou éventuellement exécuter du code et
élever ses privilèges, en envoyant des messages dbus contrefaits pour
l'occasion.</p>

<p>Pour Debian 9 <q>Stretch</q>, ce problème a été corrigé dans la version
232-25+deb9u14.</p>

<p>Nous vous recommandons de mettre à jour vos paquets systemd.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de systemd, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/systemd">\
https://security-tracker.debian.org/tracker/systemd</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS,
comment appliquer ces mises à jour dans votre système et les questions
fréquemment posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2022/dla-3063.data"
# $Id: $
