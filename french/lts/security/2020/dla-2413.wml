#use wml::debian::translation-check translation="9860429d04ce056f7fc566fc957bcfa30059839c" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Plusieurs vulnérabilités ont été trouvées dans le paquet phpmyadmin.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-19617">CVE-2019-19617</a>

<p>phpMyAdmin ne protège pas certaines informations de Git, relatives
à libraries/classes/Display/GitRevision.php et
libraries/classes/Footer.php.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-26934">CVE-2020-26934</a>

<p>Une vulnérabilité a été découverte par laquelle un attaquant peut provoquer
une attaque XSS à l’aide de la fonction de transformation.</p>

<p>Si un attaquant envoie un lien contrefait à la victime avec du JavaScript
malveillant, quand la victime clique sur le lien, le JavaScript s’exécute et
effectue les instructions conçues par l’attaquant.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-26935">CVE-2020-26935</a>

<p>Une vulnérabilité d’injection SQL a été découverte dans la façon dont
phpMyAdmin traite des constructions dans la fonction de recherche. Un attaquant
pourrait utiliser ce défaut pour injecter du code SQL malveillant dans une requête.</p></li>

</ul>

<p>Pour Debian 9 <q>Stretch</q>, ces problèmes ont été corrigés dans
la version 4.6.6-4+deb9u2.</p>

<p>Nous vous recommandons de mettre à jour vos paquets phpmyadmin.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de phpmyadmin, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/phpmyadmin">https://security-tracker.debian.org/tracker/phpmyadmin</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify lea following line
#include "$(ENGLISHDIR)/lts/security/2020/dla-2413.data"
# $Id: $
