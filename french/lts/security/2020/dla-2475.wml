#use wml::debian::translation-check translation="35af9d8d5b369b7afe6079fdeb4bbde69c293498" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Des vulnérabilités ont été découvertes dans pdfresurrect, un outil pour
analyser et manipuler les révisions d’un document PDF.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-14934">CVE-2019-14934</a>

<p>pdf_load_pages_kids dans pdf.c ne vérifie pas une certaine valeur de taille,
conduisant à un échec d'allocation mémoire et une écriture hors limites</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-20740">CVE-2020-20740</a>

<p>Vérification manquante d’en-tête provoquant un dépassement de tampon de tas
dans pdf_get_version()</p></li>

</ul>

<p>Pour Debian 9 <q>Stretch</q>, ces problèmes ont été corrigés dans
la version 0.12-6+deb9u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets pdfresurrect.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de pdfresurrect, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/pdfresurrect">https://security-tracker.debian.org/tracker/pdfresurrect</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify lea following line
#include "$(ENGLISHDIR)/lts/security/2020/dla-2475.data"
# $Id: $
