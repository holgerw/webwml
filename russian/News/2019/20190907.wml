#use wml::debian::translation-check translation="21882a152ab072f844cf526dc172ff70559e05e7" maintainer="Lev Lamberov"
<define-tag pagetitle>Обновлённый Debian 10: выпуск 10.1</define-tag>
<define-tag release_date>2019-09-07</define-tag>
#use wml::debian::news

<define-tag release>10</define-tag>
<define-tag codename>buster</define-tag>
<define-tag revision>10.1</define-tag>

<define-tag dsa>
    <tr><td align="center"><a href="$(HOME)/security/%0/dsa-%1">DSA-%1</a></td>
        <td align="center"><:
    my @p = ();
    for my $p (split (/,\s*/, "%2")) {
	push (@p, sprintf ('<a href="https://packages.debian.org/src:%s">%s</a>', $p, $p));
    }
    print join (", ", @p);
:></td></tr>
</define-tag>

<define-tag correction>
    <tr><td><a href="https://packages.debian.org/src:%0">%0</a></td>              <td>%1</td></tr>
</define-tag>

<define-tag srcpkg><a href="https://packages.debian.org/src:%0">%0</a></define-tag>

<p>Проект Debian с радостью сообщает о первом обновлении своего
стабильного выпуска Debian <release> (кодовое имя <q><codename></q>).
Это обновление в основном содержит исправления проблем безопасности,
а также несколько корректировок серьёзных проблем. Рекомендации по безопасности
опубликованы отдельно и указываются при необходимости.</p>

<p>Заметьте, что это обновление не является новой версией Debian
<release>, а лишь обновлением некоторых включённых в выпуск пакетов. Нет
необходимости выбрасывать старые носители с выпуском <q><codename></q>. После установки
пакеты можно обновить до текущих версий, используя актуальное
зеркало Debian.</p>

<p>Тем, кто часто устанавливает обновления с security.debian.org, не придётся
обновлять много пакетов, большинство обновлений с security.debian.org
включены в данное обновление.</p>

<p>Новые установочные образы будут доступны позже в обычном месте.</p>

<p>Обновление существующих систем до этой редакции можно выполнить с помощью
системы управления пакетами, используя одно из множества HTTP-зеркал Debian.
Исчерпывающий список зеркал доступен на странице:</p>

<div class="center">
  <a href="$(HOME)/mirror/list">https://www.debian.org/mirror/list</a>
</div>




<h2>Исправления различных ошибок</h2>

<p>Данное стабильное обновление вносит несколько важных исправлений для следующих пакетов:</p>

<table border=0>
<tr><th>Пакет</th>               <th>Причина</th></tr>
<correction acme-tiny "Поддержка грядущего изменения протокола ACME">
<correction android-sdk-meta "Новый выпуск основной ветки разработки; исправление регулярного выражения для добавления версии Debian к двоичным пакетам">
<correction apt-setup "Исправление предварительной настройки Secure Apt для локальных репозиториев через apt-setup/localX/">
<correction asterisk "Исправление переполнения буфера в res_pjsip_messaging [AST-2019-002 / CVE-2019-12827]; исправление вызываемой удалённо аварийной остановки в chan_sip [AST-2019-003 / CVE-2019-13161]">
<correction babeltrace "Изменение зависимости от символов ctf до версии, выпущенной перед слиянием">
<correction backup-manager "Исправление очистки удалённых архивов через FTP или SSH">
<correction base-files "Обновление для текущей редакции">
<correction basez "Корректное декодирование закодированных с помощью base64url строк">
<correction bro "Исправления безопасности [CVE-2018-16807 CVE-2018-17019]">
<correction bzip2 "Исправление регрессии при распаковке некоторых файлов">
<correction cacti "Исправление некоторых проблем с обновлениями с версии из stretch">
<correction calamares-settings-debian "Исправление прав доступа для образа initramfs, когда включено полное шифрование диска [CVE-2019-13179]">
<correction ceph "Повторная сборка с новой версией libbabeltrace">
<correction clamav "Предотвращение распаковки нерекурсивных zip-бомб; новый стабильный выпуск основной ветки разработки с исправлениями безопасности &mdash; добавление ограничения времени сканирования для снижения риска от zip-бомб [CVE-2019-12625]; исправление записи за пределами выделенного буфера памяти в bzip2-библиотеке NSIS [CVE-2019-12900]">
<correction cloudkitty "Исправление ошибок сборки с обновлённой версией SQLAlchemy">
<correction console-setup "Исправление проблем интернационализации при переключении локалей с Perl &gt;= 5.28">
<correction cryptsetup "Исправление поддержки заголовков LUKS2 без какого-либо привязанного слота ключей; исправление переполнения отображаемых сегментов на 32-битных архитектурах">
<correction cups "Исправление многочисленных проблем безопасности &mdash; переполнения SNMP-буфера [CVE-2019-8696 CVE-2019-8675], переполнение IPP-буфера, отказ в обслуживании и раскрытие содержимого памяти в планировщике">
<correction dbconfig-common "Исправление проблемы, вызванной изменениями в POSIX-поведении bash">
<correction debian-edu-config "Использование опции PXE <q>ipappend 2</q> для загрузки LTSP-клиента; исправление настройки sudo-ldap; исправление потери динамически выделяемого IPv4-адреса; несколько исправлений и улучшений debian-edu-config.fetch-ldap-cert">
<correction debian-edu-doc "Обновление Debian Edu Buster, а также руководств и переводов ITIL">
<correction dehydrated "Исправление загрузки информации об учётной записи; последующие исправления обработки идентификатора учётной записи и совместимости APIv1">
<correction devscripts "debchange: поддержка buster-backports в опции --bpo">
<correction dma "Отмена ограничения TLS-соединений версией TLS 1.0">
<correction dpdk "Новая стабильная версия основной ветки разработки">
<correction dput-ng "Добавление кодовых имён buster-backports и stretch-backports-sloppy">
<correction e2fsprogs "Исправление аварийных остановок e4defrag на 32-битных архитектурах">
<correction enigmail "Новый выпуск основной ветки разработки; исправления безопасности [CVE-2019-12269]">
<correction epiphany-browser "Проверка того, чтобы веб-расширение использовало поставляемую в его составе копию libdazzle">
<correction erlang-p1-pkix "Исправление обработки сертификатов GnuTLS">
<correction facter "Исправление грамматического разбора флагов пути Linux, не являющихся ключом/значением (например, onlink)">
<correction fdroidserver "Новая версия основной ветки разработки">
<correction fig2dev "Предотвращение ошибки сегментирования при использовании стрелок с круглыми и полукруглыми концами с увеличением более 42 [CVE-2019-14275]">
<correction firmware-nonfree "atheros: добавление прошивки Qualcomm Atheros QCA9377 rev 1.0 версии WLAN.TF.2.1-00021-QCARMSWP-1; realtek: добавление прошивки Realtek RTL8822CU Bluetooth; atheros: отмена изменений прошивки QCA9377 rev 1.0 в версии пакета 20180518-1; misc-nonfree: добавление прошивки для беспроводных чипов MediaTek MT76x0/MT76x2u, bluetooth-чипов MediaTek MT7622/MT7668, подписанной прошивки GV100">
<correction freeorion "Исправление аварийной остановки при загрузке или сохранении данных игры">
<correction fuse-emulator "Предпочтение движка X11, а не Wayland; показ иконки Fuse в GTK-окне и в диалоге About">
<correction fusiondirectory "Более строгие проверки поиска LDAP; добавление отсутствующей зависимости от php-xml">
<correction gcab "Исправление повреждения данных при распаковке">
<correction gdb "Повторная сборка с новой версией libbabeltrace">
<correction glib2.0 "Использование движка настроек GKeyFile для создания ~/.config и файлов настройки с ограниченными правами доступа [CVE-2019-13012]">
<correction gnome-bluetooth "Предотвращение аварийных остановок GNOME Shell при использовании gnome-shell-extension-bluetooth-quick-connect">
<correction gnome-control-center "Исправление аварийной остановки, когда выбрана панель Details -&gt; Overview (info-overview); исправление утечек памяти в панели Universal Access; исправление регрессии, из-за которой не работали опции отслеживания мыши Universal Access -&gt; Zoom; обновление переводов на исландский и японский языки">
<correction gnupg2 "Обратный перенос множества исправлений ошибок и заплат стабильности из основной ветки разработки; использование keys.openpgp.org в качестве сервера ключей по умолчанию; импортирование по умолчанию только своих собственных подписей">
<correction gnuplot "Исправление неполной/небезопасной инициализации массива ARGV">
<correction gosa "Более строгие проверки поиска LDAP">
<correction hfst "Более гладкие обновления с выпуска stretch">
<correction initramfs-tools "Отключение продолжения работы, если отсутствуют подходящие swap-устройства; MODULES=most: добавление всех модулей драйверов клавиатур, драйверов cros_ec_spi и SPI, extcon-usbc-cros-ec; MODULES=dep: добавление драйверов extcon">
<correction jython "Сохранение обратной совместимости с Java 7">
<correction lacme "Обновление с целью удаления поддержки неаутентифицированных GET-запросов из Let's Encrypt ACMEv2 API">
<correction libblockdev "Использование существующего API cryptsetup для изменения парольной фразы слота ключей">
<correction libdatetime-timezone-perl "Обновление поставляемых в пакете данных">
<correction libjavascript-beautifier-perl "Добавление поддержки для оператора <q>=&gt;</q>">
<correction libsdl2-image "Исправление переполнений буфера [CVE-2019-5058 CVE-2019-5052 CVE-2019-7635]; исправление обращения за пределами выделенного буфера памяти в коде обработки PCX [CVE-2019-12216 CVE-2019-12217 CVE-2019-12218 CVE-2019-12219 CVE-2019-12220 CVE-2019-12221 CVE-2019-12222 CVE-2019-5051]">
<correction libtk-img "Прекращение использования внутренних копий кодеков JPEG, Zlib и PixarLog, исправление аварийных остановок">
<correction libxslt "Исправление обхода системы безопасности [CVE-2019-11068], неинициализированного чтения токена xsl:number [CVE-2019-13117] и неинициализированного чтения с группирующими символами UTF-8 [CVE-2019-13118]">
<correction linux "Новый стабильный выпуск основной ветки разработки">
<correction linux-latest "Обновление с целью поддержки ABI ядра 4.19.0-6">
<correction linux-signed-amd64 "Новый стабильный выпуск основной ветки разработки">
<correction linux-signed-arm64 "Новый стабильный выпуск основной ветки разработки">
<correction linux-signed-i386 "Новый стабильный выпуск основной ветки разработки">
<correction lttv "Повторная сборка с новой версией libbabeltrace">
<correction mapproxy "Исправление возможностей WMS с Python 3.7">
<correction mariadb-10.3 "Новый стабильный выпуск основной ветки разработки; исправления безопасности [CVE-2019-2737 CVE-2019-2739 CVE-2019-2740 CVE-2019-2758 CVE-2019-2805]; исправление ошибки сегментирования при обращении 'information_schema'; переименование 'mariadbcheck' в 'mariadb-check'">
<correction musescore "Отключение функциональности webkit">
<correction ncbi-tools6 "Повторное создание пакета без несвободных данных/UniVec.*">
<correction ncurses "Удаление <q>rep</q> из xterm-new и производных описаний terminfo">
<correction netdata "Удаление Google Analytics из создаваемой документации; отключение отправки анонимной статистики; удаление кнопки <q>sign in</q>">
<correction newsboat "Исправление использования указателей после освобождения памяти">
<correction nextcloud-desktop "Добавление отсутствующей зависимости от nextcloud-desktop-common к пакету nextcloud-desktop-cmd">
<correction node-lodash "Исправление загрязнения прототипа [CVE-2019-10744]">
<correction node-mixin-deep "Исправление загрязнения прототипа">
<correction nss "Исправление проблем безопасности [CVE-2019-11719 CVE-2019-11727 CVE-2019-11729]">
<correction nx-libs "Исправление нескольких утечек памяти">
<correction open-infrastructure-compute-tools "Исправление запуска контейнера">
<correction open-vm-tools "Правильная обработка версий ОС вида <q>X</q>, а не <q>X.Y</q>">
<correction openldap "Ограничение rootDN proxyauthz собственной базой данных [CVE-2019-13057]; принудительное включение ACL-утверждения sasl_ssf при всяком соединении [CVE-2019-13565]; исправление ситуации, когда slapo-rwm не освобождал изначальный фильтр, если перезаписанный фильтр неверен">
<correction osinfo-db "Добавление информации о buster 10.0; исправление URL для загрузки stretch; исправление имени параметра, используемого для установки полного имени при создании файла предварительной настройки">
<correction osmpbf "Повторная сборка с protobuf 3.6.1">
<correction pam-u2f "Исправление небезопасной обработки отладочного файла [CVE-2019-12209]; исправление утечки дескриптора отладочного файла [CVE-2019-12210]; исправление обращения за пределами выделенного буфера памяти; исправление ошибки сегментирования, следующего за ошибкой выделения буфера">
<correction passwordsafe "Установка файлов локализации в правильный каталог">
<correction piuparts "Обновление настроек для выпуска buster; исправление нетипичной ошибки при удалении пакетов с именами, оканчивающимися на '+'; создание отдельных имён tar-архивов для chroot с --merged-usr">
<correction postgresql-common "Исправление ошибки <q>pg_upgradecluster из postgresql-common 200, 200+deb10u1, 201 и 202 повреждает настройки data_directory, если используется *дважды* для обновления кластера (например, 9.6 -&gt; 10 -&gt; 11)</q>">
<correction pulseaudio "Исправление восстановления состояния приглушения звука">
<correction puppet-module-cinder "Исправление попыток записи в /etc/init">
<correction python-autobahn "Исправление сборочных зависимостей pyqrcode">
<correction python-django "Новый выпуск исправления безопасности из основной ветки разработки [CVE-2019-12781]">
<correction raspi3-firmware "Добавление поддержки Raspberry Pi Compute Module 3 (CM3), Raspberry Pi Compute Module 3 Lite и Raspberry Pi Compute Module IO Board V3">
<correction reportbug "Обновление имён выпусков, следующих за выпуском buster; повторное включение запросов stretch-pu; исправление аварийных остановок при поиске пакета или версии; добавление отсутствующей зависимости от пакета sensible-utils">
<correction ruby-airbrussh "Прекращение вывода исключения при выводе некорректного UTF-8 в SSH">
<correction sdl-image1.2 "Исправление переполнений буфера [CVE-2019-5052 CVE-2019-7635], обращений за пределами выделенного буфера памяти [CVE-2019-12216 CVE-2019-12217 CVE-2019-12218 CVE-2019-12219 CVE-2019-12220 CVE-2019-12221 CVE-2019-12222 CVE-2019-5051]">
<correction sendmail "sendmail-bin.postinst, initscript: разрешение ситуации, когда start-stop-daemon совпадает по pid-файлу и исполняемому файлу; sendmail-bin.prerm: остановка sendmail до удаления альтернатив">
<correction slirp4netns "Новый стабильный выпуск основной ветки разработки с исправлениями безопасности &mdash; проверка результата sscanf при эмуляции ident [CVE-2019-9824]; исправление переполнения динамической памяти в поставляемой библиотеке libslirp [CVE-2019-14378]">
<correction systemd "Network: исправление ошибки включения интерфейса с ядром Linux 5.2; ask-password: предотвращение переполнения буфера при чтении из связки ключей; network: более вежливое поведение в случае отключения IPv6">
<correction tzdata "Новый выпуск основной ветки разработки">
<correction unzip "Исправление проблем, связанных с zip-бомбами [CVE-2019-13232]">
<correction usb.ids "Штатное обновление идентификаторов USB">
<correction warzone2100 "Исправление ошибки сегментирования при создании многопользовательской игры">
<correction webkit2gtk "Новый стабильный выпуск основной ветки разработки; прекращение требования ЦП с поддержкой SSE2">
<correction win32-loader "Повторная сборка с текущими пакетами, в частности, с пакетом debian-archive-keyring; исправление ошибки сборки путём принудительного использования локали POSIX">
<correction xymon "Исправление нескольких (только серверных) проблем безопасности [CVE-2019-13273 CVE-2019-13274 CVE-2019-13451 CVE-2019-13452 CVE-2019-13455 CVE-2019-13484 CVE-2019-13485 CVE-2019-13486]">
<correction yubikey-personalization "Обратный перенос дополнительных средств обеспечения безопасности">
<correction z3 "Прекращение установки SONAME для libz3java.so в значение libz3.so.4">
</table>


<h2>Обновления безопасности</h2>


<p>В данный выпуск внесены следующие обновления безопасности. Команда
безопасности уже выпустила рекомендации для каждого
из этих обновлений:</p>

<table border=0>
<tr><th>Идентификационный номер рекомендации</th>  <th>Пакет</th></tr>
<dsa 2019 4477 zeromq3>
<dsa 2019 4478 dosbox>
<dsa 2019 4479 firefox-esr>
<dsa 2019 4480 redis>
<dsa 2019 4481 ruby-mini-magick>
<dsa 2019 4482 thunderbird>
<dsa 2019 4483 libreoffice>
<dsa 2019 4484 linux>
<dsa 2019 4484 linux-signed-i386>
<dsa 2019 4484 linux-signed-arm64>
<dsa 2019 4484 linux-signed-amd64>
<dsa 2019 4486 openjdk-11>
<dsa 2019 4488 exim4>
<dsa 2019 4489 patch>
<dsa 2019 4490 subversion>
<dsa 2019 4491 proftpd-dfsg>
<dsa 2019 4493 postgresql-11>
<dsa 2019 4494 kconfig>
<dsa 2019 4495 linux-signed-amd64>
<dsa 2019 4495 linux-signed-arm64>
<dsa 2019 4495 linux>
<dsa 2019 4495 linux-signed-i386>
<dsa 2019 4496 pango1.0>
<dsa 2019 4498 python-django>
<dsa 2019 4499 ghostscript>
<dsa 2019 4501 libreoffice>
<dsa 2019 4502 ffmpeg>
<dsa 2019 4503 golang-1.11>
<dsa 2019 4504 vlc>
<dsa 2019 4505 nginx>
<dsa 2019 4507 squid>
<dsa 2019 4508 h2o>
<dsa 2019 4509 apache2>
<dsa 2019 4510 dovecot>
</table>


<h2>Удалённые пакеты</h2>

<p>Следующие пакеты были удалены из-за обстоятельств, на которые мы не
можем повлиять:</p>


<table border=0>
<tr><th>Пакет</th>               <th>Причина</th></tr>
<correction pump "Не сопровождается; проблемы безопасности">
<correction rustc "Удаление устаревшего rust-doc">

</table>

<h2>Программа установки Debian</h2>

Программа установки была обновлена с целью включения исправлений, добавленных в
данную редакцию стабильного выпуска.

<h2>URL</h2>

<p>Полный список пакетов, которые были изменены в данной редакции:</p>

<div class="center">
  <url "http://ftp.debian.org/debian/dists/<downcase <codename>>/ChangeLog">
</div>

<p>Текущий стабильный выпуск:</p>

<div class="center">
  <url "http://ftp.debian.org/debian/dists/stable/">
</div>

<p>Предлагаемые обновления для стабильного выпуска:</p>

<div class="center">
  <url "http://ftp.debian.org/debian/dists/proposed-updates">
</div>

<p>Информация о стабильном выпуске (информация о выпуске, известные ошибки и т. д.):</p>

<div class="center">
  <a
  href="$(HOME)/releases/stable/">https://www.debian.org/releases/stable/</a>
</div>

<p>Анонсы безопасности и информация:</p>

<div class="center">
  <a href="$(HOME)/security/">https://www.debian.org/security/</a>
</div>

<h2>О Debian</h2>

<p>Проект Debian &mdash; объединение разработчиков свободного программного обеспечения,
которые жертвуют своё время и знания для создания абсолютно свободной
операционной системы Debian.</p>

<h2>Контактная информация</h2>

<p>Более подробную информацию вы можете получить на сайте Debian
<a href="$(HOME)/">https://www.debian.org/</a>, либо отправив письмо по адресу
&lt;press@debian.org&gt;, либо связавшись с командой стабильного выпуска по адресу
&lt;debian-release@lists.debian.org&gt;.</p>
