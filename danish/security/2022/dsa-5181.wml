#use wml::debian::translation-check translation="3f23b56e7451102ddf68b78669a3f961f3d25e97" mindelta="1"
<define-tag description>sikkerhedsopdatering</define-tag>
<define-tag moreinfo>
<p>Adskillige sårbarheder er opdaget i Request Tracker, et udvidbart system til 
sporing af fejlrapporter.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-25802">CVE-2022-25802</a>

    <p>Man opdagede at Request Tracker var sårbar over for et angreb i 
    forbindelse med udførelse af skripter på tværs af websteder (XSS), når der 
    blev vist indhold med forfalskede indholdstyper.</p></li>

</ul>

<p>Yderligere opdagede man at Request Tracker ikke udførte en komplet 
rettighedskontrol ved tilgang til skræddersyede fil- eller billedtypefelter, 
muligvis gørende det muligt at tilgå disse skræddersyede felter for brugere uden 
rettigheder til at tilgå de tilknyttede objekter, medførende 
informationsafsløring.</p>

<p>I den gamle stabile distribution (buster), er disse problemer rettet
i version 4.4.3-2+deb10u2.</p>

<p>I den stabile distribution (bullseye), er disse problemer rettet i
version 4.4.4+dfsg-2+deb11u2.</p>

<p>Vi anbefaler at du opgraderer dine request-tracker4-pakker.</p>

<p>For detaljeret sikkerhedsstatus vedrørende request-tracker4, se
dens sikkerhedssporingsside på:
<a href="https://security-tracker.debian.org/tracker/request-tracker4">\
https://security-tracker.debian.org/tracker/request-tracker4</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2022/dsa-5181.data"
