# translation of others.po to Arabic
# Isam Bayazidi <isam@bayazidi.net>, 2004.
# Ossama M. Khayat <okhayat@yahoo.com>, 2005.
msgid ""
msgstr ""
"Project-Id-Version: \n"
"POT-Creation-Date: \n"
"PO-Revision-Date: 2023-11-23 13:54+0000\n"
"Last-Translator: Debian l10n Arabic <debian-l10n-arabic@lists.debian.org>\n"
"Language-Team: Debian l10n Arabic <debian-l10n-arabic@lists.debian.org>\n"
"Language: ar\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=6; plural=n==0 ? 0 : n==1 ? 1 : n==2 ? 2 : n%100>=3 "
"&& n%100<=10 ? 3 : n%100>=11 ? 4 : 5;\n"
"X-Generator: Poedit 3.2.2\n"

#: ../../english/devel/join/nm-steps.inc:7
msgid "New Members Corner"
msgstr "ركن الأعضاء الجدد"

#: ../../english/devel/join/nm-steps.inc:10
msgid "Step 1"
msgstr "خطوة 1"

#: ../../english/devel/join/nm-steps.inc:11
msgid "Step 2"
msgstr "خطوة 2"

#: ../../english/devel/join/nm-steps.inc:12
msgid "Step 3"
msgstr "خطوة 3"

#: ../../english/devel/join/nm-steps.inc:13
msgid "Step 4"
msgstr "خطوة 4"

#: ../../english/devel/join/nm-steps.inc:14
msgid "Step 5"
msgstr "خطوة 5"

#: ../../english/devel/join/nm-steps.inc:15
msgid "Step 6"
msgstr "خطوة 6"

#: ../../english/devel/join/nm-steps.inc:16
msgid "Step 7"
msgstr "خطوة 7"

#: ../../english/devel/join/nm-steps.inc:19
msgid "Applicants' checklist"
msgstr "قائمة متطلبات المتقدمين"

#: ../../english/devel/website/tc.data:11
msgid ""
"See <a href=\"m4_HOME/intl/french/\">https://www.debian.org/intl/french/</a> "
"(only available in French) for more information."
msgstr ""
"انظر <a href=\"m4_HOME/intl/french/\">https://www.debian.org/intl/french/</"
"a> (متوفرة بالفرنسية فقط) للمزيد من المعلومات."

#: ../../english/devel/website/tc.data:12
#: ../../english/devel/website/tc.data:14
#: ../../english/devel/website/tc.data:15
#: ../../english/devel/website/tc.data:16
#: ../../english/events/merchandise.def:145
msgid "More information"
msgstr "مزيد من المعلومات"

#: ../../english/devel/website/tc.data:13
msgid ""
"See <a href=\"m4_HOME/intl/spanish/\">https://www.debian.org/intl/spanish/</"
"a> (only available in Spanish) for more information."
msgstr ""
"انظر <a href=\"m4_HOME/intl/spanish/\">https://www.debian.org/intl/spanish/</"
"a> (متوفرة فقط بالأسبانية) للمزيد من المعلومات."

#: ../../english/distrib/pre-installed.defs:18
msgid "Phone"
msgstr "الهاتف"

#: ../../english/distrib/pre-installed.defs:19
msgid "Fax"
msgstr "الفاكس"

#: ../../english/distrib/pre-installed.defs:21
msgid "Address"
msgstr "العنوان"

#: ../../english/events/merchandise.def:13
msgid "Products"
msgstr "المنتجات"

#: ../../english/events/merchandise.def:16
msgid "T-shirts"
msgstr "أقمصة"

#: ../../english/events/merchandise.def:19
msgid "hats"
msgstr "قبعات"

#: ../../english/events/merchandise.def:22
msgid "stickers"
msgstr "ملصقات"

#: ../../english/events/merchandise.def:25
msgid "mugs"
msgstr "أكواب"

#: ../../english/events/merchandise.def:28
msgid "other clothing"
msgstr "ألبسة أخرى"

#: ../../english/events/merchandise.def:31
msgid "polo shirts"
msgstr "أقمصة بولو"

#: ../../english/events/merchandise.def:34
msgid "frisbees"
msgstr "فرِزبي"

#: ../../english/events/merchandise.def:37
msgid "mouse pads"
msgstr "لوحة ماوس"

#: ../../english/events/merchandise.def:40
msgid "badges"
msgstr "شارات"

#: ../../english/events/merchandise.def:43
msgid "basketball goals"
msgstr "شباك مرمى كرة سلة"

#: ../../english/events/merchandise.def:47
msgid "earrings"
msgstr "أقراط آذان"

#: ../../english/events/merchandise.def:50
msgid "suitcases"
msgstr "حقائب"

#: ../../english/events/merchandise.def:53
msgid "umbrellas"
msgstr ""

#: ../../english/events/merchandise.def:56
msgid "pillowcases"
msgstr ""

#: ../../english/events/merchandise.def:59
msgid "keychains"
msgstr ""

#: ../../english/events/merchandise.def:62
msgid "Swiss army knives"
msgstr ""

#: ../../english/events/merchandise.def:65
msgid "USB-Sticks"
msgstr ""

#: ../../english/events/merchandise.def:80
msgid "lanyards"
msgstr ""

#: ../../english/events/merchandise.def:83
msgid "others"
msgstr ""

#: ../../english/events/merchandise.def:90
msgid "Available languages:"
msgstr ""

#: ../../english/events/merchandise.def:107
msgid "International delivery:"
msgstr ""

#: ../../english/events/merchandise.def:118
msgid "within Europe"
msgstr ""

#: ../../english/events/merchandise.def:122
msgid "Original country:"
msgstr ""

#: ../../english/events/merchandise.def:187
msgid "Donates money to Debian"
msgstr ""

#: ../../english/events/merchandise.def:192
msgid "Money is used to organize local free software events"
msgstr ""

#: ../../english/logos/index.data:6
msgid "With&nbsp;``Debian''"
msgstr "مع &nbsp;``دبيان''"

#: ../../english/logos/index.data:9
msgid "Without&nbsp;``Debian''"
msgstr "بدون &nbsp;``دبيان''"

#: ../../english/logos/index.data:12
msgid "Encapsulated PostScript"
msgstr "Encapsulated PostScript"

#: ../../english/logos/index.data:18
msgid "[Powered by Debian]"
msgstr "[يدعمه دبيان]"

#: ../../english/logos/index.data:21
msgid "[Powered by Debian GNU/Linux]"
msgstr "[يدعمه دبيان جنو/لينكس]"

#: ../../english/logos/index.data:24
msgid "[Debian powered]"
msgstr "[يدعمه دبيان]"

#: ../../english/logos/index.data:27
msgid "[Debian] (mini button)"
msgstr "[دبيان] (زر مصغّر)"

#: ../../english/mirror/submit.inc:7
msgid "same as the above"
msgstr ""

#: ../../english/women/profiles/profiles.def:24
msgid "How long have you been using Debian?"
msgstr ""

#: ../../english/women/profiles/profiles.def:27
msgid "Are you a Debian Developer?"
msgstr "هل أنت مطور لدبيان ؟"

#: ../../english/women/profiles/profiles.def:30
msgid "What areas of Debian are you involved in?"
msgstr ""

#: ../../english/women/profiles/profiles.def:33
msgid "What got you interested in working with Debian?"
msgstr ""

#: ../../english/women/profiles/profiles.def:36
msgid ""
"Do you have any tips for women interested in getting more involved with "
"Debian?"
msgstr ""

#: ../../english/women/profiles/profiles.def:39
msgid ""
"Are you involved with any other women in technology group? Which one(s)?"
msgstr ""

#: ../../english/women/profiles/profiles.def:42
msgid "A bit more about you..."
msgstr ""
