#use wml::debian::template title="Free의 뜻은?" NOHEADER="yes"
#use wml::debian::translation-check translation="2de158b0e259c30eefd06baf983e4930de3d7412"

<link href="$(HOME)/font-awesome.css" rel="stylesheet" type="text/css">

<div id="toc">
  <ul class="toc">
    <li><a href="#freesoftware">...에서처럼 무료?</a></li>
    <li><a href="#licenses">소프트웨어 라이선스</a></li>
    <li><a href="#choose">어떻게 라이선스를 선택하나요?</a></li>
  </ul>
</div>

<aside>
<p><span class="fas fa-caret-right fa-3x"></span> 1998년 2월, 사람들이 모여 <a href="https://www.gnu.org/philosophy/free-sw">자유소프트웨어(Free Software)</a>라는 용어를 <a href="https://opensource.org/docs/definition.html">오픈소스 소프트웨어(Open Source Software)</a>로 바꾸려고 했습니다. 용어에 대한 토론 과정에서 내재된 철학적 차이를 드러냈지만, 자유소프트웨어나 오픈소스 소프트웨어 모두 그 실질적인 요구 조건 및 기타 이 사이트에서 다뤄진 내용은 완전히 동일합니다.</p>
</aside>

<h2><a id="freesoftware">...에서처럼 무료?</a></h2>

<p>
이 주제를 처음 접하는 많은 사람들은 "free"라는 단어 때문에 혼란스러워합니다.
그들이 기대하는 방식으로 사용되지 않습니다. – "free"는 그들에게 "비용 없음"을 의미합니다.
영어 사전을 보면 "free"에 대해 거의 20가지 다른 뜻이 나열되어 있으며, 그 중 하나만 "비용 없음"입니다.
나머지는 "자유"와 "제약 부족"을 나타냅니다.
따라서, 우리가 자유 소프트웨어에 대해 말할 때 우리는 지불이 아니라 자유를 뜻합니다.
</p>

<p>
소프트웨어는 무료로 설명되지만
비용을 지불할 필요가 없다는 의미에서만 무료는 거의 없으며,
전달하는 것이 금지될 수 있으며 수정할 수 없는 것이 가장 확실합니다.
무료로 라이선스된 소프트웨어는 일반적으로 관련 제품을 홍보하거나
소규모 경쟁업체를 폐업시키기 위한 마케팅 캠페인의 무기입니다.
무료로 유지된다는 보장은 없습니다.
</p>

<p>
초심자에게 소프트웨어는 무료이거나 그렇지 않습니다.
그러나, 실생활은 그보다 훨씬 더 복잡합니다.
사람들이 자유 소프트웨어에 대해 말할 때 의미하는 바를 이해하기 위해
조금 우회하여 소프트웨어 라이선스 세계로 들어가 보겠습니다.
</p>

<h2><a id="licenses">소프트웨어 라이선스</a></h2>

<p>
저작권은 특정 유형의 저작물에 대한 창작자의 권리를 보호하는 한 가지 방법입니다.
대부분의 국가에서 새로 작성된 소프트웨어는 자동으로 저작권이 있습니다.
라이선스는 다른 사람들이 자신의 창작물(이 경우 소프트웨어)을 수용 가능한 방식으로 사용하도록 허용하는 작성자의 방법입니다.
소프트웨어 사용 방법을 선언하는 라이선스를 포함하는 것은 작성자의 몫입니다.
</p>

<p style="text-align:center"><button type="button"><span class="fas fa-book-open fa-2x"></span> <a href="https://www.copyright.gov/">저작권에 대해 더 읽어 보세요</a></button></p>

<p>
물론 상황에 따라 다른 라이선스가 필요합니다.
소프트웨어 회사는 자산을 보호할 방법을 찾고 있으므로
사람이 읽을 수 없는 컴파일된 코드만 릴리스하는 경우가 많습니다.
그들은 또한 소프트웨어 사용에 많은 제한을 두었습니다.
반면, 자유 소프트웨어의 작성자는 대부분 다른 규칙을 찾고 있으며, 때로는 다음 사항을 조합하기도 합니다:
</p>

<ul>
  <li>독점 소프트웨어에서 그들의 코드를 사용하는 것은 허용하지 않습니다.
  모든 사람이 사용할 수 있도록 소프트웨어를 출시하기 때문에
  다른 사람이 소프트웨어를 훔치는 것을 보고 싶지 않습니다.
  이 경우 코드 사용은 신뢰로 간주됩니다:
  동일한 규칙에 따라 플레이하는 한 사용할 수 있습니다.</li>
  <li>저자의 신원을 보호해야 합니다.
  사람들은 자신의 작업에 큰 자부심을 갖고,
  다른 사람들이 크레딧에서 자신의 이름을 지우거나 심지어 자신이 썼다고 주장하는 것을 바라지 않습니다.</li>
  <li>소스 코드를 배포해야 합니다.
  독점 소프트웨어의 한 가지 주요 문제는 소스 코드를 사용할 수 없기 때문에 버그를 수정하거나 사용자 지정할 수 없다는 것입니다.
  또한 공급업체는 사용자의 하드웨어 지원을 중단하기로 결정할 수 있습니다.
  대부분의 무료 라이선스에 따라 소스 코드 배포는
  사용자가 소프트웨어를 사용자 정의하고 필요에 맞게 조정할 수 있도록 하여
  사용자를 보호합니다.</li>
  <li>저자 작업의 일부를 포함하는 모든 작업(저작권 논의에서는 "파생 작업"이라고도 함)은
  동일한 라이선스를 사용해야 합니다.</li>
</ul>

<aside>
<p><span class="fas fa-caret-right fa-3x"></span>
가장 널리 사용되는 세 가지 자유 소프트웨어 라이선스는 <a
href="https://www.gnu.org/copyleft/gpl.html">GNU General Public License (GPL)</a>, <a
href="https://opensource.org/licenses/artistic-license.php">Artistic License</a>, 그리고 <a
href="https://opensource.org/licenses/BSD-3-Clause">BSD 스타일 라이선스</a>입니다.
</aside>

<h2><a id="choose">어떻게 라이선스를 선택하나요?</a></h2>

<p>
때때로 사람들은 문제가 될 수 있는 자신의 라이센스를 작성하여,
자유 소프트웨어 커뮤니티에서 눈살을 찌푸리게 됩니다.
너무 자주, 표현이 모호하거나 사람들이 서로 충돌하는 조건을 만듭니다.
법정에서 유효한 라이센스를 작성하는 것은 훨씬 더 어렵습니다.
운 좋게도 선택할 수 있는 오픈 소스 라이선스가 많이 있습니다.
공통점:
</p>

<ul>
  <li>사용자는 원하는 만큼 컴퓨터에 소프트웨어를 설치할 수 있다.</li>
  <li>여러 사람이 한 번에 소프트웨어를 사용할 수 있다.</li>
  <li>사용자는 자신이 원하는 만큼 소프트웨어 사본을 만들 수 있으며
  이러한 사본을 다른 사용자에게 제공할 수도 있다
  (자유 또는 공개 재배포).</li>
  <li>소프트웨어 수정에 대한 제한이 없다(특정 크레딧 유지 제외).</li>
  <li>사용자는 소프트웨어를 배포 뿐 아니라 판매할 수도 있다.</li>
</ul>

<p>
특히 마지막 부분, 사람들이 소프트웨어를 판매할 수 있게 하는 것은
자유 소프트웨어의 전체 개념에 반대되는 것처럼 보이지만
실제로는 장점 중 하나입니다.
라이센스는 무료 재배포를 허용하기 때문에
누군가가 사본을 얻으면 배포할 수도 있습니다.
사람들은 심지어 그것을 팔려고 할 수도 있습니다.
</p>

<p>
자유 소프트웨어는 제약이 완전히 없는 것은 아니지만
사용자에게 작업을 완료하기 위해 필요한 작업을 수행할 수 있는 유연성을 제공합니다.
동시에 작가의 권리가 보호됩니다. – 이제 그것이 자유입니다.
데비안 프로젝트와 그 구성원들은 자유 소프트웨어의 강력한 지지자입니다.
우리는 <a
href="../social_contract#guidelines">데비안 자유 소프트웨어 지침
(DFSG)</a>을 편집하여
우리가 생각하는 자유 소프트웨어에 대한 합리적인 정의를 제시했습니다.
DFSG를 준수하는 소프트웨어만 주요 데비안 배포판에서 허용합니다.
</p>
