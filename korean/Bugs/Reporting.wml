#use wml::debian::template title="Debian BTS - reporting bugs" NOHEADER=yes NOCOPYRIGHT=true
#use wml::debian::translation-check translation="d2f23dedea7ef17add18a4b0acb68520c6677264" maintainer="Sebul" mindelta="-1"

<h1>reportbug를 써서 데비안 버그를 보고하는 방법</h1>
<a name="reportbug"></a>
<p>데비안에서 우리는
<code><a
href="https://packages.debian.org/stable/utils/reportbug">reportbug</a></code>
프로그램을 써서 버그를 보고하는 것을 강력히 권합니다.</p>

<p>

reportbug는 기본으로 대부분의 시스템에 설치됩니다.
그것이 가능하지 않아면, 패키지 관리 도구를 써서 설치할 수 있습니다.
</p>

<p>
reportbug는 메뉴의 시스템 섹션 또는 명령행에서 <code>reportbug</code>를 실행하여 시작할 수 있습니다.
</p>

<p>버그 리포팅 절차를 단계적으로 알려줄 겁니다.</p>

<p>
reportbug의 대화형 프롬프트에서 해결할 수 없는 질문이 있으면, 
아래 문서의 나머지 부분을 참조하거나  <a
href="mailto:debian-user@lists.debian.org">데비안 사용자 메일링 리스트</a>에 문의할 수 있습니다.
</p>


<h1>이메일을 사용하여 Debian에서 버그를 보고하는 방법(및 고급 reportbug 사용)</h1>

<h2>버그 보고 <strong>전</strong>에 주의할 중요한 것</h2>

<a name="whatpackage"></a>
<h3>버그 리포트가 무슨 패키지에 있나요?</h3>
<p>
버그 보고서가 어떤 패키지에 대해 제출되어야 하는지 알아야 합니다. 
이 정보를 찾는 방법에 대한 자세한 내용은 <a href="#findpkgver">이 예</a>를 보세오. 
(이 정보를 사용하여 <a href="#filedalready">버그 보고서가 이미 제출되었는지</a> 확인합니다.)
</p>

<p>버그리포트를 어느 패키지에 할 지 모르면,
이메일을 <a href="mailto:debian-user@lists.debian.org">데비안 사용자 메일링 리스트</a>에 보내서 조언을 요청하세요.</p>

<p>만약 당신의 문제가 단지 하나의 패키지가 아니라 일부 일반적인 데비안 서비스와 관련이 있다면, 대신 우리에게 메시지를 전달하는 데 사용할 수 있는 몇 개의 <a href="pseudo-packages">pseudo-packages</a>유사 패키지</a> 또는 심지어 <a href="../MailingLists/">mailing lists</a>메일링 리스트</a>가 있습니다.</p>

<a name="filedalready"></a>
<h3>버그 리포트가 이미 제출되었나요?</h3>
<p>You should check to see if your bug report has already been filed 
before submitting it. You can see which bugs have been filed in
a specific package using the
<a href="./#pkgreport">package option of the bug search form</a>.
If there is an existing bug report #<var>&lt;number&gt;</var>,
you should submit your comments by sending e-mail to
<var>&lt;number&gt;</var>@bugs.debian.org instead of reporting a
new bug.</p>

<h3>여러 버그에 대해 여러 보고서 보내기</h3>
<p>Please don't report multiple unrelated bugs &mdash; especially ones in
different packages &mdash; in a single bug report.</p>

<h3>버그를 업스트림에 보내지 마세요</h3>
<p>
만약 당신이 데비안에서 버그를 제출한다면, 
그 버그가 데비안에만 존재할 가능성이 있기 때문에, 
당신 자신이 업스트림 소프트웨어 관리자에게 복사본을 보내지 마세요. 
필요한 경우 패키지의 관리자가 버그를 업스트림으로 전달합니다</p>

<h2>e-mail 통해 버그 리포트 보내기</h2>

<p>You can report bugs in Debian by sending an e-mail to
<a href="mailto:submit@bugs.debian.org"><code>submit@bugs.debian.org</code></a>
with a special format described below. <code>reportbug</code> (<a
href="#reportbug">see above</a>) will properly format the e-mails for you;
please use it!</p>

<h3>Headers</h3>
<p>Like any e-mail you should include a clear, descriptive
<code>Subject</code> line in your main mail header.  The subject you
give will be used as the initial bug title in the tracking system, so
please try to make it informative!</p>

<p>If you'd like to send a copy of your bug report to additional recipients
(such as mailing lists), you shouldn't use the usual e-mail headers, but
<a href="#xcc">a different method, described below</a>.</p>

<h3><a name="pseudoheader">Pseudo-headers</a></h3>
<p>The first part of the bug report are the pseudo-headers which contain
information about what package and version your bug report applies to.
The first line of the message body has to include a pseudo-header.
It should say:</p>

<pre>
Package: &lt;packagename&gt;
</pre>

<p>Replace <code>&lt;packagename&gt;</code> with the <a href="#whatpackage">name of the package</a> which
has the bug.</p>

<p>The second line of the message should say:</p>

<pre>
Version: &lt;packageversion&gt;
</pre>

<p>Replace <code>&lt;packageversion&gt;</code> with the version of the package.
Please don't include any text here other than the version itself, as the
bug tracking system relies on this field to work out which releases are
affected by the bug.</p>

<p>You need to supply a correct <code>Package</code> line in the
pseudo-header in order for the bug tracking system to deliver the message
to the package's maintainer. See <a href="#findpkgver">this example</a> for
information on how to find this information.</p>

<p>For other valid pseudo-headers, see <a
href="#additionalpseudoheaders">Additional pseudo-headers</a></p>

<h3>The body of the report</h3>
<p>Please include in your report:</p>

<ul>
<li>The <em>exact</em> and <em>complete</em> text of any error
messages printed or logged.  This is very important!</li>
<li>Exactly what you typed or did to demonstrate the problem.</li>
<li>A description of the incorrect behavior: exactly what behavior
you were expecting, and what you observed.  A transcript of an
example session is a good way of showing this.</li>
<li>A suggested fix, or even a patch, if you have one.</li>
<li>Details of the configuration of the program with the problem.
Include the complete text of its configuration files.</li>
<li>The versions of any packages on which the buggy package depends.</li>
<li>What kernel version you're using (type <code>uname -a</code>), your
shared C library (type <code>ls -l /lib/*/libc.so.6</code> or
<code>apt show libc6 | grep ^Version</code>), and any other details about
your Debian system, if it seems appropriate.  For example, if you had a
problem with a Perl script, you would want to provide the version of the
`perl' binary (type <code>perl -v</code> or <code>dpkg -s perl | grep
^Version:</code>).</li>
<li>Appropriate details of the hardware in your system.  If you're
reporting a problem with a device driver please list <em>all</em> the
hardware in your system, as problems are often caused by IRQ and I/O
address conflicts.</li>
<li>If you have <a
href="https://packages.debian.org/stable/utils/reportbug">reportbug</a>
 installed the output of
 <code>reportbug --template -T none -s none -S normal -b --list-cc
 none -q &lt;package&gt;</code>
will also be useful, as it contains the output of maintainer specific
scripts and version information.</li>
</ul>

<p>Include any detail that seems relevant &mdash; you are in very little danger
of making your report too long by including too much information.  If
they are small, please include in your report any files you were using
to reproduce the problem. (If they are large, consider making them
available on a publicly available website if possible.)</p>

<p>For more advice on how to help the developers solve your problem,
please read <a href="https://www.chiark.greenend.org.uk/~sgtatham/bugs.html">
How to Report Bugs Effectively</a>.</p>


<h2><a name="example">버그 리포트 예시</a></h2>

<p>A bug report with header and pseudo-header looks something like this:</p>

<pre>
  To: submit@bugs.debian.org
  From: diligent@testing.linux.org
  Subject: Hello says `goodbye'

  Package: hello
  Version: 1.3-16

  When I invoke `hello' without arguments from an ordinary shell
  prompt it prints `goodbye', rather than the expected `hello, world'.
  Here is a transcript:

  $ hello
  goodbye
  $ /usr/bin/hello
  goodbye
  $

  I suggest that the output string, in hello.c, be corrected.

  I am using Debian GNU/Linux 2.2, kernel 2.2.17-pre-patch-13
  and libc6 2.1.3-10.
</pre>


<h2><a name="xcc">버그 리포트 사본을 다른 주소에 보내기</a></h2>

<p>Sometimes it is necessary to send a copy of a bug report to somewhere
else besides <code>debian-bugs-dist</code> and the package maintainer,
which is where they are normally sent.</p>

<p>You could do this by CC'ing your bug report to the other address(es),
but then the other copies would not have the bug report number put in
the <code>Reply-To</code> field and the <code>Subject</code> line.
When the recipients reply they will probably preserve the
<code>submit@bugs.debian.org</code> entry in the header and have their
message filed as a new bug report.  This leads to many duplicated
reports.</p>

<p>The <em>right</em> way to do this is to use the
<code>X-Debbugs-CC</code> header.  Add a line like this to your
message's mail header:</p>
<pre>
 X-Debbugs-CC: other-list@cosmic.edu
</pre>
<p>This will cause the bug tracking system to send a copy of your report
to the address(es) in the <code>X-Debbugs-CC</code> line as well as to
<code>debian-bugs-dist</code>.</p>

<p>If you want to send copies to more than one address, add them
comma-separated in only one <code>X-Debbugs-CC</code> line.</p>

<p>Avoid sending such copies to the addresses of other bug reports, as
they will be caught by the checks that prevent mail loops. There is
relatively little point in using <code>X-Debbugs-CC</code> for this
anyway, as the bug number added by that mechanism will just be replaced
by a new one; use an ordinary <code>CC</code> header instead.</p>

<p>This feature can often be combined usefully with mailing
<code>quiet</code> &mdash; see below.</p>

<a name="additionalpseudoheaders"></a>
<h1>Additional Pseudoheaders</h1>

<h2><a name="severities">Severity levels</a></h2>

<p>If a report is of a particularly serious bug, or is merely a feature
request, you can set the severity level of the bug as you report
it.  This is not required however, and the package maintainer will assign an
appropriate severity level to your report even if you do not (or pick
the wrong severity).</p>

<p>To assign a severity level, put a line like this one in the
<a href="#pseudoheader">pseudo-header</a>:</p>

<pre>
Severity: &lt;<var>severity</var>&gt;
</pre>

<p>Replace &lt;<var>severity</var>&gt; with one of the available severity
levels, as described in the
<a href="Developer#severities">advanced documentation</a>.</p>

<h2><a name="tags">Assigning tags</a></h2>

<p>You can set tags on a bug as you are reporting it. For example, if
you are including a patch with your bug report, you may wish to set the
<code>patch</code> tag. This is not required, however, and the developers
will set tags on your report as and when it is appropriate.</p>

<p>To set tags, put a line like this one in the
<a href="#pseudoheader">pseudo-header</a>:</p>

<pre>
Tags: &lt;<var>tags</var>&gt;
</pre>

<p>Replace &lt;<var>tags</var>&gt; with one or more of the available tags,
as described in the
<a href="Developer#tags">advanced documentation</a>.
Separate multiple tags with commas, spaces, or both.</p>

<pre>
User: &lt;<var>username</var>&gt;
Usertags: &lt;<var>usertags</var>&gt;
</pre>

<p>Replace &lt;<var>usertags</var>&gt; with one or more usertags.
Separate multiple tags with commas, spaces, or both. If you specify a
&lt;<var>username</var>&gt;, that user's tags will be set. Otherwise,
the e-mail address of the sender will be used as the username.</p>

<p>You can set usertags for multiple users at bug submission time by
including multiple User pseudo-headers; each Usertags pseudo-header
sets the usertags for the preceding User pseudo-header. This is especially
useful for setting usertags for a team with multiple users, setting
usertags for multiple teams, or setting the
<a href="https://wiki.debian.org/Teams/Debbugs/ArchitectureTags">architecture usertags</a>
for bugs affecting multiple architectures.
</p>

<pre>
User: &lt;<var>first-username</var>&gt;
Usertags: &lt;<var>first-username usertags</var>&gt;
User: &lt;<var>second-username</var>&gt;
Usertags: &lt;<var>second-username usertags</var>&gt;
</pre>

<h2>Setting Forwarded</h2>
<pre>
Forwarded: <var>foo@example.com</var>
</pre>

<p>
will mark the newly submitted bug as forwarded to foo@example.com. See
<a href="Developer#forward">Recording that you have passed on a bug
report</a> in the developers' documentation for details.
</p>

<h2>Claiming ownership</h2>
<pre>
Owner: <var>foo@example.com</var>
</pre>

<p>
will indicate that foo@example.com is now responsible for fixing this
bug. See <a href="Developer#owner">Changing bug ownership</a> in the
developers' documentation for details.
</p>

<h2>Source Package</h2>
<pre>
Source: <var>foopackage</var>
</pre>

<p>
the equivalent of <code>Package:</code> for bugs present in the source
package of foopackage; for most bugs in most packages you don't want
to use this option.
</p>

<h2><a name="control">Control Commands</a></h2>
<pre>
Control: <var>control commands</var>
</pre>

<p>
Allows for any of the commands which must be sent to
<code>control@bugs.debian.org</code> to work when sent to <code>submit@bugs.debian.org</code> or
<code>nnn@bugs.debian.org</code>. -1 initially refers to the current
    bug (that is, the bug created by a mail to submit@ or the bug
    messaged with nnn@). Please see <a href="server-control">the
    server control documentation</a> for more information on the
    control commands which are valid.</p>

<p>For example, the following pseudoheader in a message  sent
  to <code>12345@bugs.debian.org</code>:</p>

<pre>
Control: retitle -1 this is the title
Control: severity -1 normal
Control: summary -1 0
Control: forwarded -1 https://bugs.debian.org/nnn
</pre>

<p>would cause 12345 to be retitled, its severity changed, summary set,
and marked as forwarded.</p>



<h2>X-Debbugs- headers</h2>
<p>Finally, if your
<acronym title="Mail User Agent" lang="en">MUA</acronym>
doesn't allow you to edit the headers, you can
set the various <code>X-Debbugs-</code> headers in the
<a href="#pseudoheader">pseudo-headers</a>.</p>


<h1>추가 정보</h1>

<h2>Different submission addresses (minor or mass bug reports)</h2>

<p>If a bug report is minor, for example, a documentation typo or a trivial
build problem, please adjust the severity appropriately and send it to
<code>maintonly@bugs.debian.org</code> instead of <code>submit@bugs.debian.org</code>.
<code>maintonly</code> will forward the report to the package maintainer
only, it won't forward it to the BTS mailing lists.</p>

<p>If you're submitting many reports at once, you should definitely use
<code>maintonly@bugs.debian.org</code> so that you don't cause too much redundant
traffic on the BTS mailing lists. Before submitting many similar bugs you
may also want to post a summary on <code>debian-bugs-dist</code>.</p>

<p>If wish to report a bug to the bug tracking system that's already been
sent to the maintainer, you can use <code>quiet@bugs.debian.org</code>. Bugs sent to
<code>quiet@bugs.debian.org</code> will not be forwarded anywhere, only filed.</p>

<p>When you use different submission addresses, the bug tracking system will
set the <code>Reply-To</code> of any forwarded message so that the replies
will by default be processed in the same way as the original report. That
means that, for example, replies to <code>maintonly</code> will go to
<var>nnn</var><code>-maintonly@bugs.debian.org</code> instead of
<var>nnn</var><code>@bugs.debian.org</code>, unless of course one overrides this
manually.</p>


<h2>Acknowledgements</h2>

<p>Normally, the bug tracking system will return an acknowledgement to you
by e-mail when you report a new bug or submit additional information to an
existing bug. If you want to suppress this acknowledgement, include an
<code>X-Debbugs-No-Ack</code> header or pseudoheader in your e-mail
(the contents of this header do not matter). If you report a new bug
with this header, you will need to check the web interface yourself to
find the bug number.</p>

<p>Note that this header will not suppress acknowledgements from the
<code>control@bugs.debian.org</code> mailserver, since those acknowledgements may
contain error messages which should be read and acted upon.</p>

<h2>Spamfighting and missing mail</h2>

<p>The bug tracking system implements a rather extensive set of rules
  designed to make sure that spam does not make it through the BTS.
  While we try to minimize the number of false positives, they do
  occur. If you suspect your mail has triggered a false positive, feel
  free to contact <code>owner@bugs.debian.org</code> for assistance.
  Another common cause of mail not making it through to the BTS is
  utilizing addresses which match procmail's FROM_DAEMON, which
  includes mail from addresses like <code>mail@foobar.com</code>. If
  you suspect your mail matches FROM_DAEMON,
  see <a href="https://manpages.debian.org/cgi-bin/man.cgi?query=procmailrc">procmailrc(5)</a>
  to verify, and then resend the mail using an address which does not
  match FROM_DAEMON.</p>


<h2>알려지지 않은 패키지에 버그 리포트</h2>

<p>If the bug tracking system doesn't know who the maintainer of the
relevant package is it will forward the report to
<code>debian-bugs-dist</code> even if <code>maintonly</code> was used.</p>

<p>When sending to <code>maintonly@bugs.debian.org</code> or
<var>nnn</var><code>-maintonly@bugs.debian.org</code> you should make sure that
the bug report is assigned to the right package, by putting a correct
<code>Package</code> at the top of an original submission of a report,
or by using <A href="server-control">the
<code>control@bugs.debian.org</code> service</A> to (re)assign the report
appropriately.</p>


<h2><a name="findpkgver">Using <code>dpkg</code> to find the package and
version for the report</a></h2>

<p>When using <code>reportbug</code> to report a bug in a command, say
<code>grep</code>, the following will automatically select the right package
and let you write the report right away: <code>reportbug --file $(which
grep)</code></p>

<p>You can also find out which package installed it by using <code>dpkg
--search</code>. You can find out which version of a package you have
installed by using <code>dpkg --list</code> or <code>dpkg --status</code>.
</p>

<p>For example:</p>
<pre>
$ which apt-get
/usr/bin/apt-get
$ type apt-get
apt-get is /usr/bin/apt-get
$ dpkg --search /usr/bin/apt-get
apt: /usr/bin/apt-get
$ dpkg --list apt
Desired=Unknown/Install/Remove/Purge/Hold
| Status=Not/Installed/Config-files/Unpacked/Failed-config/Half-installed
|/ Err?=(none)/Hold/Reinst-required/X=both-problems (Status,Err: uppercase=bad)
||/ Name           Version        Description
+++-==============-==============-============================================
ii  apt            0.3.19         Advanced front-end for dpkg
$ dpkg --status apt
Package: apt
Status: install ok installed
Priority: standard
Section: base
Installed-Size: 1391
Maintainer: APT Development Team &lt;deity@lists.debian.org&gt;
Version: 0.3.19
Replaces: deity, libapt-pkg-doc (&lt;&lt; 0.3.7), libapt-pkg-dev (&lt;&lt; 0.3.7)
Provides: libapt-pkg2.7
Depends: libapt-pkg2.7, libc6 (&gt;= 2.1.2), libstdc++2.10
Suggests: dpkg-dev
Conflicts: deity
Description: Advanced front-end for dpkg
 This is Debian's next generation front-end for the dpkg package manager.
 It provides the apt-get utility and APT dselect method that provides a
 simpler, safer way to install and upgrade packages.
 .
 APT features complete installation ordering, multiple source capability
 and several other unique features, see the Users Guide in
 /usr/doc/apt/guide.text.gz

</pre>

<a name="otherusefulcommands"></a>
<h2>다른 쓸모 있는 명령 및 패키지</h2>

<p>
The <kbd>querybts</kbd> tool, available from the same package as
<a href="https://packages.debian.org/stable/utils/reportbug">reportbug</a>,
provides a convenient text-based interface to the bug tracking system.</p>

<p>Emacs users can also use the debian-bug command provided by the
<code><a href="https://packages.debian.org/stable/utils/debian-el">\
debian-el</a></code> package. When called with <kbd>M-x
debian-bug</kbd>, it will ask for all necessary information in a
similar way to <code>reportbug</code>.</p>

<hr />

#use "otherpages.inc"

#use "$(ENGLISHDIR)/Bugs/footer.inc"
