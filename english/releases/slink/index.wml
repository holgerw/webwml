#use wml::debian::template title="Debian 2.1 (slink) Information" BARETITLE=yes
#use wml::debian::release

<strong>Debian 2.1 has been superseded.</strong>

<p>

Since <a href="../">newer releases</a> have been made, the 2.1 release
has been superseded.  These pages are being retained for historical
purposes.  You should be aware that Debian 2.1 is no longer maintained.

<p>

  The following architectures were supported in Debian 2.1:

<ul>
<li> alpha
<li> i386
<li> mk68k
<li> sparc
</ul>


<h2><a name="release-notes"></a>Release Notes</h2>

<p>

To find out what's new for Debian 2.1, see the Release Notes for your
architecture.  The Release Notes also contain instructions for users
who are upgrading from prior releases.

<ul>
<li> <a href="alpha/release-notes.txt">Release Notes for Alpha<a>
<li> <a href="i386/release-notes.txt">Release Notes for 32-bit PC (i386)<a>
<li> <a href="m68k/release-notes.txt">Release Notes for Motorola 680x0<a>
<li> <a href="sparc/release-notes.txt">Release Notes for SPARC<a>
</ul>

<h2><a name="errata"></a>Errata</h2>

<p>

Sometimes, in the case of critical problems or security updates, the
released distribution (in this case, Slink) is updated.  Generally,
these are indicated as point releases.  The current point release is
Debian 2.1r5.  You can find the <a
href="http://archive.debian.org/debian/dists/slink/ChangeLog">ChangeLog</a>
on any Debian archive mirror.

<p>

Slink is certified for use with the 2.0.x series of Linux kernels.  If
you wish to run the Linux 2.2.x kernel with slink, see the <a
href="running-kernel-2.2">list of known problems</a>.


<h3>APT</h3>

<p>

An updated version of <code>apt</code> is available in Debian, as of
2.1r3.  The benefits of this updated version is primarily that it is
able to handle installation from multiple CD-ROMs itself.  This makes
the <code>dpkg-multicd</code> acquisition option in
<code>dselect</code> unnecessary.  However, your 2.1 CD-ROM may
contain an older <code>apt</code>, so you might want to upgrade based
on the one now in Slink.



<h2><a name="acquiring"></a>Getting Debian 2.1</h2>

<p>

Debian is available electronically or from CD vendors.

<h3>Buying Debian on CD</h3>

<p>

We maintain a <a href="../../CD/vendors/">list of CD vendors</a>
that sell CDs of Debian 2.1.


<h3>Downloading Debian Over the Internet</h3>

<p>

We maintain a <a href="../../distrib/ftplist">list of sites</a> which
mirror the distribution.


<!-- Keep this comment at the end of the file
Local variables:
mode: sgml
sgml-indent-data:nil
sgml-doctype:"../.doctype"
End:
-->
