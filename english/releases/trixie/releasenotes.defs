#use wml::debian::common_tags
#use wml::debian::languages
#use wml::debian::toc

# script mostly copied from english/doc/ - holgerw

<define-tag in-rn whitespace=delete>
  <set-var %attributes />
  <if <get-var name /> "" <exit message="you must pass a name to the in-rn tag" /> />
  #   Default values
  <defvar index index />
  <if <get-var langs />
      <set-var add-lang-ext=true />
      <group <set-var langs=en /><set-var add-lang-ext= /> />
  />
  <defvar formats html />
  <defvar path "<get-var name />" />
  <defvar basename "<get-var name />" />
  <defvar vcsname <get-var name /> />
# naming: default(extention: pt-br, ...), locale(extention: pt_BR, ...), none(no listing)
  <defvar naming default />
  <defvar srctype XML />
  <defvar vcstype ddp />

  <subst-in-var langs " " "\n" />
  <subst-in-var formats " " "\n" />

 <when <not <match <get-var naming /> "none" />/>>
  <br/>
  <ul>
  <foreach lang langs>
    <li>
    <:
    use Locale::Language;
    my $xy = '<get-var lang />';
    $xy =~ s/[-_]\w+$//; # for pt-br, pt_BR and such
    my $printlang = $trans{$CUR_ISO_LANG}{lc(code2language($xy))};
    $printlang = $trans{$CUR_ISO_LANG}{'norwegian'} if ($xy eq 'nb'); # nb is refered as Norwegian on the website
    $printlang = $trans{$CUR_ISO_LANG}{'greek'} if ($xy eq 'el'); # el is displayed as “Greek, Modern (1453-)”
    $printlang = "<strong>$printlang</strong>" if ($xy eq $CUR_ISO_LANG);
    print $printlang;
    :>
    <set-var version=<get-var version-<get-var name />-<get-var lang /> /> />
    <when <get-var version />>
      &nbsp;<gettext domain="doc">(version <get-var version />)</gettext>
    </when>
    :
    <foreach format formats>
      <set-var extlang=<get-var lang /> />
      <when <match <get-var naming /> "locale" />>
      	<when <match <get-var format /> "(dvi|epub|txt|pdf|ps)" />>
		<subst-in-var extlang "(\w\w)-(\w\w)" "\\1_<:=uc("\\2"):>" />
      	</when>
      </when>
      &nbsp;
      <if <match <get-var format /> "^html$" action=report />
        <set-var baselink=<get-var index /> />
        <set-var baselink=<get-var basename /> />
      />
      <if <get-var add-lang-ext />
        <if <match <get-var format /> "^singlepagehtml$" action=report />
              <set-var loc="<get-var baselink />.<get-var extlang />.html" />
        <set-var loc="<get-var baselink />.<get-var extlang />.<get-var format />" />
        />
        <if <match <get-var format /> "^singlepagehtml$" action=report />
              <set-var loc="<get-var baselink />.<get-var format />.html" />
        <set-var loc="<get-var baselink />.<get-var format />" />
        />
      />
      <when <match <get-var add-lang-ext /> "^$" action=report />>
        <when <match <get-var index /> "^index$" action=report />>
          <when <match <get-var format /> "^html$" action=report />>
            <set-var loc="" />
          </when>
        </when>
      </when>
      <when <match <get-var format /> "(pdf|ps|dvi|epub|html)" action=report />>
        <set-var txt=<upcase <get-var format /> /> />
      </when>
      <when <match <get-var format /> "txt" action=report />>
        <set-var txt=<gettext domain="doc">plain text</gettext> />
      </when>
      <when <match <get-var format /> "singlepagehtml$" action=report />>
        <set-var txt=<gettext domain="doc">HTML (single page)</gettext> />
      </when>
      <when <match <get-var format /> "(txt|pdf|ps|dvi|epub)" action=report />>
      [<a href="<get-var loc />"><get-var txt /></a>]
      </when>
      <when <match <get-var format /> "html" action=report />>
      [<a href="<get-var path />/<get-var loc />"><get-var txt /></a>]
      </when>
    </foreach>
    </li>
  </foreach>

  </ul>
 </when>

</define-tag>

# -----------------------------------------------------------------------------------------

# cheat sheet for tags

# "naming": If unset, set "naming" to "default" and used for listing automatically
# naming="none" used not listing at all
# naming="locale" used for listing automatically (see below)
# naming="locale" (used in debian-faq and refcard) provides correct
# pt_BR naming from pt-br declared langs, for every built files, as
# discussed in <20100406221005.GB15316@dedibox.ebzao.info>, but
# keeps pt-br fot html files to comply with Apache rewriting.

# formats: see manuals.defs for available ones
# langs: If unset, file extention becomes .html instead of .en.html
# vcsname: set "vcsname" when the DDP VCS repo name is not same as "name"

# vcstype="ddp": DDP git site (no need for vcsweb and vcsrepo)
# vcstype="git": Generic git site

# vcsweb: URL for vcsweb site etc.
# vcsrepo: repository checkout URL (anonymous R-only one)

<define-tag definition-releasenotes>
  <in-rn name="release-notes"
            naming="locale"
            langs="de el en es fr gl it ja nl pt pt_BR ro sv zh_CN"
# disabled languagess: be ca da fi lt ml nb pl ru sk vi zh_TW
            formats="html txt pdf"
            />
</define-tag>
