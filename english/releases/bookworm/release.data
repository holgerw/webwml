<perl>

# list of architectures, ordered by Popularity Contest on 2014-04-26.
@arches = (
#	alpha,
	amd64,
#	arm,
	arm64,
	armel,
	armhf,
#	hppa,
#	'hurd-i386',
	i386,
#	ia64,
#	'kfreebsd-amd64',
#	'kfreebsd-i386',
#	m68k,
#	mips,
	mipsel,
	mips64el,
#	powerpc,
#	ppc64,
	ppc64el,
#	s390,
	s390x,
#	sh4,
#	sparc,
);

# list of languages install manual is translated to
%langsinstall = (
	english => "en",
	catalan => "ca",
	czech => "cs",
	danish => "da",
	german => "de",
	greek => "el",
	spanish => "es",
#	basque => "eu,
#	finnish => "fi",
	french => "fr",
#	hungarian => "hu",
	indonesian => "id",
	italian => "it",
	japanese => "ja",
	korean => "ko",
	dutch => "nl",
#	norwegian_nynorsk => "nn",
	portuguese => "pt",
#	portuguese_br => "pt-br",
	romanian => "ro",
	russian => "ru",
	swedish => "sv",
#	tagalog => "tl",
	vietnamese => "vi",
	chinese_cn => "zh-cn",
#	chinese => "zh-tw",
);

# list of languages release notes are translated to
%langsrelnotes = (
	english => "en",
	belarusian => "be",
	catalan => "ca",
	czech => "cs",
	danish => "da",
	german => "de",
	greek => "el",
	spanish => "es",
	finnish => "fi",
	french => "fr",
	galician=> "gl",
	italian => "it",
	japanese => "ja",
	lithuanian => "lt",
	malayalam => "ml",
	norwegian_bokmal => "nb",
	dutch => "nl",
	polish => "pl",
	portuguese => "pt",
	portuguese_br => "pt-br",
	romanian => "ro",
	russian => "ru",
	slovak => "sk",
	swedish => "sv",
	vietnamese => "vi",
	chinese_cn => "zh-cn",
	chinese => "zh-tw",
);

</perl>


### While bookworm is stable, we can *reuse* the tags defined in
#### templates/debian/release_images.wml.
#### When the website gets prepared for the next stable release,
#### the tags should be defined here instead (see previous releases
#### for examples).
#### Note that images for the new oldstable release will be moved to
#### cdimage/archive at the time of the release!
#
### Next line should be changed to 'wml::debian::installer' when
### preparing for next stable release; don't forget the Makefile!
#use wml::debian::release_images

<define-tag netinst-images>
<stable-netinst-images />
</define-tag>

<define-tag full-cd-images>
<stable-full-cd-images />
</define-tag>

<define-tag full-cd-torrent>
<stable-full-cd-torrent />
</define-tag>

<define-tag full-cd-jigdo>
<stable-full-cd-jigdo />
</define-tag>

<define-tag full-dvd-images>
<stable-full-dvd-images />
</define-tag>

<define-tag full-dvd-torrent>
<stable-full-dvd-torrent />
</define-tag>

<define-tag full-dvd-jigdo>
<stable-full-dvd-jigdo />
</define-tag>

<define-tag full-bluray-jigdo>
<stable-full-bluray-jigdo />
</define-tag>

<define-tag other-images>
<stable-other-images />
</define-tag>

<define-tag small-non-free-cd-images>
<stable-small-non-free-cd-images />
</define-tag>

#--------------------------------------------------------------------------

# Is <releasename>-2 the current release ? (yes/no)
<set-var pre-pre-release-state="no" />

# Is <releasename>-1 the current release ? (yes/no)
<set-var pre-release-state="no" />

# Is <releasename> the current release ? (yes/no)
<set-var release-state="yes" />

# Is <releasename>+1 the current release ? (yes/no)
<set-var post-release-state="no" />

# Security updates discontinued for current release ? (yes/no)
<set-var no-security-state="no" />

# Has LTS period begun for current release ? (yes/no)
<set-var release-lts-state="no" />

#--------------------------------------------------------------------------

# Support for <if-pre-pre-release state="yes|no"> tags:
<set-var diversions:_pre_pre_release=0 />
<define-tag if-pre-pre-release endtag=required>
<preserve state />
<set-var %attributes />
<ifneq "<get-var state />" "<get-var pre-pre-release-state />"
       "<enter NOpre_pre_release<get-var diversions:_pre_pre_release /> />"
       "<enter pre_pre_release<get-var diversions:_pre_pre_release /> />" />
%body
<leave />
<dump pre_pre_release<get-var diversions:_pre_pre_release /> />
<increment diversions:_pre_pre_release />
<restore state />
</define-tag>

#--------------------------------------------------------------------------

# Support for <if-pre-release state="yes|no"> tags:
<set-var diversions:_pre_release=0 />
<define-tag if-pre-release endtag=required>
<preserve state />
<set-var %attributes />
<ifneq "<get-var state />" "<get-var pre-release-state />"
       "<enter NOpre_release<get-var diversions:_pre_release /> />"
       "<enter pre_release<get-var diversions:_pre_release /> />" />
%body
<leave />
<dump pre_release<get-var diversions:_pre_release /> />
<increment diversions:_pre_release />
<restore state />
</define-tag>

#--------------------------------------------------------------------------

# Support for <if-release state="yes|no"> tags:
<set-var diversions:_release=0 />
<define-tag if-release endtag=required>
<preserve state />
<set-var %attributes />
<ifneq "<get-var state />" "<get-var release-state />"
       "<enter NOrelease<get-var diversions:_release /> />"
       "<enter release<get-var diversions:_release /> />" />
%body
<leave />
<dump release<get-var diversions:_release /> />
<increment diversions:_release />
<restore state />
</define-tag>

#--------------------------------------------------------------------------

# Support for <if-post-release state="yes|no"> tags:
<set-var diversions:_post_release=0 />
<define-tag if-post-release endtag=required>
<preserve state />
<set-var %attributes />
<ifneq "<get-var state />" "<get-var post-release-state />"
       "<enter NOpost_release<get-var diversions:_post_release /> />"
       "<enter post_release<get-var diversions:_post_release /> />" />
%body
<leave />
<dump post_release<get-var diversions:_post_release /> />
<increment diversions:_post_release />
<restore state />
</define-tag>

#--------------------------------------------------------------------------

# Support for <if-no-security state="yes|no"> tags:
<set-var diversions:_no_security=0 />
<define-tag if-no-security endtag=required>
<preserve state />
<set-var %attributes />
<ifneq "<get-var state />" "<get-var no-security-state />"
       "<enter NOno_security<get-var diversions:_no_security /> />"
       "<enter no_security<get-var diversions:_no_security /> />" />
%body
<leave />
<dump no_security<get-var diversions:_no_security /> />
<increment diversions:_no_security />
<restore state />
</define-tag>

#--------------------------------------------------------------------------

# Support for <if-release-lts state="yes|no"> tags:
<set-var diversions:_release_lts=0 />
<define-tag if-release-lts endtag=required>
<preserve state />
<set-var %attributes />
<ifneq "<get-var state />" "<get-var release-lts-state />"
       "<enter NOrelease_lts<get-var diversions:_release_lts /> />"
       "<enter release_lts<get-var diversions:_release_lts /> />" />
%body
<leave />
<dump release_lts<get-var diversions:_release_lts /> />
<increment diversions:_release_lts />
<restore state />
</define-tag>
