<define-tag pagetitle>Updated Debian 11: 11.5 released</define-tag>
<define-tag release_date>2022-09-10</define-tag>
#use wml::debian::news
# $Id:

<define-tag release>11</define-tag>
<define-tag codename>bullseye</define-tag>
<define-tag revision>11.5</define-tag>

<define-tag dsa>
    <tr><td align="center"><a href="$(HOME)/security/%0/dsa-%1">DSA-%1</a></td>
        <td align="center"><:
    my @p = ();
    for my $p (split (/,\s*/, "%2")) {
	push (@p, sprintf ('<a href="https://packages.debian.org/src:%s">%s</a>', $p, $p));
    }
    print join (", ", @p);
:></td></tr>
</define-tag>

<define-tag correction>
    <tr><td><a href="https://packages.debian.org/src:%0">%0</a></td>              <td>%1</td></tr>
</define-tag>

<define-tag srcpkg><a href="https://packages.debian.org/src:%0">%0</a></define-tag>

<p>The Debian project is pleased to announce the fifth update of its
stable distribution Debian <release> (codename <q><codename></q>). 
This point release mainly adds corrections for security issues,
along with a few adjustments for serious problems.  Security advisories
have already been published separately and are referenced where available.</p>

<p>Please note that the point release does not constitute a new version of Debian
<release> but only updates some of the packages included.  There is
no need to throw away old <q><codename></q> media. After installation,
packages can be upgraded to the current versions using an up-to-date Debian
mirror.</p>

<p>Those who frequently install updates from security.debian.org won't have
to update many packages, and most such updates are
included in the point release.</p>

<p>New installation images will be available soon at the regular locations.</p>

<p>Upgrading an existing installation to this revision can be achieved by
pointing the package management system at one of Debian's many HTTP mirrors.
A comprehensive list of mirrors is available at:</p>

<div class="center">
  <a href="$(HOME)/mirror/list">https://www.debian.org/mirror/list</a>
</div>




<h2>Miscellaneous Bugfixes</h2>

<p>This stable update adds a few important corrections to the following packages:</p>

<table border=0>
<tr><th>Package</th>               <th>Reason</th></tr>
<correction avahi "Fix display of URLs containing '&amp;' in avahi-discover; do not disable timeout cleanup on watch cleanup; fix NULL pointer crashes when trying to resolve badly-formatted hostnames [CVE-2021-3502]">
<correction base-files "Update /etc/debian_version for the 11.5 point release">
<correction cargo-mozilla "New source package to support building of newer firefox-esr and thunderbird versions">
<correction clamav "New upstream stable release">
<correction commons-daemon "Fix JVM detection">
<correction curl "Reject cookies with <q>control bytes</q> [CVE-2022-35252]">
<correction dbus-broker "Fix assertion failure when disconnecting peer groups; fix memory leak; fix null pointer dereference [CVE-2022-31213]">
<correction debian-installer "Rebuild against proposed-updates; increase Linux kernel ABI to 5.10.0-18">
<correction debian-installer-netboot-images "Rebuild against proposed-updates; increase Linux kernel ABI to 5.10.0-18">
<correction debian-security-support "Update support status of various packages">
<correction debootstrap "Ensure non-merged-usr chroots can continue to be created for older releases and buildd chroots">
<correction dlt-daemon "Fix double free issue [CVE-2022-31291]">
<correction dnsproxy "Listen on localhost by default, rather than the possibly unavailable 192.168.168.1">
<correction dovecot "Fix possible security issues when two passdb configuration entries exist with the same driver and args settings [CVE-2022-30550]">
<correction dpkg "Fix conffile removal-on-upgrade handling, memory leak in remove-on-upgrade handling; Dpkg::Shlibs::Objdump: Fix apply_relocations to work with versioned symbols; add support for ARCv2 CPU; several updates and fixes to dpkg-fsys-usrunmess">
<correction fig2dev "Fix double free issue [CVE-2021-37529], denial of service issue [CVE-2021-37530]; stop misplacement of embedded eps images">
<correction foxtrotgps "Fix crash by ensuring that threads are always unreferenced">
<correction gif2apng "Fix heap-based buffer overflows [CVE-2021-45909 CVE-2021-45910 CVE-2021-45911]">
<correction glibc "Fix an off-by-one buffer overflow/underflow in getcwd() [CVE-2021-3999]; fix several overflows in wide character functions; add a few EVEX optimized string functions to fix a performance issue (up to 40%) with Skylake-X processors; make grantpt usable after multi-threaded fork; ensure that libio vtable protection is enabled">
<correction golang-github-pkg-term "Fix building on newer Linux kernels">
<correction gri "Use <q>ps2pdf</q> instead of <q>convert</q> for converting from PS to PDF">
<correction grub-efi-amd64-signed "New upstream release">
<correction grub-efi-arm64-signed "New upstream release">
<correction grub-efi-ia32-signed "New upstream release">
<correction grub2 "New upstream release">
<correction http-parser "Unset F_CHUNKED on new Transfer-Encoding, fixing possible HTTP request smuggling issue [CVE-2020-8287]">
<correction ifenslave "Fix bonded interface configurations">
<correction inetutils "Fix buffer overflow issue [CVE-2019-0053], stack exhaustion issue, handling of FTP PASV responses [CVE-2021-40491], denial of service issue [CVE-2022-39028]">
<correction knot "Fix IXFR to AXFR fallback with dnsmasq">
<correction krb5 "Use SHA256 as Pkinit CMS Digest">
<correction libayatana-appindicator "Provide compatibility for software that depends on libappindicator">
<correction libdatetime-timezone-perl "Update included data">
<correction libhttp-daemon-perl "Improve handling of Content-Length header [CVE-2022-31081]">
<correction libreoffice "Support EUR in .hr locale; add HRK&lt;-&gt;EUR conversion rate to Calc and the Euro Wizard; security fixes [CVE-2021-25636 CVE-2022-26305 CVE-2022-26306 CVE-2022-26307]; fix hang accessing Evolution address books">
<correction linux "New upstream stable release">
<correction linux-signed-amd64 "New upstream stable release">
<correction linux-signed-arm64 "New upstream stable release">
<correction linux-signed-i386 "New upstream stable release">
<correction llvm-toolchain-13 "New source package to support building of newer firefox-esr and thunderbird versions">
<correction lwip "Fix buffer overflow issues [CVE-2020-22283 CVE-2020-22284]">
<correction mokutil "New upstream version, to allow for SBAT management">
<correction node-log4js "Do not create world-readable files by default [CVE-2022-21704]">
<correction node-moment "Fix regular expression-based denial of service issue [CVE-2022-31129]">
<correction nvidia-graphics-drivers "New upstream release; security fixes [CVE-2022-31607 CVE-2022-31608 CVE-2022-31615]">
<correction nvidia-graphics-drivers-legacy-390xx "New upstream release; security fixes [CVE-2022-31607 CVE-2022-31608 CVE-2022-31615]">
<correction nvidia-graphics-drivers-tesla-450 "New upstream release; security fixes [CVE-2022-31607 CVE-2022-31608 CVE-2022-31615]">
<correction nvidia-graphics-drivers-tesla-470 "New upstream release; security fixes [CVE-2022-31607 CVE-2022-31608 CVE-2022-31615]">
<correction nvidia-settings "New upstream release; fix cross-building">
<correction nvidia-settings-tesla-470 "New upstream release; fix cross-building">
<correction pcre2 "Fix out-of-bounds read issues [CVE-2022-1586 CVE-2022-1587]">
<correction postgresql-13 "Do not let extension scripts replace objects not already belonging to the extension [CVE-2022-2625]">
<correction publicsuffix "Update included data">
<correction rocksdb "Fix illegal instruction on arm64">
<correction sbuild "Buildd::Mail: support MIME encoded Subject: header, also copy the Content-Type: header when forwarding mail">
<correction systemd "Drop bundled copy of linux/if_arp.h, fixing build failures with newer kernel headers; support detection for ARM64 Hyper-V guests; detect OpenStack instance as KVM on arm">
<correction twitter-bootstrap4 "Actually install CSS map files">
<correction tzdata "Update timezone data for Iran and Chile">
<correction xtables-addons "Support both old and new versions of security_skb_classify_flow()">
</table>


<h2>Security Updates</h2>


<p>This revision adds the following security updates to the stable release.
The Security Team has already released an advisory for each of these
updates:</p>

<table border=0>
<tr><th>Advisory ID</th>  <th>Package</th></tr>
<dsa 2022 5175 thunderbird>
<dsa 2022 5176 blender>
<dsa 2022 5177 ldap-account-manager>
<dsa 2022 5178 intel-microcode>
<dsa 2022 5179 php7.4>
<dsa 2022 5180 chromium>
<dsa 2022 5181 request-tracker4>
<dsa 2022 5182 webkit2gtk>
<dsa 2022 5183 wpewebkit>
<dsa 2022 5184 xen>
<dsa 2022 5185 mat2>
<dsa 2022 5187 chromium>
<dsa 2022 5188 openjdk-11>
<dsa 2022 5189 gsasl>
<dsa 2022 5190 spip>
<dsa 2022 5191 linux-signed-amd64>
<dsa 2022 5191 linux-signed-arm64>
<dsa 2022 5191 linux-signed-i386>
<dsa 2022 5191 linux>
<dsa 2022 5192 openjdk-17>
<dsa 2022 5193 firefox-esr>
<dsa 2022 5194 booth>
<dsa 2022 5195 thunderbird>
<dsa 2022 5196 libpgjava>
<dsa 2022 5197 curl>
<dsa 2022 5198 jetty9>
<dsa 2022 5199 xorg-server>
<dsa 2022 5200 libtirpc>
<dsa 2022 5201 chromium>
<dsa 2022 5202 unzip>
<dsa 2022 5203 gnutls28>
<dsa 2022 5204 gst-plugins-good1.0>
<dsa 2022 5205 ldb>
<dsa 2022 5205 samba>
<dsa 2022 5206 trafficserver>
<dsa 2022 5207 linux-signed-amd64>
<dsa 2022 5207 linux-signed-arm64>
<dsa 2022 5207 linux-signed-i386>
<dsa 2022 5207 linux>
<dsa 2022 5208 epiphany-browser>
<dsa 2022 5209 net-snmp>
<dsa 2022 5210 webkit2gtk>
<dsa 2022 5211 wpewebkit>
<dsa 2022 5213 schroot>
<dsa 2022 5214 kicad>
<dsa 2022 5215 open-vm-tools>
<dsa 2022 5216 libxslt>
<dsa 2022 5217 firefox-esr>
<dsa 2022 5218 zlib>
<dsa 2022 5219 webkit2gtk>
<dsa 2022 5220 wpewebkit>
<dsa 2022 5221 thunderbird>
<dsa 2022 5222 dpdk>
</table>


<h2>Removed packages</h2>

<p>The following packages were removed due to circumstances beyond our control:</p>

<table border=0>
<tr><th>Package</th>               <th>Reason</th></tr>
<correction evenement "Unmaintained; only needed for already-removed movim">
<correction php-cocur-slugify "Unmaintained; only needed for already-removed movim">
<correction php-defuse-php-encryption "Unmaintained; only needed for already-removed movim">
<correction php-dflydev-fig-cookies "Unmaintained; only needed for already-removed movim">
<correction php-embed "Unmaintained; only needed for already-removed movim">
<correction php-fabiang-sasl "Unmaintained; only needed for already-removed movim">
<correction php-markdown "Unmaintained; only needed for already-removed movim">
<correction php-raintpl "Unmaintained; only needed for already-removed movim">
<correction php-react-child-process "Unmaintained; only needed for already-removed movim">
<correction php-react-http "Unmaintained; only needed for already-removed movim">
<correction php-respect-validation "Unmaintained; only needed for already-removed movim">
<correction php-robmorgan-phinx "Unmaintained; only needed for already-removed movim">
<correction ratchet-pawl "Unmaintained; only needed for already-removed movim">
<correction ratchet-rfc6455 "Unmaintained; only needed for already-removed movim">
<correction ratchetphp "Unmaintained; only needed for already-removed movim">
<correction reactphp-cache "Unmaintained; only needed for already-removed movim">
<correction reactphp-dns "Unmaintained; only needed for already-removed movim">
<correction reactphp-event-loop "Unmaintained; only needed for already-removed movim">
<correction reactphp-promise-stream "Unmaintained; only needed for already-removed movim">
<correction reactphp-promise-timer "Unmaintained; only needed for already-removed movim">
<correction reactphp-socket "Unmaintained; only needed for already-removed movim">
<correction reactphp-stream "Unmaintained; only needed for already-removed movim">

</table>

<h2>Debian Installer</h2>
<p>The installer has been updated to include the fixes incorporated
into stable by the point release.</p>

<h2>URLs</h2>

<p>The complete lists of packages that have changed with this revision:</p>

<div class="center">
  <url "https://deb.debian.org/debian/dists/<downcase <codename>>/ChangeLog">
</div>

<p>The current stable distribution:</p>

<div class="center">
  <url "https://deb.debian.org/debian/dists/stable/">
</div>

<p>Proposed updates to the stable distribution:</p>

<div class="center">
  <url "https://deb.debian.org/debian/dists/proposed-updates">
</div>

<p>stable distribution information (release notes, errata etc.):</p>

<div class="center">
  <a
  href="$(HOME)/releases/stable/">https://www.debian.org/releases/stable/</a>
</div>

<p>Security announcements and information:</p>

<div class="center">
  <a href="$(HOME)/security/">https://www.debian.org/security/</a>
</div>

<h2>About Debian</h2>

<p>The Debian Project is an association of Free Software developers who
volunteer their time and effort in order to produce the completely
free operating system Debian.</p>

<h2>Contact Information</h2>

<p>For further information, please visit the Debian web pages at
<a href="$(HOME)/">https://www.debian.org/</a>, send mail to
&lt;press@debian.org&gt;, or contact the stable release team at
&lt;debian-release@lists.debian.org&gt;.</p>


