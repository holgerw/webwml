<define-tag pagetitle>Updated Debian 9: 9.7 released</define-tag>
<define-tag release_date>2019-01-23</define-tag>
#use wml::debian::news
# $Id:

<define-tag release>9</define-tag>
<define-tag codename>stretch</define-tag>
<define-tag revision>9.7</define-tag>

<define-tag dsalink><a href="$(HOME)/security/%0/dsa-%1">DSA-%1</a></define-tag>

<define-tag dsa>
    <tr><td align="center"><a href="$(HOME)/security/%0/dsa-%1">DSA-%1</a></td>
        <td align="center"><:
    my @p = ();
    for my $p (split (/,\s*/, "%2")) {
	push (@p, sprintf ('<a href="https://packages.debian.org/src:%s">%s</a>', $p, $p));
    }
    print join (", ", @p);
:></td></tr>
</define-tag>

<define-tag correction>
    <tr><td><a href="https://packages.debian.org/src:%0">%0</a></td>              <td>%1</td></tr>
</define-tag>

<define-tag srcpkg><a href="https://packages.debian.org/src:%0">%0</a></define-tag>

<p>The Debian project is pleased to announce the seventh update of its
stable distribution Debian <release> (codename <q><codename></q>). 
This point release incorporates the recent security update for <srcpkg APT>,
in order to help ensure that new installations of <codename> are not
vulnerable. No other updates are included.
</p>

<p>New installation images will be available soon at the regular locations.</p>

<p>Upgrading an existing installation to this revision can be achieved by
pointing the package management system at one of Debian's many HTTP mirrors. Due to
the nature of the included updates, in this case it is recommended to follow the
instructions listed in <dsalink 2019 4371>.</p>

<p>A comprehensive list of mirrors is available at:</p>

<div class="center">
  <a href="$(HOME)/mirror/list">https://www.debian.org/mirror/list</a>
</div>

<h2>Miscellaneous Bugfixes</h2>

<p>This stable update adds a few important corrections to the following packages:</p>

<table border=0>
<tr><th>Package</th>               <th>Reason</th></tr>
<correction base-files "Update for the point release">
</table>

<h2>Security Updates</h2>


<p>This revision adds the following security updates to the stable release.
The Security Team has already released an advisory for each of these
updates:</p>

<table border=0>
<tr><th>Advisory ID</th>  <th>Package</th></tr>
<dsa 2019 4371 apt>
</table>

<h2>URLs</h2>

<p>The complete lists of packages that have changed with this revision:</p>

<div class="center">
  <url "http://ftp.debian.org/debian/dists/<downcase <codename>>/ChangeLog">
</div>

<p>The current stable distribution:</p>

<div class="center">
  <url "http://ftp.debian.org/debian/dists/stable/">
</div>

<p>Proposed updates to the stable distribution:</p>

<div class="center">
  <url "http://ftp.debian.org/debian/dists/proposed-updates">
</div>

<p>stable distribution information (release notes, errata etc.):</p>

<div class="center">
  <a
  href="$(HOME)/releases/stable/">https://www.debian.org/releases/stable/</a>
</div>

<p>Security announcements and information:</p>

<div class="center">
  <a href="$(HOME)/security/">https://www.debian.org/security/</a>
</div>

<h2>About Debian</h2>

<p>The Debian Project is an association of Free Software developers who
volunteer their time and effort in order to produce the completely
free operating system Debian.</p>

<h2>Contact Information</h2>

<p>For further information, please visit the Debian web pages at
<a href="$(HOME)/">https://www.debian.org/</a>, send mail to
&lt;press@debian.org&gt;, or contact the stable release team at
&lt;debian-release@lists.debian.org&gt;.</p>


