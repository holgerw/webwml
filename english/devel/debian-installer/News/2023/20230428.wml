<define-tag pagetitle>Debian Installer Bookworm RC 2 release</define-tag>
<define-tag release_date>2023-04-28</define-tag>
#use wml::debian::news

<p>
The Debian Installer <a
href="https://wiki.debian.org/DebianInstaller/Team">team</a> is pleased to
announce the second release candidate of the installer for Debian 12
<q>Bookworm</q>.
</p>


<h2>Improvements in this release</h2>

<ul>
  <li>apt-setup:
    <ul>
      <li>Enable &lt;codename&gt;-updates when installing testing, not
        only when installing (old)stable (<a href="https://bugs.debian.org/991947">#991947</a>).</li>
    </ul>
  </li>
  <li>argon2:
    <ul>
      <li>Restore threading support in the installer (<a href="https://bugs.debian.org/1034696">#1034696</a>).</li>
    </ul>
  </li>
  <li>base-installer:
    <ul>
      <li>Install zstd alongside initramfs-tools, like busybox.</li>
    </ul>
  </li>
  <li>choose-mirror:
    <ul>
      <li>Update Mirrors.masterlist.</li>
    </ul>
  </li>
  <li>cryptsetup:
    <ul>
      <li>Improve support for low-memory systems (<a href="https://bugs.debian.org/1028250">#1028250</a>): Check for physical
        memory available also in PBKDF benchmark, and use only half of detected
        free memory on systems without swap.</li>
    </ul>
  </li>
  <li>debian-cd:
    <ul>
      <li>Include zstd alongside initramfs-tools, like busybox.</li>
    </ul>
  </li>
  <li>debian-installer:
    <ul>
      <li>Fix grub font loading, making sure we get the nice graphical splash
        screen in Secure Boot mode too (<a href="https://bugs.debian.org/1034535">#1034535</a>).</li>
    </ul>
  </li>
  <li>grub-installer:
    <ul>
      <li>Install shim-signed on i386 and arm64 as well as amd64 when we're
        installing grub-efi-*-signed packages.</li>
      <li>If we detect other operating systems installed, check with the user
        (at low priority) and re-enable os-prober.</li>
    </ul>
  </li>
  <li>grub2:
    <ul>
      <li>Add debconf logic for GRUB_DISABLE_OS_PROBER to make it easier to
        control things here, particularly useful for the installer (<a href="https://bugs.debian.org/1031594">#1031594</a>,
        <a href="https://bugs.debian.org/1012865">#1012865</a>, <a href="https://bugs.debian.org/1025698">#1025698</a>).</li>
      <li>Add luks2 to the signed grub efi images (<a href="https://bugs.debian.org/1001248">#1001248</a>).</li>
      <li>Fix probing of LUKS2 devices (<a href="https://bugs.debian.org/1028301">#1028301</a>).</li>
      <li>Fix Secure Boot on arm64 (<a href="https://bugs.debian.org/1033657">#1033657</a>).</li>
      <li>Don't warn about os-prober if it's not installed (<a href="https://bugs.debian.org/1020769">#1020769</a>).</li>
      <li>Fix an issue where a logical volume rename would lead grub to fail to
        boot (<a href="https://bugs.debian.org/987008">#987008</a>).</li>
    </ul>
  </li>
  <li>live-installer:
    <ul>
      <li>Remove the live packages before the cdrom is unmounted, avoiding a
        long wait at the end of the installation process.</li>
    </ul>
  </li>
  <li>partman-efi:
    <ul>
      <li>Fix detection of BIOS-bootable systems, fixing at least the “Guided -
        use entire disk and set up LVM” use case on UEFI systems (<a href="https://bugs.debian.org/834373">#834373</a>,
        <a href="https://bugs.debian.org/1033913">#1033913</a>).</li>
    </ul>
  </li>
  <li>preseed:
    <ul>
      <li>Restore support for the “hostname” alias on the kernel command line
        (<a href="https://bugs.debian.org/1031643">#1031643</a>, <a href="https://bugs.debian.org/1034062">#1034062</a>).</li>
      <li>Switch auto-install/defaultroot to bookworm.</li>
    </ul>
  </li>
</ul>


<h2>Hardware support changes</h2>

<ul>
  <li>flash-kernel:
    <ul>
      <li>Adjust OLPC XO-1.75 boot script.</li>
      <li>Add Lenovo Miix 630 and Lenovo Yoga C630.</li>
      <li>Add StarFive VisionFive board.</li>
      <li>Add D1 SoC boards.</li>
      <li>Add QEMU-related “dummy” entries.</li>
      <li>Add A20-OLinuXino_MICRO-eMMC (<a href="https://bugs.debian.org/1019881">#1019881</a>).</li>
      <li>Add Lenovo ThinkPad X13s.</li>
      <li>Add Colibri iMX6ULL eMMC.</li>
      <li>Add Raspberry Pi 3 Model B Plus Rev 1.3.</li>
    </ul>
  </li>
  <li>linux:
    <ul>
      <li>udeb: add intel_lpss* (optional) to kernel-image (<a href="https://bugs.debian.org/1032136">#1032136</a>). Many
        laptops have their touchpad accessible over I2C, only visible if LPSS is
        available in the installer.</li>
    </ul>
  </li>
</ul>


<h2>Localization status</h2>

<ul>
  <li>78 languages are supported in this release.</li>
  <li>Full translation for 41 of them.</li>
</ul>


<h2>Known issues in this release</h2>

<p>
See the <a href="$(DEVEL)/debian-installer/errata">errata</a> for
details and a full list of known issues.
</p>


<h2>Feedback for this release</h2>

<p>
We need your help to find bugs and further improve the installer, so please
try it. Installer CDs, other media and everything else you will need are
available at our <a href="$(DEVEL)/debian-installer">web site</a>.
</p>


<h2>Thanks</h2>

<p>
The Debian Installer team thanks everybody who has contributed to this
release.
</p>
