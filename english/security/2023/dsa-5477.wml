<define-tag description>security update</define-tag>
<define-tag moreinfo>
<p>Several vulnerabilities have been discovered in Samba, which could result
in information disclosure, denial of service or insufficient enforcement
of security-relevant config directives.</p>

<p>The version of Samba in the oldstable distribution (bullseye) cannot be
fully supported further: If you are using Samba as a domain controller
you should either upgrade to the stable distribution or if that's not
an immediate option consider to migrate to Samba from bullseye-backports
(which will be kept updated to the version in stable). Operating Samba
as a file/print server will continue to be supported, a separate DSA
will provide an update along with documentation about the scope of continued
support.</p>

<p>For the stable distribution (bookworm), these problems have been fixed in
version 2:4.17.10+dfsg-0+deb12u1.</p>

<p>We recommend that you upgrade your samba packages.</p>

<p>For the detailed security status of samba please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/samba">\
https://security-tracker.debian.org/tracker/samba</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2023/dsa-5477.data"
# $Id: $
