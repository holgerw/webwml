<define-tag description>security update</define-tag>
<define-tag moreinfo>
<p>It was reported that incorrect bound checks in the dsaVerify function
in node-browserify-sign, a Node.js library which adds crypto signing
for browsers, allows an attacker to perform signature forgery attacks
by constructing signatures that can be successfully verified by any
public key.</p>

<p>For the oldstable distribution (bullseye), this problem has been fixed
in version 4.2.1-1+deb11u1.</p>

<p>For the stable distribution (bookworm), this problem has been fixed in
version 4.2.1-3+deb12u1.</p>

<p>We recommend that you upgrade your node-browserify-sign packages.</p>

<p>For the detailed security status of node-browserify-sign please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/node-browserify-sign">\
https://security-tracker.debian.org/tracker/node-browserify-sign</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2023/dsa-5539.data"
# $Id: $
