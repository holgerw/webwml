<define-tag description>security update</define-tag>
<define-tag moreinfo>
<p>Two vunerabilities were discovered in c-ares, an asynchronous name
resolver library:</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-31130">CVE-2023-31130</a>

    <p>ares_inet_net_pton() is found to be vulnerable to a buffer underflow
    for certain ipv6 addresses, in particular "0::00:00:00/2" was found
    to cause an issue. c-ares only uses this function internally for
    configuration purposes, however external usage for other purposes may
    cause more severe issues.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-32067">CVE-2023-32067</a>

    <p>Target resolver may erroneously interprets a malformed UDP packet
    with a lenght of 0 as a graceful shutdown of the connection, which
    could cause a denial of service.</p></li>

</ul>

<p>For the stable distribution (bullseye), these problems have been fixed in
version 1.17.1-1+deb11u3.</p>

<p>We recommend that you upgrade your c-ares packages.</p>

<p>For the detailed security status of c-ares please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/c-ares">\
https://security-tracker.debian.org/tracker/c-ares</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2023/dsa-5419.data"
# $Id: $
