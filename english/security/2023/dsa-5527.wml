<define-tag description>security update</define-tag>
<define-tag moreinfo>
<p>The following vulnerabilities have been discovered in the WebKitGTK
web engine:</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-39928">CVE-2023-39928</a>

    <p>Marcin Noga discovered that a specially crafted web page can abuse
    a vulnerability in the MediaRecorder API to cause memory
    corruption and potentially arbitrary code execution.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-41074">CVE-2023-41074</a>

    <p>Junsung Lee and Me Li discovered that processing web content may
    lead to arbitrary code execution.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-41993">CVE-2023-41993</a>

    <p>Bill Marczak and Maddie Stone discovered that processing web
    content may lead to arbitrary code execution. Apple is aware of a
    report that this issue may have been actively exploited.</p></li>

</ul>

<p>For the oldstable distribution (bullseye), these problems have been fixed
in version 2.42.1-1~deb11u1.</p>

<p>For the stable distribution (bookworm), these problems have been fixed in
version 2.42.1-1~deb12u1.</p>

<p>We recommend that you upgrade your webkit2gtk packages.</p>

<p>For the detailed security status of webkit2gtk please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/webkit2gtk">\
https://security-tracker.debian.org/tracker/webkit2gtk</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2023/dsa-5527.data"
# $Id: $
