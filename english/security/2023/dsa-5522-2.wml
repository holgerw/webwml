<define-tag description>security update</define-tag>
<define-tag moreinfo>
<p>The patch to address
<a href="https://security-tracker.debian.org/tracker/CVE-2023-44487">CVE-2023-44487</a>
(Rapid Reset Attack) was incomplete and caused a regression when using
asynchronous I/O (the default for NIO and NIO2). DATA frames must be
included when calculating the HTTP/2 overhead count to ensure that
connections are not prematurely terminated.</p>

<p>For the oldstable distribution (bullseye), this problem has been fixed
in version 9.0.43-2~deb11u8.</p>

<p>We recommend that you upgrade your tomcat9 packages.</p>

<p>For the detailed security status of tomcat9 please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/tomcat9">\
https://security-tracker.debian.org/tracker/tomcat9</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2023/dsa-5522.data"
# $Id: $
