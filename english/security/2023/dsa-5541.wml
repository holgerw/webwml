<define-tag description>security update</define-tag>
<define-tag moreinfo>
<p>Multiple vulnerabilities have been discovered in Request Tracker, an
extensible trouble-ticket tracking system.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-41259">CVE-2023-41259</a>

    <p>Tom Wolters reported that Request Tracker is vulnerable to accepting
    unvalidated RT email headers in incoming email and the mail-gateway
    REST interface.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-41260">CVE-2023-41260</a>

    <p>Tom Wolters reported that Request Tracker is vulnerable to
    information leakage via response messages returned from requests
    sent via the mail-gateway REST interface.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-45024">CVE-2023-45024</a>

    <p>It was reported that Request Tracker is vulnerable to information
    leakage via transaction searches made by authenticated users in the
    transaction query builder.</p></li>

</ul>

<p>For the stable distribution (bookworm), these problems have been fixed in
version 5.0.3+dfsg-3~deb12u2.</p>

<p>We recommend that you upgrade your request-tracker5 packages.</p>

<p>For the detailed security status of request-tracker5 please refer to its
security tracker page at:
<a href="https://security-tracker.debian.org/tracker/request-tracker5">\
https://security-tracker.debian.org/tracker/request-tracker5</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2023/dsa-5541.data"
# $Id: $
