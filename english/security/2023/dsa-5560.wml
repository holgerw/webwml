<define-tag description>security update</define-tag>
<define-tag moreinfo>
<p>Florian Picca reported a bug in the charon-tkm daemon in strongSwan an
IKE/IPsec suite.</p>

<p>The TKM-backed version of the charon IKE daemon (charon-tkm) doesn't
check the length of received Diffie-Hellman public values before copying
them to a fixed-size buffer on the stack, causing a buffer overflow that
could potentially be exploited for remote code execution by sending a
specially crafted and unauthenticated IKE_SA_INIT message.</p>

<p>For the oldstable distribution (bullseye), this problem has been fixed
in version 5.9.1-1+deb11u4.</p>

<p>For the stable distribution (bookworm), this problem has been fixed in
version 5.9.8-5+deb12u1.</p>

<p>We recommend that you upgrade your strongswan packages.</p>

<p>For the detailed security status of strongswan please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/strongswan">\
https://security-tracker.debian.org/tracker/strongswan</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2023/dsa-5560.data"
# $Id: $
