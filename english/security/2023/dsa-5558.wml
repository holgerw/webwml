<define-tag description>security update</define-tag>
<define-tag moreinfo>
<p>Two security vulnerabilities have been discovered in Netty, a Java NIO
client/server socket framework.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-34462">CVE-2023-34462</a>

    <p>It might be possible for a remote peer to send a client hello packet
    during a TLS handshake which lead the server to buffer up to 16 MB of data
    per connection. This could lead to a OutOfMemoryError and so result in a
    denial of service.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-44487">CVE-2023-44487</a>

    <p>The HTTP/2 protocol allowed a denial of service (server resource
    consumption) because request cancellation can reset many streams quickly.
    This problem is also known as Rapid Reset Attack.</p></li>

</ul>

<p>For the oldstable distribution (bullseye), these problems have been fixed
in version 1:4.1.48-4+deb11u2.</p>

<p>For the stable distribution (bookworm), these problems have been fixed in
version 1:4.1.48-7+deb12u1.</p>

<p>We recommend that you upgrade your netty packages.</p>

<p>For the detailed security status of netty please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/netty">\
https://security-tracker.debian.org/tracker/netty</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2023/dsa-5558.data"
# $Id: $
