<define-tag description>security update</define-tag>
<define-tag moreinfo>
<p>Tavis Ormandy discovered that under specific microarchitectural
circumstances, a vector register in <q>Zen 2</q> CPUs may not be written to 0
correctly. This flaw allows an attacker to leak register contents across
concurrent processes, hyper threads and virtualized guests.</p>

<p>For details please refer to
<a href="https://lock.cmpxchg8b.com/zenbleed.html">https://lock.cmpxchg8b.com/zenbleed.html</a>
<a href="https://github.com/google/security-research/security/advisories/GHSA-v6wh-rxpg-cmm8">\
https://github.com/google/security-research/security/advisories/GHSA-v6wh-rxpg-cmm8</a></p>

<p>The initial microcode release by AMD only provides updates for second
generation EPYC CPUs: Various Ryzen CPUs are also affected, but no
updates are available yet. Fixes will be provided in a later update once
they are released.</p>

<p>For more specific details and target dates please refer to the AMD
advisory at
<a href="https://www.amd.com/en/resources/product-security/bulletin/amd-sb-7008.html">\
https://www.amd.com/en/resources/product-security/bulletin/amd-sb-7008.html</a></p>

<p>For the oldstable distribution (bullseye), this problem has been fixed
in version 3.20230719.1~deb11u1. Additionally the update contains a fix
for <a href="https://security-tracker.debian.org/tracker/CVE-2019-9836">CVE-2019-9836</a>.</p>

<p>For the stable distribution (bookworm), this problem has been fixed in
version 3.20230719.1~deb12u1.</p>

<p>We recommend that you upgrade your amd64-microcode packages.</p>

<p>For the detailed security status of amd64-microcode please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/amd64-microcode">\
https://security-tracker.debian.org/tracker/amd64-microcode</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2023/dsa-5459.data"
# $Id: $
