<define-tag description>security update</define-tag>
<define-tag moreinfo>
<p>Several vulnerabilities were discovered in BIND, a DNS server
implementation.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-2828">CVE-2023-2828</a>

    <p>Shoham Danino, Anat Bremler-Barr, Yehuda Afek and Yuval Shavitt
    discovered that a flaw in the cache-cleaning algorithm used in named
    can cause that named's configured cache size limit can be
    significantly exceeded, potentially resulting in denial of service.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-2911">CVE-2023-2911</a>

    <p>It was discovered that a flaw in the handling of recursive-clients
    quota may result in denial of service (named daemon crash).</p></li>

</ul>

<p>For the oldstable distribution (bullseye), these problems have been fixed
in version 1:9.16.42-1~deb11u1.</p>

<p>For the stable distribution (bookworm), these problems have been fixed in
version 1:9.18.16-1~deb12u1.</p>

<p>We recommend that you upgrade your bind9 packages.</p>

<p>For the detailed security status of bind9 please refer to its security
tracker page at:
<a href="https://security-tracker.debian.org/tracker/bind9">\
https://security-tracker.debian.org/tracker/bind9</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2023/dsa-5439.data"
# $Id: $
