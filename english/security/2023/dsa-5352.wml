<define-tag description>security update</define-tag>
<define-tag moreinfo>
<p>The following vulnerabilities have been discovered in the WPE WebKit
web engine:</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-23529">CVE-2023-23529</a>

    <p>An anonymous researcher discovered that processing maliciously
    crafted web content may lead to arbitrary code execution. Apple is
    aware of a report that this issue may have been actively
    exploited.</p></li>

</ul>

<p>For the stable distribution (bullseye), this problem has been fixed in
version 2.38.5-1~deb11u1.</p>

<p>We recommend that you upgrade your wpewebkit packages.</p>

<p>For the detailed security status of wpewebkit please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/wpewebkit">\
https://security-tracker.debian.org/tracker/wpewebkit</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2023/dsa-5352.data"
# $Id: $
