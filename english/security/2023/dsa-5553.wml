<define-tag description>security update</define-tag>
<define-tag moreinfo>
<p>Several vulnerabilities have been discovered in the PostgreSQL
database system.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-5868">CVE-2023-5868</a>

    <p>Jingzhou Fu discovered a memory disclosure flaw in aggregate
    function calls.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-5869">CVE-2023-5869</a>

    <p>Pedro Gallegos reported integer overflow flaws resulting in buffer
    overflows in the array modification functions.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-5870">CVE-2023-5870</a>

    <p>Hemanth Sandrana and Mahendrakar Srinivasarao reported that the
    pg_cancel_backend role can signal certain superuser processes,
    potentially resulting in denial of service.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-39417">CVE-2023-39417</a>

    <p>Micah Gate, Valerie Woolard, Tim Carey-Smith, and Christoph Berg
    reported that an extension script using @substitutions@ within
    quoting may allow to perform an SQL injection for an attacker having
    database-level CREATE privileges.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-39418">CVE-2023-39418</a>

    <p>Dean Rasheed reported that the MERGE command fails to enforce
    UPDATE or SELECT row security policies.</p></li>

</ul>

<p>For the stable distribution (bookworm), these problems have been fixed
in version 15.5-0+deb12u1.</p>

<p>We recommend that you upgrade your postgresql-15 packages.</p>

<p>For the detailed security status of postgresql-15 please refer to its
security tracker page at:
<a href="https://security-tracker.debian.org/tracker/postgresql-15">\
https://security-tracker.debian.org/tracker/postgresql-15</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2023/dsa-5553.data"
# $Id: $
