<define-tag description>security update</define-tag>
<define-tag moreinfo>
<p>A regression was discovered in the Http2UpgradeHandler class of Tomcat 9
introduced by the patch to fix
<a href="https://security-tracker.debian.org/tracker/CVE-2023-44487">CVE-2023-44487</a>
(Rapid Reset Attack). A wrong value for the overheadcount variable forced HTTP2
connections to close early.</p>

<p>For the oldstable distribution (bullseye), this problem has been fixed
in version 9.0.43-2~deb11u9.</p>

<p>We recommend that you upgrade your tomcat9 packages.</p>

<p>For the detailed security status of tomcat9 please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/tomcat9">\
https://security-tracker.debian.org/tracker/tomcat9</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2023/dsa-5522.data"
# $Id: $
