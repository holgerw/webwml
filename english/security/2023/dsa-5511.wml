<define-tag description>security update</define-tag>
<define-tag moreinfo>
<p>Several security vulnerabilities have been discovered in mosquitto, a MQTT
compatible message broker, which may be abused for a denial of service attack.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-34434">CVE-2021-34434</a>

    <p>In Eclipse Mosquitto when using the dynamic security plugin, if the ability
    for a client to make subscriptions on a topic is revoked when a durable
    client is offline, then existing subscriptions for that client are not
    revoked.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-0809">CVE-2023-0809</a>

    <p>Fix excessive memory being allocated based on malicious initial packets
    that are not CONNECT packets.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-3592">CVE-2023-3592</a>

    <p>Fix memory leak when clients send v5 CONNECT packets with a will message
    that contains invalid property types.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-28366">CVE-2023-28366</a>

    <p>The broker in Eclipse Mosquitto has a memory leak that can be abused
    remotely when a client sends many QoS 2 messages with duplicate message
    IDs, and fails to respond to PUBREC commands. This occurs because of
    mishandling of EAGAIN from the libc send function.</p></li>

<p>Additionally <a href="https://security-tracker.debian.org/tracker/CVE-2021-41039">CVE-2021-41039</a> has been fixed for Debian 11 <q>Bullseye</q>.</p>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-41039">CVE-2021-41039</a>

    <p>An MQTT v5 client connecting with a large number of user-property
    properties could cause excessive CPU usage, leading to a loss of
    performance and possible denial of service.</p></li>

</ul>

<p>For the oldstable distribution (bullseye), these problems have been fixed
in version 2.0.11-1+deb11u1.</p>

<p>For the stable distribution (bookworm), these problems have been fixed in
version 2.0.11-1.2+deb12u1.</p>

<p>We recommend that you upgrade your mosquitto packages.</p>

<p>For the detailed security status of mosquitto please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/mosquitto">\
https://security-tracker.debian.org/tracker/mosquitto</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2023/dsa-5511.data"
# $Id: $
