<define-tag description>security update</define-tag>
<define-tag moreinfo>
<p>Multiple vulnerabilities have been discovered in the lrzip compression
program which could result in denial of service or potentially the
execution of arbitrary code.</p>

<p>For the oldstable distribution (buster), these problems have been fixed
in version 0.631+git180528-1+deb10u1. This update also addresses
<a href="https://security-tracker.debian.org/tracker/CVE-2021-27345">CVE-2021-27345</a>, 
<a href="https://security-tracker.debian.org/tracker/CVE-2020-25467">CVE-2020-25467</a> and 
<a href="https://security-tracker.debian.org/tracker/CVE-2021-27347">CVE-2021-27347</a>.</p>

<p>For the stable distribution (bullseye), these problems have been fixed in
version 0.641-1+deb11u1.</p>

<p>We recommend that you upgrade your lrzip packages.</p>

<p>For the detailed security status of lrzip please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/lrzip">\
https://security-tracker.debian.org/tracker/lrzip</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2022/dsa-5145.data"
# $Id: $
