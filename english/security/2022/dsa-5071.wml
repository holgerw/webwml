<define-tag description>security update</define-tag>
<define-tag moreinfo>
<p>Several vulnerabilities were discovered in Samba, a SMB/CIFS file,
print, and login server for Unix.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-44142">CVE-2021-44142</a>

    <p>Orange Tsai reported an out-of-bounds heap write vulnerability in
    the VFS module vfs_fruit, which could result in remote execution of
    arbitrary code as root.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-0336">CVE-2022-0336</a>

    <p>Kees van Vloten reported that Samba AD users with permission to
    write to an account can impersonate arbitrary services.</p></li>

</ul>

<p>For the oldstable distribution (buster), these problems have been fixed
in version 2:4.9.5+dfsg-5+deb10u3. As per DSA 5015-1, <a href="https://security-tracker.debian.org/tracker/CVE-2022-0336">CVE-2022-0336</a> has
not been addressed for the oldstable distribution (buster).</p>

<p>For the stable distribution (bullseye), these problems have been fixed
in version 2:4.13.13+dfsg-1~deb11u3. Additionally, some followup fixes
for <a href="https://security-tracker.debian.org/tracker/CVE-2020-25717">CVE-2020-25717</a> are included in this update (Cf. #1001068).</p>

<p>We recommend that you upgrade your samba packages.</p>

<p>For the detailed security status of samba please refer to its security
tracker page at:
<a href="https://security-tracker.debian.org/tracker/samba">https://security-tracker.debian.org/tracker/samba</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2022/dsa-5071.data"
# $Id: $
