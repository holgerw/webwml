<define-tag description>security update</define-tag>
<define-tag moreinfo>
<p>Several vulnerabilities have been discovered in the chromium web browser.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-5787">CVE-2019-5787</a>

    <p>Zhe Jin discovered a use-after-free issue.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-5788">CVE-2019-5788</a>

    <p>Mark Brand discovered a use-after-free issue in the FileAPI
    implementation.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-5789">CVE-2019-5789</a>

    <p>Mark Brand discovered a use-after-free issue in the WebMIDI
    implementation.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-5790">CVE-2019-5790</a>

    <p>Dimitri Fourny discovered a buffer overflow issue in the v8 javascript
    library.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-5791">CVE-2019-5791</a>

    <p>Choongwoo Han discovered a type confusion issue in the v8 javascript
    library.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-5792">CVE-2019-5792</a>

    <p>pdknsk discovered an integer overflow issue in the pdfium library.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-5793">CVE-2019-5793</a>

    <p>Jun Kokatsu discovered a permissions issue in the Extensions
    implementation.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-5794">CVE-2019-5794</a>

    <p>Juno Im of Theori discovered a user interface spoofing issue.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-5795">CVE-2019-5795</a>

    <p>pdknsk discovered an integer overflow issue in the pdfium library.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-5796">CVE-2019-5796</a>

    <p>Mark Brand discovered a race condition in the Extensions implementation.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-5797">CVE-2019-5797</a>

    <p>Mark Brand discovered a race condition in the DOMStorage implementation.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-5798">CVE-2019-5798</a>

    <p>Tran Tien Hung discovered an out-of-bounds read issue in the skia library.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-5799">CVE-2019-5799</a>

    <p>sohalt discovered a way to bypass the Content Security Policy.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-5800">CVE-2019-5800</a>

    <p>Jun Kokatsu discovered a way to bypass the Content Security Policy.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-5802">CVE-2019-5802</a>

    <p>Ronni Skansing discovered a user interface spoofing issue.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-5803">CVE-2019-5803</a>

    <p>Andrew Comminos discovered a way to bypass the Content Security Policy.</p></li>

</ul>

<p>For the stable distribution (stretch), these problems have been fixed in
version 73.0.3683.75-1~deb9u1.</p>

<p>We recommend that you upgrade your chromium packages.</p>

<p>For the detailed security status of chromium please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/chromium">https://security-tracker.debian.org/tracker/chromium</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2019/dsa-4421.data"
# $Id: $
