<define-tag description>security update</define-tag>
<define-tag moreinfo>
<p>Multiple vulnerabilities have been discovered in Wireshark, a network
protocol analyzer which could result in denial of service or the execution of
arbitrary code.</p>

<p>For the oldstable distribution (buster), <a href="https://security-tracker.debian.org/tracker/CVE-2021-39925">\
CVE-2021-39925</a> has been fixed in version 2.6.20-0+deb10u2.</p>

<p>For the stable distribution (bullseye), these problems have been fixed in
version 3.4.10-0+deb11u1.</p>

<p>We recommend that you upgrade your wireshark packages.</p>

<p>For the detailed security status of wireshark please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/wireshark">\
https://security-tracker.debian.org/tracker/wireshark</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2021/dsa-5019.data"
# $Id: $
