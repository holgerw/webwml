<define-tag description>security update</define-tag>
<define-tag moreinfo>
<p>Several vulnerabilities have been discovered in Samba, a SMB/CIFS file,
print, and login server for Unix.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-2124">CVE-2016-2124</a>

    <p>Stefan Metzmacher reported that SMB1 client connections can be
    downgraded to plaintext authentication.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-25717">CVE-2020-25717</a>

    <p>Andrew Bartlett reported that Samba may map domain users to local
    users in an undesired way, allowing for privilege escalation. The
    update introduces a new parameter <q>min domain uid</q> (default to 1000)
    to not accept a UNIX uid below this value.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-25718">CVE-2020-25718</a>

    <p>Andrew Bartlett reported that Samba as AD DC, when joined by an
    RODC, did not confirm if the RODC was allowed to print a ticket for
    that user, allowing an RODC to print administrator tickets.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-25719">CVE-2020-25719</a>

    <p>Andrew Bartlett reported that Samba as AD DC, did not always rely on
    the SID and PAC in Kerberos tickets and could be confused about the
    user a ticket represents. If a privileged account was attacked this
    could lead to total domain compromise.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-25721">CVE-2020-25721</a>

    <p>Andrew Bartlett reported that Samba as a AD DC did not provide a way
    for Linux applications to obtain a reliable SID (and samAccountName)
    in issued tickets.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-25722">CVE-2020-25722</a>

    <p>Andrew Bartlett reported that Samba as AD DC did not do sufficient
    access and conformance checking of data stored, potentially allowing
    total domain compromise.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-3738">CVE-2021-3738</a>

    <p>William Ross reported that the Samba AD DC RPC server can use memory
    that was free'd when a sub-connection is closed, resulting in denial
    of service, and potentially, escalation of privileges.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-23192">CVE-2021-23192</a>

    <p>Stefan Metzmacher reported that if a client to a Samba server sent a
    very large DCE/RPC request, and chose to fragment it, an attacker
    could replace later fragments with their own data, bypassing the
    signature requirements.</p></li>

</ul>

<p>For the stable distribution (bullseye), these problems have been fixed in
version 2:4.13.13+dfsg-1~deb11u2.</p>

<p>We recommend that you upgrade your samba packages.</p>

<p>For the detailed security status of samba please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/samba">\
https://security-tracker.debian.org/tracker/samba</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2021/dsa-5003.data"
# $Id: $
