<define-tag description>security update</define-tag>
<define-tag moreinfo>
<p>It was discovered that incorrect memory handling in the SLIRP networking
implementation could result in denial of service or potentially the
execution of arbitrary code.</p>

<p>For the stable distribution (buster), this problem has been fixed in
version 1:3.1+dfsg-8+deb10u7. In addition this update fixes a regression
caused by the patch for <a href="https://security-tracker.debian.org/tracker/CVE-2020-13754">\
CVE-2020-13754</a>, which could lead to startup
failures in some Xen setups.</p>

<p>We recommend that you upgrade your qemu packages.</p>

<p>For the detailed security status of qemu please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/qemu">\
https://security-tracker.debian.org/tracker/qemu</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2020/dsa-4733.data"
# $Id: $
