<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>It was discovered that there was an issue in ruby-doorkeeper, an OAuth2
provider for Ruby on Rails applications. Doorkeeper automatically processed
authorization requests without user consent for public clients that have been
previously approved, but public clients are inherently vulnerable to
impersonation as their identity cannot be assured.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-34246">CVE-2023-34246</a>

    <p>Doorkeeper is an OAuth 2 provider for Ruby on Rails / Grape. Prior to
    version 5.6.6, Doorkeeper automatically processes authorization requests
    without user consent for public clients that have been previous approved.
    Public clients are inherently vulnerable to impersonation, their identity
    cannot be assured. This issue is fixed in version 5.6.6.</p></li>

</ul>

<p>For Debian 10 <q>Buster</q>, this problem has been fixed in version
4.4.2-1+deb10u1.</p>

<p>We recommend that you upgrade your ruby-doorkeeper packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3494.data"
# $Id: $
