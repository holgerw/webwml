<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Several out of bounds memory access and buffer overflows were fixed in xrdp,
an open source project which provides a graphical login to remote machines
using Microsoft Remote Desktop Protocol (RDP)</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-23468">CVE-2022-23468</a>

    <p>xrdp < v0.9.21 contain a buffer over flow in xrdp_login_wnd_create() function.
    There are no known workarounds for this issue.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-23478">CVE-2022-23478</a>

    <p>xrdp < v0.9.21 contain a Out of Bound Write in
    xrdp_mm_trans_process_drdynvc_channel_open() function. There are no known
    workarounds for this issue.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-23479">CVE-2022-23479</a>

    <p>xrdp < v0.9.21 contain a buffer over flow in xrdp_mm_chan_data_in() function.
    There are no known workarounds for this issue.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-23483">CVE-2022-23483</a>

    <p>xrdp < v0.9.21 contain a Out of Bound Read in libxrdp_send_to_channel() function.
    There are no known workarounds for this issue.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-23484">CVE-2022-23484</a>

    <p>xrdp < v0.9.21 contain a Integer Overflow in xrdp_mm_process_rail_update_window_text()
    function. There are no known workarounds for this issue.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-23493">CVE-2022-23493</a>

    <p>xrdp < v0.9.21 contain a Out of Bound Read in xrdp_mm_trans_process_drdynvc_channel_close()
    function. There are no known workarounds for this issue.</p></li>

</ul>

<p>For Debian 10 buster, these problems have been fixed in version
0.9.9-1+deb10u2.</p>

<p>We recommend that you upgrade your xrdp packages.</p>

<p>For the detailed security status of xrdp please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/xrdp">https://security-tracker.debian.org/tracker/xrdp</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3370.data"
# $Id: $
