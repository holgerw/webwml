<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Issues were found in ncurses, a collection of shared libraries for
terminal handling, which could lead to denial of service.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-39537">CVE-2021-39537</a>

    <p>It has been discovered that the <code>tic(1)</code> utility is susceptible to a
    heap overflow on crafted input due to improper bounds checking.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-29491">CVE-2023-29491</a>

    <p>Jonathan Bar Or, Michael Pearse and Emanuele Cozzi have discovered
    that when ncurses is used by a setuid application, a local user can
    trigger security-relevant memory corruption via malformed data in a
    terminfo database file found in <code>$HOME/.terminfo</code> or reached via the
    <code>TERMINFO</code> or <code>TERM</code> environment variables.</p>

    <p>In order to mitigate this issue, ncurses now further restricts
    programs running with elevated privileges (setuid/setgid programs).
    Programs run by the superuser remain able to load custom terminfo
    entries.</p>

    <p>This change aligns ncurses' behavior in buster-security with that of
    Debian Bullseye's latest point release (6.2+20201114-2+deb11u2).</p></li>

</ul>

<p>For Debian 10 buster, these problems have been fixed in version
6.1+20181013-2+deb10u5.</p>

<p>We recommend that you upgrade your ncurses packages.</p>

<p>For the detailed security status of ncurses please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/ncurses">https://security-tracker.debian.org/tracker/ncurses</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3682.data"
# $Id: $
