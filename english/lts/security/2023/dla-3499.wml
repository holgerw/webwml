<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Open Redirect vulnerabilities were found in libapache2-mod-auth-openidc,
OpenID Connect Relying Party implementation for Apache, which could lead
to information disclosure via phishing attacks.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-39191">CVE-2021-39191</a>

    <p>The 3rd-party init SSO functionality of <code>mod_auth_openidc</code> was
    reported to be vulnerable to an open redirect attack by supplying a
    crafted URL in the <code>target_link_uri</code> parameter.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-23527">CVE-2022-23527</a>

    <p>When providing a logout parameter to the redirect URI,
    <code>mod_auth_openidc</code> failed to properly check for URLs starting with
    "<code>/&bsol;t</code>", leading to an open redirect.</p></li>

</ul>

<p>For Debian 10 buster, these problems have been fixed in version
2.3.10.2-1+deb10u3.</p>

<p>We recommend that you upgrade your libapache2-mod-auth-openidc packages.</p>

<p>For the detailed security status of libapache2-mod-auth-openidc please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/libapache2-mod-auth-openidc">https://security-tracker.debian.org/tracker/libapache2-mod-auth-openidc</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3499.data"
# $Id: $
