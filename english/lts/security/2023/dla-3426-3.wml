<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Another regression was identified in Netatalk, the Apple Filing Protocol
service, introduced with the patch for <a href="https://security-tracker.debian.org/tracker/CVE-2022-23123">CVE-2022-23123</a>. It is impacting a
subset of users that have certain metadata in their shared files. The issue
leads to an unavoidable crash and renders netatalk useless with their shared
volumes.</p>

<p>Separately, it also contains a fix for saving MS Office files onto an
otherwise functioning shared volume.</p>

<p>For Debian 10 buster, this problem has been fixed in version
3.1.12~ds-3+deb10u3.</p>

<p>We recommend that you upgrade your netatalk packages.</p>

<p>For the detailed security status of netatalk please refer to
its security tracker page at:
<a rel="nofollow" href="https://security-tracker.debian.org/tracker/netatalk">https://security-tracker.debian.org/tracker/netatalk</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a rel="nofollow" href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3426-3.data"
# $Id: $
