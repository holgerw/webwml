<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>A security vulnerability was found in the Open VMware Tools. A malicious actor
that has been granted Guest Operation Privileges in a target virtual machine
may be able to elevate their privileges if that target virtual machine has been
assigned a more privileged Guest Alias.</p>

<p>For Debian 10 buster, this problem has been fixed in version
2:10.3.10-1+deb10u5.</p>

<p>We recommend that you upgrade your open-vm-tools packages.</p>

<p>For the detailed security status of open-vm-tools please refer to
its security tracker page at:
<a rel="nofollow" href="https://security-tracker.debian.org/tracker/open-vm-tools">https://security-tracker.debian.org/tracker/open-vm-tools</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a rel="nofollow" href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3597.data"
# $Id: $
