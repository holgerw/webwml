<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Hubert Kario discovered a timing side channel in the RSA decryption
implementation of the GNU TLS library.</p>

<p>For Debian 10 buster, this problem has been fixed in version
3.6.7-4+deb10u10.</p>

<p>We recommend that you upgrade your gnutls28 packages.</p>

<p>For the detailed security status of gnutls28 please refer to
its security tracker page at:
<a rel="nofollow" href="https://security-tracker.debian.org/tracker/gnutls28">https://security-tracker.debian.org/tracker/gnutls28</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a rel="nofollow" href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3321.data"
# $Id: $
