<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>libxpm is a library handling X PixMap image format (so called xpm files).
xpm files are an extension of the monochrome X BitMap format specified
in the X protocol, and are commonly used in traditional X applications.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-4883">CVE-2022-4883</a>

    <p>When processing files with
    .Z or .gz extensions, the library calls external programs
    to compress and uncompress files, relying on the
    PATH environment variable to find these programs,
    which could allow a malicious user to execute other programs
    by manipulating the PATH environment variable.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-44617">CVE-2022-44617</a>

    <p>When processing a file with width of 0
    and a very large height, some parser functions will be
    called repeatedly and can lead to an infinite loop,
    resulting in a Denial of Service in the application linked
    to the library.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-46285">CVE-2022-46285</a>

    <p>When parsing a file with a comment
    not closed, an end-of-file condition will not be detected,
    leading to an infinite loop and resulting in a
    Denial of Service in the application linked to the library.</p></li>

</ul>

<p>For Debian 10 buster, these problems have been fixed in version
1:3.5.12-1+deb10u1.</p>

<p>We recommend that you upgrade your libxpm packages.</p>

<p>For the detailed security status of libxpm please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/libxpm">https://security-tracker.debian.org/tracker/libxpm</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3459.data"
# $Id: $
