<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>It was discovered that python-urllib3, a user-friendly HTTP client
library for Python, did not remove the HTTP request body when an HTTP
redirect response using status 301, 302, or 303 after the request had
its method changed from one that could accept a request body, like POST,
to GET, as required by the HTTP RFCs.  This could lead to information
disclosure.</p>

<p>For Debian 10 buster, these problems have been fixed in version
1.24.1-1+deb10u2.</p>

<p>We recommend that you upgrade your python-urllib3 packages.</p>

<p>For the detailed security status of python-urllib3 please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/python-urllib3">https://security-tracker.debian.org/tracker/python-urllib3</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3649.data"
# $Id: $
