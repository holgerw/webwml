<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Multiple vulnerabilies were fixed in php-dompdf a CSS 2.1 compliant HTML
to PDF converter, written in PHP.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-3838">CVE-2021-3838</a>

    <p>php-dompdf was vulnerable to deserialization of Untrusted Data using
    PHAR deserialization (phar://) as url for image.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-2400">CVE-2022-2400</a>

    <p>php-dompdf was vulnerable to External Control of File Name bypassing
    unallowed access verification.</p></li>

</ul>

<p>For Debian 10 buster, these problems have been fixed in version
0.6.2+dfsg-3+deb10u1.</p>

<p>We recommend that you upgrade your php-dompdf packages.</p>

<p>For the detailed security status of php-dompdf please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/php-dompdf">https://security-tracker.debian.org/tracker/php-dompdf</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3495.data"
# $Id: $
