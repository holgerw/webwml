<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Pierre Rudloff discovered a potential XSS vulnerability in Symfony, a PHP
framework. Some Twig filters in CodeExtension use `is_safe=html` but do not
actually ensure their input is safe. Symfony now escapes the output of the
affected filters.</p>

<p>For Debian 10 buster, this problem has been fixed in version
3.4.22+dfsg-2+deb10u3.</p>

<p>We recommend that you upgrade your symfony packages.</p>

<p>For the detailed security status of symfony please refer to
its security tracker page at:
<a rel="nofollow" href="https://security-tracker.debian.org/tracker/symfony">https://security-tracker.debian.org/tracker/symfony</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a rel="nofollow" href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3664.data"
# $Id: $
