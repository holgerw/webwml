<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Arbitrary file write vulnerabilities were discovered in pandoc, an
Haskell library and CLI tool for converting from one markup format to
another.  These vulnerabilities can be triggered by providing a
specially crafted image element in the input when generating files using
the <code>--extract-media</code> option or outputting to PDF format, and allow an
attacker to create or overwrite arbitrary files on the system (depending
on the privileges of the process running pandoc).</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-35936">CVE-2023-35936</a>

    <p>Entroy C discovered that by appending percent-encoded directory
    components at the end of malicious <code>data:</code> URI, an attacker could
    trick pandoc into creating or overwriting arbitrary files on the
    system.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-38745">CVE-2023-38745</a>

    <p>Guilhem Moulin discovered that the upstream fix for
    <a href="https://security-tracker.debian.org/tracker/CVE-2023-35936">CVE-2023-35936</a> was
    incomplete, namely that the vulnerability remained when encoding '<code>&percnt;</code>'
    characters as '<code>&percnt;25</code>'.</p></li>

</ul>

<p>For Debian 10 buster, these problems have been fixed in version
2.2.1-3+deb10u1.</p>

<p>We recommend that you upgrade your pandoc packages.</p>

<p>For the detailed security status of pandoc please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/pandoc">https://security-tracker.debian.org/tracker/pandoc</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3507.data"
# $Id: $
