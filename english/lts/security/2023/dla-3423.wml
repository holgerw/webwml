<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>It was discovered that there was a potential credential stealing attack in
<code>epiphany-browser</code>, the default GNOME web browser.</p>

<p>When using a sandboxed Content Security Policy (CSP) or the HTML
<code>iframe</code> tag, the sandboxed web content was trusted by the
main/surrounding resource. After this change, however, the password manager is
disabled entirely in this situations, so that the untrusted web content cannot
exfiltrate passwords.</p>


<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-26081">CVE-2023-26081</a>

    <p>In Epiphany (aka GNOME Web) through 43.0, untrusted web content can
    trick users into exfiltrating passwords, because autofill occurs in
    sandboxed contexts.</p></li>

</ul>

<p>For Debian 10 <q>Buster</q>, this problem has been fixed in version
3.32.1.2-3~deb10u3.</p>

<p>We recommend that you upgrade your epiphany-browser packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3423.data"
# $Id: $
