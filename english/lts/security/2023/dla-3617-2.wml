<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>A regression was discovered in the Http2UpgradeHandler class of Tomcat 9
introduced by the patch to fix <a href="https://security-tracker.debian.org/tracker/CVE-2023-44487">CVE-2023-44487</a> (Rapid Reset Attack). A wrong
value for the overheadcount variable forced HTTP2 connections to close early.</p>

<p>For Debian 10 buster, this problem has been fixed in version
9.0.31-1~deb10u10.</p>

<p>We recommend that you upgrade your tomcat9 packages.</p>

<p>For the detailed security status of tomcat9 please refer to
its security tracker page at:
<a rel="nofollow" href="https://security-tracker.debian.org/tracker/tomcat9">https://security-tracker.debian.org/tracker/tomcat9</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a rel="nofollow" href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3617-2.data"
# $Id: $
