<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Kokorin Vsevolod discovered a Prototype Pollution vulnerability in
node-tough-cookie, a RFC6265 Cookies and Cookie Jar library for node.js.
The issue is due to improper handling of Cookies when using CookieJar in
<code>rejectPublicSuffixes=false</code> mode.</p>

<p>For Debian 10 buster, this problem has been fixed in version
2.3.4+dfsg-1+deb10u1.</p>

<p>We recommend that you upgrade your node-tough-cookie packages.</p>

<p>For the detailed security status of node-tough-cookie please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/node-tough-cookie">https://security-tracker.debian.org/tracker/node-tough-cookie</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3488.data"
# $Id: $
