<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>It was discovered that there were two potential denial of service (DoS)
attacks in lldpd, a implementation of the IEEE 802.1ab (LLDP) protocol used to
administer and monitor networking devices.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-27827">CVE-2020-27827</a>

    <p>A flaw was found in multiple versions of OpenvSwitch. Specially crafted
    LLDP packets can cause memory to be lost when allocating data to handle
    specific optional TLVs, potentially causing a denial of service. The
    highest threat from this vulnerability is to system availability.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-43612">CVE-2021-43612</a>

    <p>crash in SONMP decoder</p></li>

</ul>

<p>For Debian 10 <q>Buster</q>, these problems have been fixed in version
1.0.3-1+deb10u1.</p>

<p>We recommend that you upgrade your lldpd packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3389.data"
# $Id: $
