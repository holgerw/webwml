<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Multiple vulnerabilties have been found in Amanda,a backup system
designed to archive many computers on a network to a single
large-capacity tape drive. The vulnerabilties potentially allows local
privilege escalation from the backup user to root or leak information
whether a directory exists in the filesystem.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-37703">CVE-2022-37703</a>

    <p>In Amanda 3.5.1, an information leak vulnerability was found in the
    calcsize SUID binary. An attacker can abuse this vulnerability to
    know if a directory exists or not anywhere in the fs. The binary
    will use `opendir()` as root directly without checking the path,
    letting the attacker provide an arbitrary path.</p>

<p></p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-37705">CVE-2022-37705</a>

    <p>A privilege escalation flaw was found in Amanda 3.5.1 in which the
    backup user can acquire root privileges. The vulnerable component is
    the runtar SUID program, which is a wrapper to run /usr/bin/tar with
    specific arguments that are controllable by the attacker. This
    program mishandles the arguments passed to tar binary.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-30577">CVE-2023-30577</a>

    <p>The SUID binary <q>runtar</q> can accept the possibly malicious GNU tar
    options if fed with some non-argument option starting with
    "--exclude" (say --exclude-vcs). The following option will be
    accepted as <q>good</q> and it could be an option passing some
    script/binary that would be executed with root permissions.</p></li>

</ul>

<p>For Debian 10 buster, these problems have been fixed in version
1:3.5.1-2+deb10u2.</p>

<p>We recommend that you upgrade your amanda packages.</p>

<p>For the detailed security status of amanda please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/amanda">https://security-tracker.debian.org/tracker/amanda</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3681.data"
# $Id: $
