<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Brief introduction</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-34058">CVE-2023-34058</a>

    <p>A file descriptor hijack vulnerability was found in
    the vmware-user-suid-wrapper command.
    A malicious actor with non-root privileges might have been able
    to hijack the /dev/uinput file descriptor allowing
    them to simulate user inputs.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-34059">CVE-2023-34059</a>

    <p>A SAML Token Signature Bypass vulnerability was found.
    A malicious actor that has been granted Guest Operation Privileges
    in a target virtual machine might have been able to
    elevate their privileges if that target
    virtual machine has been assigned a more privileged Guest Alias.</p></li>

</ul>

<p>For Debian 10 buster, these problems have been fixed in version
2:10.3.10-1+deb10u6.</p>

<p>We recommend that you upgrade your open-vm-tools packages.</p>

<p>For the detailed security status of open-vm-tools please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/open-vm-tools">https://security-tracker.debian.org/tracker/open-vm-tools</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3646.data"
# $Id: $
