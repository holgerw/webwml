<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>It was discovered that there were two denial of service vulnerabilities
in graphicsmagick, a collection of image processing tools:</p>

<ul>

  <li><a href="https://security-tracker.debian.org/tracker/CVE-2016-5240">CVE-2016-5240</a>

    <p>Prevent denial-of-service by detecting and rejecting
    negative stroke-dasharray arguments which were resulting in an
    endless loop.</p></li>

  <li><a href="https://security-tracker.debian.org/tracker/CVE-2016-5241">CVE-2016-5241</a>

    <p>Fix divide-by-zero problem if fill or stroke pattern
    image has zero columns or rows to prevent DoS attack.</p></li>

</ul>

<p>For Debian 7 <q>Wheezy</q>, this issue has been fixed in graphicsmagick version
1.3.16-1.1+deb7u3.</p>

<p>We recommend that you upgrade your graphicsmagick packages.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2016/dla-574.data"
# $Id: $
