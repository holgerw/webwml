<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Several vulnerabilities were discovered in QEMU, a fast processor
emulator (notably used in KVM and Xen HVM virtualization). An attacker
could trigger a denial-of-service (DoS), information leak, and
possibly execute arbitrary code with the privileges of the QEMU
process on the host.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-15469">CVE-2020-15469</a>

    <p>A MemoryRegionOps object may lack read/write callback methods,
    leading to a NULL pointer dereference.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-15859">CVE-2020-15859</a>

    <p>QEMU has a use-after-free in hw/net/e1000e_core.c because a guest
    OS user can trigger an e1000e packet with the data's address set
    to the e1000e's MMIO address.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-25084">CVE-2020-25084</a>

    <p>QEMU has a use-after-free in hw/usb/hcd-xhci.c because the
    usb_packet_map return value is not checked.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-28916">CVE-2020-28916</a>

    <p>hw/net/e1000e_core.c has an infinite loop via an RX descriptor
    with a NULL buffer address.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-29130">CVE-2020-29130</a>

    <p>slirp.c has a buffer over-read because it tries to read a certain
    amount of header data even if that exceeds the total packet
    length.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-29443">CVE-2020-29443</a>

    <p>ide_atapi_cmd_reply_end in hw/ide/atapi.c allows out-of-bounds
    read access because a buffer index is not validated.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-20181">CVE-2021-20181</a>

    <p>9pfs: ZDI-CAN-10904: QEMU Plan 9 file system TOCTOU privilege
    escalation vulnerability.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-20221">CVE-2021-20221</a>

    <p>aarch64: GIC: out-of-bound heap buffer access via an interrupt ID
    field.</p></li>

</ul>

<p>For Debian 9 stretch, these problems have been fixed in version
1:2.8+dfsg-6+deb9u13.</p>

<p>We recommend that you upgrade your qemu packages.</p>

<p>For the detailed security status of qemu please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/qemu">https://security-tracker.debian.org/tracker/qemu</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2021/dla-2560.data"
# $Id: $
