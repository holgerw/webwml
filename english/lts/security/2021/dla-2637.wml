<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>The Drupal project identified a vulnerability in the sanitization
performed in the _filter_xss_arttributes function, potentially
allowing a cross-site scripting, and granted it the Drupal Security
Advisory ID SA-CORE-2021-002:</p>

    <p>https://www.drupal.org/sa-core-2021-002</p>

<p>No CVE number has been announced.</p>

<p>For Debian 9 <q>Stretch</q>, the fix to this issue was backported in
version 7.52-2+deb9u15.</p>

<p>We recommend you upgrade your drupal7 package.</p>

<p>For detailed security status of drupal7, please refer to its security
tracker page:</p>

    <p>https://security-tracker.debian.org/tracker/source-package/drupal7</p>

<p>Further information about Debian LTS security advisories, how to
apply these updates to your system, and other frequently asked
questions can be found at:</p>

    <p>https://wiki.debian.org/LTS</p>

<p>For Debian 9 <q>Stretch</q>, these issues have been fixed in drupal7 version 7.52-2+deb9u15</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2021/dla-2637.data"
# $Id: $
