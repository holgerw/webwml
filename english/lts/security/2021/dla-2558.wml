<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>xterm through Patch #365 allows remote attackers to cause a
denial of service (segmentation fault) or possibly have
unspecified other impact via a crafted UTF-8 character sequence.</p>

<p>For Debian 9 stretch, this problem has been fixed in version
327-2+deb9u1.</p>

<p>We recommend that you upgrade your xterm packages.</p>

<p>For the detailed security status of xterm please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/xterm">https://security-tracker.debian.org/tracker/xterm</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2021/dla-2558.data"
# $Id: $
