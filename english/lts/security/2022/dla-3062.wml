<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>uBlock, a Firefox add-on and efficient ads, malware and trackers blocker,
supported an arbitrary depth of parameter nesting for strict blocking, which
allows crafted web sites to cause a denial of service (unbounded recursion that
can trigger memory consumption and a loss of all blocking functionality).</p>

<p>Please note that webext-ublock-origin was replaced by webext-ublock-originfirefox.</p>

<p>For Debian 9 stretch, this problem has been fixed in version
1.42.0+dfsg-1~deb9u1.</p>

<p>We recommend that you upgrade your ublock-origin packages.</p>

<p>For the detailed security status of ublock-origin please refer to
its security tracker page at:
<a rel="nofollow" href="https://security-tracker.debian.org/tracker/ublock-origin">https://security-tracker.debian.org/tracker/ublock-origin</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a rel="nofollow" href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2022/dla-3062.data"
# $Id: $
