<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>It was discovered that the Debian package of Avahi, a framework for Multicast
DNS Service Discovery, executed the script avahi-daemon-check-dns.sh with root
privileges which would allow a local attacker to cause a denial of service or
create arbitrary empty files via a symlink attack on files under
/var/run/avahi-daemon. This script is now executed with the privileges of user
and group avahi and requires sudo in order to achieve that.</p>

<p>The aforementioned script has been removed from Debian 10 <q>Buster</q> onwards. The
workaround could not be implemented for Debian 9 <q>Stretch</q> because libnss-mdns
0.10 does not provide the required functionality to replace it.</p>

<p>Furthermore it was found (<a href="https://security-tracker.debian.org/tracker/CVE-2021-3468">CVE-2021-3468</a>) that the event used to signal the
termination of the client connection on the avahi Unix socket is not correctly
handled in the client_work function, allowing a local attacker to trigger an
infinite loop.</p>

<p>For Debian 9 stretch, these problems have been fixed in version
0.6.32-2+deb9u1.</p>

<p>We recommend that you upgrade your avahi packages.</p>

<p>For the detailed security status of avahi please refer to
its security tracker page at:
<a rel="nofollow" href="https://security-tracker.debian.org/tracker/avahi">https://security-tracker.debian.org/tracker/avahi</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a rel="nofollow" href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2022/dla-3047.data"
# $Id: $
