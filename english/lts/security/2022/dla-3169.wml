<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>It was discovered that Apache Batik, a SVG library for Java, allowed
attackers to run arbitrary Java code by processing a malicious SVG file.</p>

<p>For Debian 10 buster, these problems have been fixed in version
1.10-2+deb10u2.</p>

<p>We recommend that you upgrade your batik packages.</p>

<p>For the detailed security status of batik please refer to
its security tracker page at:
<a rel="nofollow" href="https://security-tracker.debian.org/tracker/batik">https://security-tracker.debian.org/tracker/batik</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a rel="nofollow" href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2022/dla-3169.data"
# $Id: $
