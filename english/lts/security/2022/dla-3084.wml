<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Two security issues have been discovered in ndpi: deep packet inspection
library.</p>

<p></p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-15472">CVE-2020-15472</a>

    <p>H.323 dissector is vulnerable to a heap-based buffer over-read in ndpi_search_h323 in lib/protocols/h323.c.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-15476">CVE-2020-15476</a>

    <p>Oracle protocol dissector has a heap-based buffer over-read in ndpi_search_oracle.</p></li>

</ul>

<p>For Debian 10 buster, these problems have been fixed in version
2.6-3+deb10u1.</p>

<p>We recommend that you upgrade your ndpi packages.</p>

<p>For the detailed security status of ndpi please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/ndpi">https://security-tracker.debian.org/tracker/ndpi</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2022/dla-3084.data"
# $Id: $
