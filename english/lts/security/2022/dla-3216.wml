<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Mitsurugi Heishiro found out that in VLC, multimedia player and streamer,
a potential buffer overflow in the vnc module could trigger remote code
execution if a malicious vnc URL is deliberately played.</p>

<p>For Debian 10 buster, this problem has been fixed in version
3.0.17.4-0+deb10u2.</p>

<p>We recommend that you upgrade your vlc packages.</p>

<p>For the detailed security status of vlc please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/vlc">https://security-tracker.debian.org/tracker/vlc</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2022/dla-3216.data"
# $Id: $
