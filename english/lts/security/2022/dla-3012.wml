<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Felix Wilhelm discovered that libxml2, the GNOME XML library, did not correctly
check for integer overflows or used wrong types for buffer sizes. This could
result in out-of-bounds writes or other memory errors when working on large,
multi-gigabyte buffers.</p>

<p>For Debian 9 stretch, this problem has been fixed in version
2.9.4+dfsg1-2.2+deb9u7.</p>

<p>We recommend that you upgrade your libxml2 packages.</p>

<p>For the detailed security status of libxml2 please refer to
its security tracker page at:
<a rel="nofollow" href="https://security-tracker.debian.org/tracker/libxml2">https://security-tracker.debian.org/tracker/libxml2</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a rel="nofollow" href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2022/dla-3012.data"
# $Id: $
