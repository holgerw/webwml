<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-12584">CVE-2018-12584</a>
      <p>A flaw in function ConnectionBase::preparseNewBytes of
      resip/stack/ConnectionBase.cxx has been detected, that
      allows remote attackers to cause a denial of service
      (buffer overflow) or possibly execute arbitrary code
      when TLS communication is enabled.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-11521">CVE-2017-11521</a>

      <p>A flaw in function SdpContents::Session::Medium::parse of
      resip/stack/SdpContents.cxx has been detected, that allows
      remote attackers to cause a denial of service (memory
      consumption) by triggering many media connections.</p></li>

</ul>

<p>For Debian 8 <q>Jessie</q>, these problems have been fixed in version
1:1.9.7-5+deb8u1.</p>

<p>We recommend that you upgrade your resiprocate packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2018/dla-1439.data"
# $Id: $
