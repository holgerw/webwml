<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>A security vulnerability was discovered in lucene-solr, an enterprise
search server.</p>

<p>The DataImportHandler, an optional but popular module to pull in data
from databases and other sources, has a feature in which the whole DIH
configuration can come from a request's <q>dataConfig</q> parameter. The
debug mode of the DIH admin screen uses this to allow convenient
debugging / development of a DIH config. Since a DIH config can contain
scripts, this parameter is a security risk. Starting from now on, use
of this parameter requires setting the Java System property
<q>enable.dih.dataConfigParam</q> to true. For example this can be achieved
with solr-tomcat by adding -Denable.dih.dataConfigParam=true to
JAVA_OPTS in /etc/default/tomcat7.</p>

<p>For Debian 8 <q>Jessie</q>, this problem has been fixed in version
3.6.2+dfsg-5+deb8u3.</p>

<p>We recommend that you upgrade your lucene-solr packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a rel="nofollow" href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2019/dla-1954.data"
# $Id: $
