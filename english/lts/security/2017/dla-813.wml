<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Several vulnerabilities were discovered in wordpress, a web blogging
tool. The Common Vulnerabilities and Exposures project identifies the
following issues.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-5488">CVE-2017-5488</a>

<p>Multiple cross-site scripting (XSS) vulnerabilities in
wp-admin/update-core.php in WordPress before 4.7.1 allow remote
attackers to inject arbitrary web script or HTML via the name or
version header of a plugin.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-5489">CVE-2017-5489</a>

<p>Cross-site request forgery (CSRF) vulnerability in WordPress before
4.7.1 allows remote attackers to hijack the authentication of
unspecified victims via vectors involving a Flash file upload.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-5490">CVE-2017-5490</a>

<p>Cross-site scripting (XSS) vulnerability in the theme-name fallback
functionality in wp-includes/class-wp-theme.php in WordPress before
4.7.1 allows remote attackers to inject arbitrary web script or HTML
via a crafted directory name of a theme, related to
wp-admin/includes/class-theme-installer-skin.php.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-5491">CVE-2017-5491</a>

<p>wp-mail.php in WordPress before 4.7.1 might allow remote attackers to
bypass intended posting restrictions via a spoofed mail server with the
mail.example.com name.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-5492">CVE-2017-5492</a>

<p>Cross-site request forgery (CSRF) vulnerability in the widget-editing
accessibility-mode feature in WordPress before 4.7.1 allows remote
attackers to hijack the authentication of unspecified victims for
requests that perform a widgets-access action, related to
wp-admin/includes/class-wp-screen.php and wp-admin/widgets.php.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-5493">CVE-2017-5493</a>

<p>wp-includes/ms-functions.php in the Multisite WordPress API in WordPress
before 4.7.1 does not properly choose random numbers for keys, which
makes it easier for remote attackers to bypass intended access
restrictions via a crafted site signup or user signup.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-5610">CVE-2017-5610</a>

<p>wp-admin/includes/class-wp-press-this.php in Press This in WordPress
before 4.7.2 does not properly restrict visibility of a
taxonomy-assignment user interface, which allows remote attackers to
bypass intended access restrictions by reading terms.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-5611">CVE-2017-5611</a>

<p>SQL injection vulnerability in wp-includes/class-wp-query.php in
WP_Query in WordPress before 4.7.2 allows remote attackers to execute
arbitrary SQL commands by leveraging the presence of an affected
plugin or theme that mishandles a crafted post type name.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-5612">CVE-2017-5612</a>

<p>Cross-site scripting (XSS) vulnerability in
wp-admin/includes/class-wp-posts-list-table.php in the posts list
table in WordPress before 4.7.2 allows remote attackers to inject
arbitrary web script or HTML via a crafted excerpt.</p></li>

</ul>

<p>For Debian 7 <q>Wheezy</q>, these problems have been fixed in version
3.6.1+dfsg-1~deb7u13.</p>

<p>We recommend that you upgrade your wordpress packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2017/dla-813.data"
# $Id: $
