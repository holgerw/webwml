<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>The cPanel Security Team discovered several security vulnerabilities in
cgiemail, a CGI program used to create HTML forms for sending mails:</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-5613">CVE-2017-5613</a>

    <p>A format string injection vulnerability allowed to supply arbitrary
    format strings to cgiemail and cgiecho. A local attacker with
    permissions to provide a cgiemail template could use this
    vulnerability to execute code as webserver user.
    Format strings in cgiemail tempaltes are now restricted to simple
    %s, %U and %H sequences.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-5614">CVE-2017-5614</a>

    <p>An open redirect vulnerability in cgiemail and cgiecho binaries
    could be exploited by a local attacker to force redirect to an
    arbitrary URL. These redirects are now limited to the domain that
    handled the request.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-5615">CVE-2017-5615</a>

    <p>A vulnerability in cgiemail and cgiecho binaries allowed injection
    of additional HTTP headers. Newline characters are now stripped
    from the redirect location to protect against this.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-5616">CVE-2017-5616</a>

    <p>Missing escaping of the addendum parameter lead to a reflected
    cross-site (XSS) vulnerability in cgiemail and cgiecho binaries.
    The output is now html escaped.</p></li>

</ul>

<p>For Debian 7 <q>Wheezy</q>, these problems have been fixed in version
1.6-37+deb7u1.</p>

<p>We recommend that you upgrade your cgiemail packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2017/dla-869.data"
# $Id: $
