<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Two security issues were discovered in mupdf, a lightweight PDF viewer.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-14687">CVE-2017-14687</a>

    <p>MuPDF allows attackers to cause a denial of service or possibly have
    unspecified other impact via a crafted .xps file. This occurs
    because of mishandling of XML tag name comparisons.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-15587">CVE-2017-15587</a>

    <p>An integer overflow was discovered in pdf_read_new_xref_section in
    pdf/pdf-xref.c</p></li>

</ul>

<p>For Debian 7 <q>Wheezy</q>, these problems have been fixed in version
0.9-2+deb7u4.</p>

<p>We recommend that you upgrade your mupdf packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2017/dla-1164.data"
# $Id: $
