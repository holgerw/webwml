<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>Jeriko One discovered that newsbeuter, a text-mode RSS feed reader,
did not properly escape the title and description of a news article
when bookmarking it. This allowed a remote attacker to run an
arbitrary shell command on the client machine.</p>


<p>For Debian 7 <q>Wheezy</q>, these problems have been fixed in version
2.5-2+deb7u2.</p>

<p>We recommend that you upgrade your newsbeuter packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2017/dla-1061.data"
# $Id: $
