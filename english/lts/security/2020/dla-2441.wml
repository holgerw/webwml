<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>A privilege escalation was discovered in Sympa, a modern mailing list
manager. It is fixed when Sympa is used in conjunction with common
MTAs (such as Exim or Postfix) by disabling a setuid executable,
although no fix is currently available for all environments (such as
sendmail).  Additionally, an open-redirect vulnerability was
discovered and fixed.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-26880">CVE-2020-26880</a>

    <p>Sympa allows a local privilege escalation from the sympa user
    account to full root access by modifying the sympa.conf
    configuration file (which is owned by sympa) and parsing it
    through the setuid sympa_newaliases-wrapper executable.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-1000671">CVE-2018-1000671</a>

    <p>Sympa contains a CWE-601: URL Redirection to Untrusted Site ('Open
    Redirect') vulnerability in The <q>referer</q> parameter of the
    wwsympa.fcgi login action. that can result in Open redirection and
    reflected XSS via data URIs.</p></li>

</ul>

<p>For Debian 9 stretch, these problems have been fixed in version
6.2.16~dfsg-3+deb9u4.</p>

<p>We recommend that you upgrade your sympa packages.</p>

<p>For the detailed security status of sympa please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/sympa">https://security-tracker.debian.org/tracker/sympa</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2020/dla-2441.data"
# $Id: $
