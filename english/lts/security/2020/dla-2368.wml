<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>It was discovered that there was an arbitrary code execution vulnerability
in <a href="https://gruntjs.com/">Grunt</a>, a Javascript task runner. This was
possible due to the unsafe loading of YAML documents.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-7729">CVE-2020-7729</a>

    <p>The package grunt before 1.3.0 are vulnerable to Arbitrary Code
    Execution due to the default usage of the function load() instead of its
    secure replacement safeLoad() of the package js-yaml inside
    grunt.file.readYAML.</p></li>

</ul>

<p>For Debian 9 <q>Stretch</q>, these problems have been fixed in version
1.0.1-5+deb9u1.</p>

<p>We recommend that you upgrade your grunt packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2020/dla-2368.data"
# $Id: $
