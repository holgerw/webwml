<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>A remote code execution vulnerability was discovered in the Form API
component of the Horde Application Framework.  An authenticated remote
attacker could use this flaw to upload arbitrary content to an arbitrary
writable location on the server and potentially execute code in the
context of the web server user.</p>

<p>For Debian 8 <q>Jessie</q>, this problem has been fixed in version
2.0.8-2+deb8u2.</p>

<p>We recommend that you upgrade your php-horde-form packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2020/dla-2162.data"
# $Id: $
