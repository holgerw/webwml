#use wml::debian::template title="Siti mirror di Debian (in tutto il mondo)" MAINPAGE=true
#use wml::debian::translation-check translation="0ef84f1326b21d5b0e941b3e5987030eb9f366ac" maintainer="Giuseppe Sacco"

<link href="$(HOME)/font-awesome.css" rel="stylesheet" type="text/css">

<ul class="toc">
  <li><a href="#per-country">Mirror Debian per nazione</a></li>
  <li><a href="#complete-list">Elenco completo dei mirror</a></li>
</ul>

<aside>
<p><span class="fas fa-caret-right fa-3x"></span> Debian è distribuita
(<a href="https://www.debian.org/mirror/">mediante mirror</a>) su centinaia di
server. Se si sta pensando di <a href="../download">scaricare</a> Debian,
provare per primo un server vicino. Probabilmente sarà più veloce e inoltre
ridurrà il carico sui nostri server.</p>

<p class="centerblock">
Esistono mirror in molti paesi, per alcuni di questi è stato aggiunto
l'alias <code>ftp.&lt;paese&gt;.debian.org</code>. A questi alias
puntano a dei mirror che si sincronizzano regolarmente e velocemente
e dispongono di tutte le architetture. L'archivio Debian è sempre
disponibile via <code>HTTP</code> e nella posizione <code>/debian</code>.
</p>

<p class="centerblock">
Altri siti mirror possono avere delle restrizioni su
cosa ospitano (a causa di limiti di spazio). Solo perché un mirror non
è <code>ftp.&lt;paese&gt;.debian.org</code> non significa necessariamente
che sia più lento o meno aggiornato di
<code>ftp.&lt;paese&gt;.debian.org</code>. Infatti un mirror che dispone
della propria architettura, che è più vicino all'utente e, quindi, più
veloce è preferibile rispetto agli altri mirror più lontani.
</p>

<p>Utilizzare il sito a voi più vicino per ottenere download più veloci
indipendentemente che abbia o non abbia l'alias per paese.
Per determinare il sito con meno latenza si può utilizzare il programma
<a href="https://packages.debian.org/stable/net/netselect-apt">\
<code>netselect-apt</code></a>; per determinare il sito con il maggior rendimento
si usi un programma come 
<a href="https://packages.debian.org/stable/web/wget">\
<code>wget</code></a> o
<a href="https://packages.debian.org/stable/net/rsync">\
<code>rsync</code></a>.
Si noti che la vicinanza geografica è spesso il fattore più importante
per determinare quale macchina offrirà il servizio migliore.</p>

<p>Se il sistema viaggia molto, potrebbe essere opportuno utilizzare
un <q>mirror</q> servito tramite una <abbr
title="Content Delivery Network">CDN</abbr> globale. A tale scopo
il progetto Debian gestisce <code>deb.debian.org</code> e quindi è
possibile usarlo nel proprio <code>apt sources.list</code>; consultare il <a
href="http://deb.debian.org/">sito web del servizio per i dettagli</a>.
</p>

<p>
Qualsiasi altro dettaglio sui mirror Debian si trova alla pagina:
<url "https://www.debian.org/mirror/">.
</p>

<h2 id="per-country">Mirror Debian per nazione</h2>

<table border="0" class="center">
<tr>
  <th>Paese</th>
  <th>Sito</th>
  <th>Architetture</th>
</tr>
#include "$(ENGLISHDIR)/mirror/list-primary.inc"
</table>

<h2 id="complete-list">Elenco completo dei mirror</h2>

<table border="0" class="center">
<tr>
  <th>Nome dell'host</th>
  <th>HTTP</th>
  <th>Architetture</th>
</tr>
#include "$(ENGLISHDIR)/mirror/list-secondary.inc"
</table>

#include "$(ENGLISHDIR)/mirror/list-footer.inc"
