#use wml::debian::translation-check translation="79d3502f9416ddb5b313214ab9f721d43b0d4107" maintainer="Giuseppe Sacco"
<define-tag description>aggiornamento di sicurezza</define-tag>
<define-tag moreinfo>
<p>Nel pacchetto del motore web webkit2gtk sono state scoperte queste vulnerabilità:</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-21775">CVE-2021-21775</a>

    <p>Marcin Towalski ha scoperto che una pagina web fatta in un certo modo, può
    portare a una fuga di notizie e anche ad una corruzione della memoria.
    Per fare in modo da sfruttare questa vulnerabilità, la vittima deve essere
    portata a visitare la pagina web in questione.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-21779">CVE-2021-21779</a>

    <p>Marcin Towalski ha scoperto che una pagina web fatta in un certo modo, può
    portare a una fuga di notizie e anche ad una corruzione della memoria.
    Per fare in modo da sfruttare questa vulnerabilità, la vittima deve essere
    portata a visitare la pagina web in questione.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-30663">CVE-2021-30663</a>

    <p>Un ricercatore anonimo ha scoperto che la gestione di una pagina web
    con un contenuto specifico può portare all'esecuzione di codice arbitrario.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-30665">CVE-2021-30665</a>

    <p>yangkang ha scoperto che la gestione di una pagina web fatta in un
    certo modo può portare all'esecuzione di codice arbitrario. Apple è
    al corrente della segnalazione e questo problema potrebbe essere
    ancora sfruttato.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-30689">CVE-2021-30689</a>

    <p>Un ricercatore anonimo ha scoperto che la gestione di una pagina web
    con un contenuto specifico può portare all'esecuzione di script da altri siti.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-30720">CVE-2021-30720</a>

    <p>David Schutz ha scoperto che un sito web potrebbe essere in grado di
    accedere a porte con accesso normalmente limitato su altri server.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-30734">CVE-2021-30734</a>

    <p>Jack Dates ha scoperto che la gestione di una pagina web fatta in un
    certo modo può portare all'esecuzione di codice arbitrario.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-30744">CVE-2021-30744</a>

    <p>Dan Hite ha scoperto che la gestione di una pagina web fatta in un
    certo modo può portare all'esecuzione di script da altri siti.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-30749">CVE-2021-30749</a>

    <p>An anonymous ha scoperto che la gestione di una pagina web
    con un contenuto specifico può portare all'esecuzione di codice arbitrario.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-30758">CVE-2021-30758</a>

    <p>Christoph Guttandin ha scoperto che una pagina web fatta in un certo modo, può
    portare all'esecuzione di codice arbitrario.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-30795">CVE-2021-30795</a>

    <p>Sergei Glazunov ha scoperto che la gestione di una pagina web fatta in un
    certo modo può portare all'esecuzione di codice arbitrario.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-30797">CVE-2021-30797</a>

    <p>Ivan Fratric ha scoperto che una pagina web fatta in un certo modo, può
    portare all'esecuzione di codice arbitrario.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-30799">CVE-2021-30799</a>

    <p>Sergei Glazunov ha scoperto che la gestione di una pagina web fatta in un
    certo modo può portare all'esecuzione di codice arbitrario.</p></li>

</ul>

<p>Per la distribuzione stabile (buster), questo problema è stato risolto nella
versione 2.32.3-1~deb10u1.</p>

<p>Si raccomanda di aggiornare i pacchetti webkit2gtk.</p>

<p>Per il dettaglio sullo stato di webkit2gtk fare riferimento alla
sua pagina con le informazioni di sicurezza:
<a href="https://security-tracker.debian.org/tracker/webkit2gtk">\
https://security-tracker.debian.org/tracker/webkit2gtk</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2021/dsa-4945.data"
# $Id: $
