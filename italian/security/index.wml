#use wml::debian::template title="Informazioni sulla sicurezza" GEN_TIME="yes"  MAINPAGE="true"
#use wml::debian::toc
#use wml::debian::recent_list_security
#include "$(ENGLISHDIR)/releases/info"
#use wml::debian::translation-check translation="e49cfbc5f5d8df40e0852a5da747d32544d67b06" maintainer="Mirco Scottà"

<link href="$(HOME)/font-awesome.css" rel="stylesheet" type="text/css">

<ul class="toc">
<li><a href="#keeping-secure">Fare in modo che il proprio sistema Debian rimanga sicuro</a></li>
<li><a href="#DSAS">Bollettini della sicurezza recenti</a></li>
</ul>

<aside>
<p><span class="fas fa-caret-right fa-3x"></span> Debian considera la
sicurezza in maniera molto seria. Gestiamo tutti i problemi che ci
vengono segnalati e ci assicuriamo che abbiano una soluzione in tempo
relativamente breve.</p>
</aside>

<p>
L'esperienza ha insegnato che la <q>sicurezza attraverso il complicato</q>
non funziona. L'avere tutto esibito al pubblico permette di arrivare prima
alla soluzione (che spesso è anche migliore) dei problemi di sicurezza. In
questo spirito si può trovare in questa pagina lo stato di tutti i problemi
conosciuti che coinvolgono Debian.
</p>

<p>
Il progetto Debian gestisce coordinandosi con altri produttori di software
libero e di conseguenza i bollettini sono pubblicati nello stesso giorno in
cui la vulnerabilità è resa pubblica.
</p>

# "reasonable timeframe" might be too vague, but we don't have
# accurate statistics. For older (out of date) information and data
# please read:
# https://www.debian.org/News/2004/20040406  [ Year 2004 data ]
# and (older)
# https://people.debian.org/~jfs/debconf3/security/ [ Year 2003 data ]
# https://lists.debian.org/debian-security/2001/12/msg00257.html [ Year 2001]
# If anyone wants to do up-to-date analysis please contact me (jfs)
# and I will provide scripts, data and database schemas.


<p>
Debian partecipa anche al gruppo per la standardizzazione della sicurezza:
</p>

<ul>
  <li>I <a href="#DSAS">Bollettini della sicurezza Debian (Debian Security
  Advisories)</a> sono <a href="cve-compatibility">compatibili con CVE</a>
  (vedere i <a href="crossreferences">riferimenti incrociati</a>).</li>
  <li>Debian è presente nella direzione del progetto <a
  href="https://oval.cisecurity.org/">Open Vulnerability Assessment
  Language</a>.</li>
</ul>

<h2><a id="keeping-secure">Fare in modo che il proprio sistema Debian rimanga sicuro</a></h2>

<p>
Per ricevere le ultime segnalazioni di sicurezza di Debian, iscriversi
alla lista di messaggi <a
href="https://lists.debian.org/debian-security-announce/">debian-security-announce</a>.
</p>

<p>
Oltre a questo, usare <a href="https://packages.debian.org/stable/admin/apt">apt</a>
per ottenere facilmente gli ultimi aggiornamenti di sicurezza. Per tenere
aggiornato il sistema con le ultime correzioni per la sicurezza, aggiungere
una riga come questa nel file <code>/etc/apt/sources.list</code>:
</p>

<pre>
deb http://security.debian.org/debian-security <current_release_security_name> main contrib non-free non-free-firmware
</pre>

<p>
Salvare la modifica ed eseguire i due comandi seguenti per scaricare e
installare gli aggiornamenti in sospeso.
</p>

<pre>
apt-get update &amp;&amp; apt-get upgrade
</pre> 

<p>
L'archivio della sicurezza è firmato con le normali <a
href="https://ftp-master.debian.org/keys.html">chiavi di firma</a> degli
archivi Debian.
</p>

<p>
Per maggiori informazioni sugli aspetti della sicurezza in Debian, si veda
le FAQ e la documentazione:
</p>

<p style="text-align:center"><button type="button"><span class="fas 
fa-book-open fa-2x"></span> <a href="faq">Security FAQ</a></button> <button 
type="button"><span class="fas fa-book-open fa-2x"></span> <a 
href="../doc/user-manuals#securing">Securing Debian</a></button></p>


<aside class="light">
  <span class="fa fa-rss fa-5x"></span>
</aside>

<h2><a id="DSAS">Bollettini della sicurezza recenti</a></h2>

<p>Queste pagine contengono un archivio dei bollettini della sicurezza postate 
nella lista <a href="https://lists.debian.org/debian-security-announce/">\
debian-security-announce</a><br><a href="dsa.html#DSAS">Elenco nel nuovo
formato</a>.</p>

<p>
<:= get_recent_security_list( '1m', '6', '.', '$(ENGLISHDIR)/security' ) :>
</p>

{#rss#:
<link rel="alternate" type="application/rss+xml"
 title="Debian Security Advisories (titles only)" href="dsa">
<link rel="alternate" type="application/rss+xml"
 title="Debian Security Advisories (summaries)" href="dsa-long">
:#rss#}

<p>
Gli ultimi bollettini della sicurezza di Debian sono anche disponibili in <a
href="dsa">format RDF</a>. Inoltre forniamo un <a href="dsa-long">secondo
file</a> che include il primo paragrafo del corrispondente bollettino, in
modo da vedere di che tratta l'avviso stesso.
</p>

#include "$(ENGLISHDIR)/security/index.include"
<p>Sono disponibili anche i bollettini più vecchi:
<:= get_past_sec_list(); :>

<p>Le distribuzioni Debian non sono vulnerabili a tutti i problemi di
sicurezza. Il <a href="https://security-tracker.debian.org/">tracciatore della
sicurezza Debian</a> raccoglie tutte le informazioni sullo stato di
vulnerabilità dei pacchetti Debian. È indicizzato per nome CVE e per
pacchetto.</p>
