#use wml::debian::translation-check translation="8841a09ee24923f86538151f0ba51430b8414c1a"
<define-tag description>actualización de seguridad</define-tag>
<define-tag moreinfo>
<p>Se han descubierto varias vulnerabilidades en ldb, una base de datos
tipo LDAP y embebida construida sobre TDB.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-10730">CVE-2020-10730</a>

    <p>Andrew Bartlett descubrió un defecto de desreferencia de puntero
    NULL y de «uso tras liberar» al tratar controles LDAP <q>ASQ</q> y <q>VLV</q> y
    combinaciones con la funcionalidad paged_results de LDAP.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-27840">CVE-2020-27840</a>

    <p>Douglas Bagnall descubrió un defecto de corrupción de memoria dinámica («heap») mediante cadenas
    de caracteres DN preparadas de una manera determinada.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-20277">CVE-2021-20277</a>

    <p>Douglas Bagnall descubrió una vulnerabilidad de lectura fuera de límites en
    la gestión de atributos LDAP que contienen varios espacios en blanco
    consecutivos al principio.</p></li>

</ul>

<p>Para la distribución «estable» (buster), estos problemas se han corregido en
la versión 2:1.5.1+really1.4.6-3+deb10u1.</p>

<p>Le recomendamos que actualice los paquetes de ldb.</p>

<p>Para información detallada sobre el estado de seguridad de ldb, consulte su página
en el sistema de seguimiento de problemas de seguridad:
<a href="https://security-tracker.debian.org/tracker/ldb">https://security-tracker.debian.org/tracker/ldb</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2021/dsa-4884.data"
