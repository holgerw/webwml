#use wml::debian::translation-check translation="cd0bf8b46e55223d74a3837b6bf7fc8d93156c9a"
<define-tag description>actualización de seguridad</define-tag>
<define-tag moreinfo>
<p>Se han descubierto varias vulnerabilidades en el navegador web Chromium:</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-21159">CVE-2021-21159</a>

    <p>Khalil Zhani descubrió un problema de desbordamiento de memoria en la implementación de pestañas («tab»).</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-21160">CVE-2021-21160</a>

    <p>Marcin Noga descubrió un problema de desbordamiento de memoria en WebAudio.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-21161">CVE-2021-21161</a>

    <p>Khalil Zhani descubrió un problema de desbordamiento de memoria en la implementación de pestañas («tab»).</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-21162">CVE-2021-21162</a>

    <p>Se descubrió un problema de «uso tras liberar» en la implementación de WebRTC.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-21163">CVE-2021-21163</a>

    <p>Alison Huffman descubrió un problema de validación de datos.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-21165">CVE-2021-21165</a>

    <p>Alison Huffman descubrió un error en la implementación de audio.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-21166">CVE-2021-21166</a>

    <p>Alison Huffman descubrió un error en la implementación de audio.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-21167">CVE-2021-21167</a>

    <p>Leecraso y Guang Gong descubrieron un problema de «uso tras liberar» en la implementación
    de favoritos.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-21168">CVE-2021-21168</a>

    <p>Luan Herrera descubrió un error de imposición de reglas en la caché de aplicaciones.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-21169">CVE-2021-21169</a>

    <p>Bohan Liu y Moon Liang descubrieron un problema de acceso fuera de límites en la
    biblioteca javascript v8.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-21170">CVE-2021-21170</a>

    <p>David Erceg descubrió un error en la interfaz de usuario.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-21171">CVE-2021-21171</a>

    <p>Irvan Kurniawan descubrió un error en la interfaz de usuario.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-21172">CVE-2021-21172</a>

    <p>Maciej Pulikowski descubrió un error de imposición de reglas en la API del
    sistema de archivos.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-21173">CVE-2021-21173</a>

    <p>Tom Van Goethem descubrió una fuga de información en el código de red.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-21174">CVE-2021-21174</a>

    <p>Ashish Guatam Kambled descubrió un error de implementación en la política
    relativa a quien hace la referencia («Referrer policy»).</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-21175">CVE-2021-21175</a>

    <p>Jun Kokatsu descubrió un error de implementación en la funcionalidad de aislamiento
    de sitios web («Site Isolation»).</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-21176">CVE-2021-21176</a>

    <p>Luan Herrera descubrió un error de implementación en el modo de pantalla completa.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-21177">CVE-2021-21177</a>

    <p>Abdulrahman Alqabandi descubrió un error de imposición de reglas en la
    funcionalidad de llenado automático («Autofill»).</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-21178">CVE-2021-21178</a>

    <p>Japong descubrió un error en la implementación de Compositor.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-21179">CVE-2021-21179</a>

    <p>Se descubrió un problema de «uso tras liberar» en la implementación de red.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-21180">CVE-2021-21180</a>

    <p>Abdulrahman Alqabandi descubrió un problema de «uso tras liberar» en la funcionalidad de búsqueda
    en pestañas.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-21181">CVE-2021-21181</a>

    <p>Xu Lin, Panagiotis Ilias y Jason Polakis descubrieron una fuga de información mediante
    ataque de canal lateral en la funcionalidad de llenado automático («Autofill»).</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-21182">CVE-2021-21182</a>

    <p>Luan Herrera descubrió un error de imposición de reglas en la implementación de la
    navegación de sitios.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-21183">CVE-2021-21183</a>

    <p>Takashi Yoneuchi descubrió un error de implementación en la API de rendimiento («Performance API»).</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-21184">CVE-2021-21184</a>

    <p>James Hartig descubrió un error de implementación en la API de rendimiento («Performance API»).</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-21185">CVE-2021-21185</a>

    <p>David Erceg descubrió un error de imposición de reglas en Extensions.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-21186">CVE-2021-21186</a>

    <p>dhirajkumarnifty descubrió un error de imposición de reglas en la implementación del
    escaneo de QR.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-21187">CVE-2021-21187</a>

    <p>Kirtikumar Anandrao Ramchandani descubrió un error de validación de datos en
    el formateo de los URL.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-21188">CVE-2021-21188</a>

    <p>Woojin Oh descubrió un problema de «uso tras liberar» en Blink/Webkit.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-21189">CVE-2021-21189</a>

    <p>Khalil Zhani descubrió un error de imposición de reglas en la implementación
    de Payments.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-21190">CVE-2021-21190</a>

    <p>Zhou Aiting descubrió uso de memoria no inicializada en la biblioteca pdfium.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-21191">CVE-2021-21191</a>

    <p>raven descubrió un problema de «uso tras liberar» en la implementación de WebRTC.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-21192">CVE-2021-21192</a>

    <p>Abdulrahman Alqabandi descubrió un problema de desbordamiento de memoria en la implementación
    de pestañas («tab»).</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-21193">CVE-2021-21193</a>

    <p>Se descubrió un problema de «uso tras liberar» en Blink/Webkit.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-21194">CVE-2021-21194</a>

    <p>Leecraso y Guang Gong descubrieron un problema de «uso tras liberar» en la funcionalidad
    de captura de pantalla.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-21195">CVE-2021-21195</a>

    <p>Liu y Liang descubrieron un problema de «uso tras liberar» en la biblioteca
    javascript v8.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-21196">CVE-2021-21196</a>

    <p>Khalil Zhani descubrió un problema de desbordamiento de memoria en la implementación de pestañas («tab»).</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-21197">CVE-2021-21197</a>

     <p>Abdulrahman Alqabandi descubrió un problema de desbordamiento de memoria en la implementación
     de pestañas («tab»).</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-21198">CVE-2021-21198</a>

    <p>Mark Brand descubrió un problema de lectura fuera de límites en la implementación de la
    comunicación entre procesos.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-21199">CVE-2021-21199</a>

    <p>Weipeng Jiang descubrió un problema de «uso tras liberar» en el gestor de ventanas
    y eventos Aura.</p></li>

</ul>

<p>Para la distribución «estable» (buster), estos problemas se han corregido en
la versión 89.0.4389.114-1~deb10u1.</p>

<p>Le recomendamos que actualice los paquetes de chromium.</p>

<p>Para información detallada sobre el estado de seguridad de chromium, consulte
su página en el sistema de seguimiento de problemas de seguridad:
<a href="https://security-tracker.debian.org/tracker/chromium">\
https://security-tracker.debian.org/tracker/chromium</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2021/dsa-4886.data"
