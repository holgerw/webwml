#use wml::debian::translation-check translation="08e97d6a66338b9fb8da51eb27b4c3dde971c164"
<define-tag description>actualización de seguridad</define-tag>
<define-tag moreinfo>
<p>Se descubrieron múltiples vulnerabilidades en coTURN, un servidor TURN y STUN para
VoIP.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-4056">CVE-2018-4056</a>

    <p>Se descubrió una vulnerabilidad de inyección de SQL en el portal web del
    administrador de coTURN. Lamentablemente, como la interfaz web de administración se
    comparte con la producción, no es posible filtrar de forma sencilla el acceso
    del exterior, y esta actualización de seguridad inhabilita completamente la interfaz web. Los usuarios
    deberían utilizar en su lugar la interfaz local de línea de órdenes.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-4058">CVE-2018-4058</a>

    <p>La configuración por omisión habilita la retransmisión («forwarding») insegura por loopback. Un atacante remoto
    con acceso a la interfaz TURN puede usar esta vulnerabilidad para obtener acceso
    a servicios que deberían ser exclusivamente locales.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-4059">CVE-2018-4059</a>

    <p>La configuración por omisión usa una contraseña vacía para la interfaz de administración
    por línea de órdenes local. Un atacante con acceso a la consola local
    (ya sea un atacante local o un atacante remoto aprovechándose de 
    <a href="https://security-tracker.debian.org/tracker/CVE-2018-4058">CVE-2018-4058</a>)
    podría elevar privilegios a los de administrador del servidor
    coTURN.</p></li>

</ul>

<p>Para la distribución «estable» (stretch), estos problemas se han corregido en
la versión 4.5.0.5-1+deb9u1.</p>

<p>Le recomendamos que actualice los paquetes de coturn.</p>

<p>Para información detallada sobre el estado de seguridad de coturn, consulte su
página en el sistema de seguimiento de problemas de seguridad:
<a href="https://security-tracker.debian.org/tracker/coturn">\
https://security-tracker.debian.org/tracker/coturn</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2019/dsa-4373.data"
