#use wml::debian::template title="Réplicas de Debian" MAINPAGE="true"
#use wml::debian::translation-check translation="0d90a782010be0f74eb482a26ebb41d18aebee61" maintainer="Laura Arjona Reina"

<link href="$(HOME)/font-awesome.css" rel="stylesheet" type="text/css">

<aside>
<p><span class="fas fa-caret-right fa-3x"></span> Las réplicas de Debian están mantenidas por voluntarios. Si tiene la posibilidad de donar espacio de disco y conectividad, considere crear una réplica para hacer Debian más accesible. Para más información, visite <a href="ftpmirror">la siguiente página</a>.<p>
</aside>

<p>Debian se distribuye en todo el mundo mediante el uso de 
servidores de réplica con el propósito de proporcionar un mejor acceso 
a nuestros usuarios.</p>

<p>Existen réplicas de los siguientes repositorios de Debian:</p>

<dl>
<dt><strong>Paquetes de Debian</strong> (<code>debian/</code>)</dt>
  <dd>El almacén de paquetes de Debian: aquí están incluidos la gran
 mayoría de los paquetes <code>.deb</code>, los materiales necesarios para la
 instalación así como las fuentes.
      <br>
      Puede ver la lista de <a href="list">réplicas de Debian</a> que contienen
      el repositorio
      <code>debian/</code>.
  </dd>
<dt><strong>Imágenes de los CD </strong> (<code>debian-cd/</code>)</dt>
  <dd>El repositorio de imágenes de los CD: incluye los archivos
 Jigdo y los archivos con las imágenes ISO.
      <br>
      Puede ver la lista de <a href="$(HOME)/CD/http-ftp/#mirrors">réplicas de 
      Debian</a> que incluyen el repositorio <code>debian-cd/</code>.
  </dd>
<dt><strong>Versiones antiguas</strong> (<code>debian-archive/</code>)</dt>
  <dd>El repositorio con las versiones antiguas de Debian publicadas.
      <br>
      Puede ver los <a href="$(HOME)/distrib/archive">Archivos históricos de la distribución</a>
      para más información.
  </dd>
</dl>

<p>Visualización del
<a href="https://mirror-master.debian.org/status/mirror-status.html">
estado de las réplicas de Debian
</a></p>
