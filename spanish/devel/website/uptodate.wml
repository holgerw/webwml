#use wml::debian::template title="Cómo mantener las traducciones al día"
#use wml::debian::translation-check translation="8f2dd37edbf6287df4029e3f234798efbcce2862" maintainer="Gildardo A. Maravilla Jácome"

<P>Ya que las páginas web no son estáticas, es una buena idea poder
seguir la pista al original a partir del que está hecha una
traducción, y usar esta información para revisar qué páginas han
cambiado desde la última traducción. Esta información debería
insertarse al principio del documento (aunque después de cualesquiera
cabeceras #use que haya) de esta forma:

<pre>
\#use wml::debian::translation-check translation="git_commit_hash"
</pre>

<p>donde <var>git_commit_hash</var> es el hash commit de git que se 
refiere  al commit del archivo original (en inglés) con el que el 
archivo traducido fue comparado. Puede obtener más detalles de ese 
commit específico usando la herramienta <code>git show</code>: 
<code>git show&lt;git_commit_hash&gt;</code> . Si usa el script 
<kbd>copypage.pl</kbd> en el directorio webwml, la línea 
<code>translation-check</code> será añadida automáticamente en la
primera línea de su página traducida, señalando la versión del archivo 
original que exista en ese momento. </p>

<p>Algunas traducciones podrían no haber sido actualizadas en mucho 
tiempo, incluso aunque el original (en inglés) lo haya hecho. Debido a 
la negociación de contenido, el lector podría no darse cuenta de esto y 
perder información importante, introducida en las nuevas versiones del 
original. La plantilla <code>translation-check</code> contiene código 
para comprobar si su traducción está obsoleta y mostrar un mensaje 
apropiado advirtiendo al usuario sobre ello. </p>

<P>También hay algunos parámetros adicionales que puede usar en la
línea <code>translation-check</code>:

<dl>
 <dt><code>original="<var>idioma</var>"</code>
 <dd>donde <var>idioma</var> es el nombre del idioma del que se está
 traduciendo, si no es inglés.
 El nombre debe corresponder al del subdirectorio de primer nivel en el 
 VCS, y al nombre en la plantilla <code>languages.wml</code>.

 <dt><code>mindelta="<var>número</var>"</code>
 <dd>el cual define la máxima diferencia en revisiones de git antes de
 que la traducción se considere <strong>obsoleta</strong>.
 El valor por omisión es <var>1</var>.
 En las páginas menos importantes, póngalo en <var>2</var>, lo que 
 significa que dos cambios hacen que la traducción se considere antigua.

 <dt><code>maxdelta="<var>número</var>"</code>
 <dd>el cual define la máxima diferencia en revisiones de git antes de
 que la traducción se considere <strong>obsoleta</strong>.
 El valor por omisión es <var>5</var>.
 En páginas muy importantes, bájelo.
 Un valor de <var>1</var> significa que cada cambio hace que la
 traducción se considere antigua.
</dl>

<p>Rastrear la edad de las traducciones también nos permite tener 
<a href="stats/">estadísticas de traducción</a>, un reporte de traducciones 
obsoletas (junto a links útiles con las diferencias entre los archivos), 
con una lista de páginas que no han sido traducidas en absoluto. 
Su objetivo es ayudar a los traductores y atraer nuevas personas para 
colaborar. </p>

<p>
Para evitar que a nuestros usuarios se les presente información que
esté muy desactualizada, las traducciones que no hayan sido actualizadas
en seis meses desde que la página original haya sido cambiada, será
eliminada automáticamente. Revise por favor la 
<a href="https://www.debian.org/devel/website/stats/">lista de
traducciones desactualizadas</a> para localizar páginas que estén
en peligro de ser eliminadas.
</p>

<P>Adicionalmente, el guión <kbd>check_trans.pl</kbd> está disponible
en el directorio webwml/, y permite obtener un informe con las páginas
que necesitan actualizaciones:

<pre>
check_trans.pl <var>idioma</var>
</pre>

<P>donde <var>idioma</var> es el nombre del directorio que contiene su
traducción, por ejemplo, «spanish».

<P>Las páginas que carecen de traducción se mostrarán como
"<code>Missing <var>fichero</var></code>", y las páginas que no estén
actualizadas con respecto al original se mostrarán como
"<code>NeedToUpdate <var>filename</var> to version <var>XXXXXXX</var></code>".

<p>Si quiere ver cuáles son los cambios exactos, puede obtener las
diferencias añadiendo la opción <kbd>-d</kbd> en la línea de órdenes
de la orden anterior. </p>

<p>Si quiere ignorar los avisos sobre traducciones faltantes (por ejemplo,
páginas de noticias viejas), puede crear el archivo llamado 
<code>.transignore</code> en el directorio donde quiere eliminar los
avisos, enumerando las páginas que no va a traducir, con un nombre por
línea. </p>

<p>
También existe un script similar para llevar un seguimiento de
traducciones de descripciones de las listas de correo. Se puede encontrar
documentación en los comentarios del guión
<code>check_desc_trans.pl</code>. 
</p>
